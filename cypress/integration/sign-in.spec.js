context('Document Preview Defaults Assertions', () => {
  beforeEach(() => {
    cy.server()
    cy.visit("/login");
    cy.fixture("signin.json").as('validSignin')
    cy.fixture("invalidSignin.json").as('invalidSignin')
  })

  describe("Sign in Components exist", () => {
    it("should visit login page and components exist", () => {
      cy.get('[data-cy="forgot"]')
        .should('exist')
      cy.get('[data-cy="register"]')
        .should('exist')
      cy.get('[data-cy="password"]')
        .should('exist')
      cy.get('[data-cy="username"]')
        .should('exist')
      cy.get('[data-cy="signin"]')
        .should('exist')
    })
  })
  describe("Test Forgot Anchor", () => {
    it("Clicking forgot link takes you to forgot page", () => {
      cy.get('[data-cy="forgot"]')
        .should('exist')
        .click()
      cy.url().should('include', '/forgot')
    })
  })

  describe("Test Register Anchor", () => {
    it("Clicking register link takes you to registration page", () => {
      cy.get('[data-cy="register"]')
        .should('exist')
        .click()
      cy.url().should('include', '/register')
    })
  })

  describe("Test Sign In Button invalid username", () => {
    it("Clicking register link takes you to registration page", () => {
      cy.route('POST', '**/login', '@invalidSignin' ).as('login');
      cy.get('[data-cy="username"]')
        .type("unknown")
      cy.get('[data-cy="password"]')
        .type('password')
      cy.get('[data-cy="signin"]')
        .click()
      cy.wait('@login')
      cy.get('[data-cy="error"]')
        .should('exist')
        .contains("Invalid username and/or password")
    })
  })
  describe("Test Sign In Button Valid username", () => {
    it("Clicking register link takes you to registration page", () => {
      cy.route('POST', '**/login', '@validSignin' ).as('login');
      cy.get('[data-cy="username"]')
        .type("kburkart")
      cy.get('[data-cy="password"]')
        .type('password')
      cy.get('[data-cy="signin"]')
        .click()
      cy.wait('@login')
      cy.get('[data-cy="error"]')
        .should('not.exist')
      cy.url().should('include', '/dashboard')
    })
  })

})
