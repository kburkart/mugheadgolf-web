context('Add Golfer Assertions', () => {
  beforeEach(() => {
    cy.login()
    cy.visit("/authenticated/dashboard/");
    cy.server()
    cy.fixture("signin.json").as('validSignin')
    cy.fixture("invalidSignin.json").as('invalidSignin')
    cy.fixture("addgolfer-request.json").as('addGolferRequest')
  })
  describe("Dashboard Components exist", () => {
    it("should visit registration page and components exist", () => {
      cy.get('input[id="firstnameId"]')
        .should('exist')
    })
  })
})
