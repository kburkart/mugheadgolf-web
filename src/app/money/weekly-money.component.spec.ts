import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyMoneyComponent } from './weekly-money.component';

describe('WeeklyMoneyComponent', () => {
  let component: WeeklyMoneyComponent;
  let fixture: ComponentFixture<WeeklyMoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyMoneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
