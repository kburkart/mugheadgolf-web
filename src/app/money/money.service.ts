import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { IMoney } from './money';
import { IGolferMoney } from './golfer-money';
import { IGolfer } from '../golfer/golfer';

@Injectable()
export class MoneyService {

  private baseUrl = environment.url + 'money/';
  golferMoney: IGolferMoney;

  constructor(private http: Http) { }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
  initGM(): IGolferMoney {
    return {
      id: 0,
      idgolfer: 0,
      golfername: 'none',
      skins: 0,
      pin1_v: 0,
      pin2_v: 0,
      lownet1: 0,
      lownet2: 0,
      longdrive: 0,
      longputt: 0,
      pin1_lr: 0,
      pin2_lr: 0,
      total: 0
    };
  }

  getGolfersMoney(year: number): Observable<IGolferMoney[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'golfers/' + year;

    return this.http.get(url, options)
      .map(response => response.json())
      //    .do(data => console.log('getGolfers: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getGolferMoney(year: number, golfer: number): Observable<IGolferMoney> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'golfer/' + golfer + '/' + year;

    return this.http.get(url, options)
      .map(this.extractData)
      //    .do(data => console.log('getGolfers: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getWeeklyWinners(year: number): Observable<any[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'year/' + year;

    return this.http.get(url, options)
      .map(response => response.json())
      //      .do(data => console.log('winners: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }
  private extractData(response: Response): IGolferMoney {
    let golferMoney = {} as IGolferMoney;
    golferMoney.total = 0;
    if (response.text() === '') {
    } else {
      golferMoney = response.json();
    }
    return golferMoney;
  }

  calculateLowNet(year: number, week: number): Observable<any[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'lownet/' + year + '/' + week;

    return this.http.get(url, options)
      .map(response => response.json())
      //      .do(data => console.log('winners: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  calculateSkins(year: number, week: number): Observable<IGolfer[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'skins/' + year + '/' + week;

    return this.http.get(url, options)
      .map(response => response.json())
      //      .do(data => console.log('winners: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveWinners(winners: IMoney[], week: number) {
    const year = localStorage.getItem('year');
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'saveWeek/' + week + '/' + year;

    return this.http.post(url, winners, options)
      .map(response => response.json())
      //      .do(data => console.log('winners: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getMoneyByWeek(week: number): Observable<IMoney[]> {
    const year = localStorage.getItem('year');
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'week/' + week + '/' + year;

    return this.http.get(url, options)
      .map(response => response.json())
      //      .do(data => console.log('winners: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  initializeMoney(): IMoney {
    return {
      idmoney: undefined,
      amount: 0,
      idgame: null,
      idgolfer: null,
      idweek: null,
      year: 0
    };

  }

}
