import { TestBed, inject } from '@angular/core/testing';

import { GolferMoneyService } from './golfer-money.service';

describe('GolferMoneyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GolferMoneyService]
    });
  });

  it('should be created', inject([GolferMoneyService], (service: GolferMoneyService) => {
    expect(service).toBeTruthy();
  }));
});
