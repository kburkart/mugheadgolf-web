import { Component, OnInit, Input, EventEmitter, OnDestroy, HostListener } from '@angular/core';
import { MoneyService } from './money.service';
import { IGolfer } from '../golfer/golfer';
import { IGolferMoney } from './golfer-money';
import { SettingsService } from '../settings/settings.service';
import { IGames } from '../settings/games';

@Component({
  selector: 'app-weekly-money',
  templateUrl: './weekly-money.component.html',
  styleUrls: ['./weekly-money.component.css']
})
export class WeeklyMoneyComponent implements OnInit {

  pageTitle = 'Weekly Money Winners';
  errorMessage: string;

  weeklyWinners: any[] = [];
  numbers: number[];

  constructor(private moneyService: MoneyService,
    private settingsService: SettingsService) { }

  ngOnInit() {
    const year  = +localStorage.getItem('year');
    this.moneyService.getWeeklyWinners(year)
    .subscribe(weeklyWinners => this.populateData(weeklyWinners),
      error => this.errorMessage = <any>error);
  }
  populateData(weeklyWinners: any[]): void {
    this.weeklyWinners = weeklyWinners;
  }

}
