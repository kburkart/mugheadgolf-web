import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';

import { SettingsService } from '../settings/settings.service';
import { ISettings } from '../settings/settings';
import { MoneyService } from './money.service';
import { GolferService } from '../golfer/golfer.service';
import { IGolfer } from '../golfer/golfer';
import { IGames } from '../settings/games';
import { IMoney } from './money';
import { Router } from '@angular/router';
import { ScoreService } from '../score/score.service';

@Component({
  selector: 'app-add-money',
  templateUrl: './add-money.component.html',
  styleUrls: ['./add-money.component.css']
})
export class AddMoneyComponent implements OnInit {

  pageTitle = 'Add Weekly Money Winners';
  errorMessage: string;
  settings: ISettings[];
  numbers: number[];
  public golfers: IGolfer[];
  moneyForm: FormGroup;
  weekForm: FormGroup;
  games: IGames[];
  skins: IGolfer[] = [];
  lownet1: IGolfer[] = [];
  lownet2: IGolfer[] = [];
  winners: IMoney[] = [];
  week = 0;
  skinsString: string;
  net1String: string;
  net2String: string;


  constructor(private fb: FormBuilder,
    private settingsService: SettingsService,
    private moneyService: MoneyService,
    private scoreService: ScoreService,
    private router: Router,
    private golferService: GolferService) { }

  ngOnInit() {
    const year  = +localStorage.getItem('year');
    this.settingsService.getSettings(year).subscribe(settings => this.populateData(year, settings.totalweeks),
      error => this.errorMessage = <any>error);
    this.weekForm = this.fb.group({
      selectWeek: ''
    });
    this.moneyForm = this.fb.group({
      skintext: ['', Validators.required],
      pin1_1: [''],
      pin2_1: [''],
      pin1_2: [''],
      pin2_2: [''],
      net1: ['', Validators.required],
      net2: ['', Validators.required],
      longdrive: ['', Validators.required],
      longputt: ['', Validators.required]
    });
  }
  populateData(year: number, numWeeks: number) {
    this.numbers = Array.from(Array(numWeeks)).map((x, i) => i);
    this.getListOfGolfers();
    this.weekForm.get('selectWeek').valueChanges.subscribe((value: number) => this.populateWeek(value));

  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
    this.settingsService.getGames()
      .subscribe(games => this.games = games,
        error => this.errorMessage = <any>error);
  }

  populateWeek(week: number): void {
    this.week = week;
    this.moneyForm.reset();
    this.moneyService.getMoneyByWeek(week).subscribe(money => this.populateWinners(money),
      error => this.errorMessage = <any>error);
  }

  populateWinners(winners: IMoney[]): void {
    this.winners = winners;
    let x = 0;
    let y = 0;
    let z = 0;

    winners.forEach(winner => {
      if (winner.idgame.idgame === 1) {
        this.skins[x] = this.golferService.initializeGolfer();
        this.skins[x] = winner.idgolfer;
        if (this.skinsString === undefined) {
          this.skinsString = this.skins[x].firstname + ' ' + this.skins[x].lastname;
        } else {

          this.skinsString += ('\n' + this.skins[x].firstname + ' ' + this.skins[x].lastname);
        }
        this.moneyForm.patchValue({ skintext: this.skinsString });

        x++;
      } else if (winner.idgame.idgame === 2) {
        let selected: IGolfer;
        this.golfers.forEach(element => {
          if (element.idgolfer === winner.idgolfer.idgolfer) {
            selected = element;
          }
        });
        this.moneyForm.patchValue({ pin1: selected });
      } else if (winner.idgame.idgame === 3) {
        let selected: IGolfer;
        this.golfers.forEach(element => {
          if (element.idgolfer === winner.idgolfer.idgolfer) {
            selected = element;
          }
        });
        this.moneyForm.patchValue({ pin2: selected });
      } else if (winner.idgame.idgame === 4) {
        this.lownet1[y] = this.golferService.initializeGolfer();
        this.lownet1[y] = winner.idgolfer;
        if (this.net1String === undefined) {
          this.net1String = this.lownet1[y].firstname + ' ' + this.lownet1[y].lastname;
        } else {

          this.net1String += ('\n' + this.lownet1[y].firstname + ' ' + this.lownet1[y].lastname);
        }
        this.moneyForm.patchValue({ net1: this.net1String });

        y++;
      } else if (winner.idgame.idgame === 5) {
        this.lownet2[z] = this.golferService.initializeGolfer();
        this.lownet2[z] = winner.idgolfer;
        if (this.net2String === undefined) {
          this.net2String = this.lownet2[z].firstname + ' ' + this.lownet2[z].lastname;
        } else {

          this.net2String += ('\n' + this.lownet2[z].firstname + ' ' + this.lownet2[z].lastname);
        }
        this.moneyForm.patchValue({ net2: this.net2String });
        z++;
      } else if (winner.idgame.idgame === 6) {
        let selected: IGolfer;
        this.golfers.forEach(element => {
          if (element.idgolfer === winner.idgolfer.idgolfer) {
            selected = element;
          }
        });
        this.moneyForm.patchValue({ longdrive: selected });
      } else if (winner.idgame.idgame === 7) {
        let selected: IGolfer;
        this.golfers.forEach(element => {
          if (element.idgolfer === winner.idgolfer.idgolfer) {
            selected = element;
          }
        });
        this.moneyForm.patchValue({ longputt: selected });
      }

    });

    if (this.games[0].amount > 0) {
      this.moneyForm.controls['skintext'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['skintext'].setValidators(null);
    }
    if (this.games[1].amount > 0) {
      this.moneyForm.controls['pin1_1'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['pin1_1'].setValidators(null);
    }
    if (this.games[2].amount > 0) {
      this.moneyForm.controls['pin2_1'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['pin2_1'].setValidators(null);
    }
    if (this.games[3].amount > 0) {
      this.moneyForm.controls['net1'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['net1'].setValidators(null);
    }
    if (this.games[4].amount > 0) {
      this.moneyForm.controls['net2'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['net2'].setValidators(null);
    }
    if (this.games[5].amount > 0) {
      this.moneyForm.controls['longdrive'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['longdrive'].setValidators(null);
    }
    if (this.games[6].amount > 0) {
      this.moneyForm.controls['longputt'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['longputt'].setValidators(null);
    }
    if (this.games[7].amount > 0) {
      this.moneyForm.controls['pin1_2'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['pin1_2'].setValidators(null);
    }
    if (this.games[8].amount > 0) {
      this.moneyForm.controls['pin2_2'].setValidators(Validators.required);
    } else {
      this.moneyForm.controls['pin2_2'].setValidators(null);
    }

    this.moneyForm.controls['skintext'].updateValueAndValidity();
    this.moneyForm.controls['pin1_1'].updateValueAndValidity();
    this.moneyForm.controls['pin2_1'].updateValueAndValidity();
    this.moneyForm.controls['pin1_2'].updateValueAndValidity();
    this.moneyForm.controls['pin2_2'].updateValueAndValidity();
    this.moneyForm.controls['net1'].updateValueAndValidity();
    this.moneyForm.controls['net2'].updateValueAndValidity();
    this.moneyForm.controls['longdrive'].updateValueAndValidity();
    this.moneyForm.controls['longputt'].updateValueAndValidity();

    // this.moneyForm..controls("skintext").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyForm.get("pin1").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyForm.get("pin2").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyForm.get("net1").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyForm.get("net2").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyForm.get("longdrive").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyForm.get("longputt").updateValueAndValidity({emitEvent: false, onlySelf: true});
    // this.moneyform

  }

  calcLownet(): void {
    const year  = +localStorage.getItem('year');

    this.moneyService.calculateLowNet(year, this.week).subscribe(lownet => this.populateLowNet(lownet),
      (error: any) => this.errorMessage = <any>error
    );

  }

  populateLowNet(lownet: any[]): void {
    this.lownet1.length = 0;
    this.lownet2.length = 0;
    const net1: IGolfer[] = lownet[0];
    const net2: IGolfer[] = lownet[1];
    this.net1String = undefined;
    this.net2String = undefined;
    net1.forEach((golfer, index) => {
      this.lownet1[index] = golfer;
      if (this.net1String === undefined) {
        this.net1String = golfer.firstname + ' ' + golfer.lastname;
      } else {
        this.net1String += ('\n' + golfer.firstname + ' ' + golfer.lastname);

      }
    });
    net2.forEach((golfer, index) => {
      this.lownet2[index] = golfer;
      if (this.net2String === undefined) {
        this.net2String = golfer.firstname + ' ' + golfer.lastname;
      } else {
        this.net2String += ('\n' + golfer.firstname + ' ' + golfer.lastname);

      }
    });


    this.moneyForm.patchValue({ net1: this.net1String });
    this.moneyForm.patchValue({ net2: this.net2String });

  }

  calcSkins(): void {
    const year  = +localStorage.getItem('year');

    this.moneyService.calculateSkins(year, this.week).subscribe(skins => this.populateSkins(skins),
      (error: any) => this.errorMessage = <any>error
    );
  }

  populateSkins(skins: IGolfer[]): void {
    this.skins.length = 0;
    this.skinsString = undefined;
    skins.forEach((skin, index) => {
      this.skins[index] = skin;
      if (this.skinsString === undefined) {
        this.skinsString = this.skins[index].firstname + ' ' + this.skins[index].lastname;
      } else {
        this.skinsString += ('\n' + this.skins[index].firstname + ' ' + this.skins[index].lastname);

      }
    });

    this.moneyForm.patchValue({ skintext: this.skinsString });

  }


  saveMoney(): void {
    this.winners.length = 0;
    const year  = +localStorage.getItem('year');

    if (confirm('Saving Winners will remove any previously saved data')) {
      let x = 0;
      if (this.games[0].amount > 0) {
        this.skins.forEach(skin => {
          this.winners[x] = this.moneyService.initializeMoney();
          this.winners[x].idgolfer = skin;
          this.winners[x].amount = (+this.games[0].amount / +this.skins.length);
          this.winners[x].idgame = this.games[0];
          this.winners[x].idweek = this.week;
          this.winners[x].year = year;
          x++;
        });
      }
      if (this.games[1].amount > 0) {
        this.winners[x] = this.moneyService.initializeMoney();
        this.winners[x].idgolfer = this.moneyForm.get('pin1_1').value;
        this.winners[x].amount = +this.games[1].amount;
        this.winners[x].idgame = this.games[1];
        this.winners[x].idweek = this.week;
        this.winners[x].year = year;
        x++;
      }
      if (this.games[2].amount > 0) {
        this.winners[x] = this.moneyService.initializeMoney();
        this.winners[x].idgolfer = this.moneyForm.get('pin2_1').value;
        this.winners[x].amount = +this.games[2].amount;
        this.winners[x].idgame = this.games[2];
        this.winners[x].idweek = this.week;
        this.winners[x].year = year;
        x++;
      }
      if (this.games[3].amount > 0) {
        this.lownet1.forEach(lownet => {
          this.winners[x] = this.moneyService.initializeMoney();
          this.winners[x].idgolfer = lownet;
          this.winners[x].amount = (+this.games[3].amount / +this.lownet1.length);
          this.winners[x].idgame = this.games[3];
          this.winners[x].idweek = this.week;
          this.winners[x].year = year;
          x++;
        });
      }
      if (this.games[4].amount > 0) {
        this.lownet2.forEach(lownet => {
          this.winners[x] = this.moneyService.initializeMoney();
          this.winners[x].idgolfer = lownet;
          this.winners[x].amount = (+this.games[4].amount / +this.lownet2.length);
          this.winners[x].idgame = this.games[4];
          this.winners[x].idweek = this.week;
          this.winners[x].year = year;
          x++;
        });
      }
      if (this.games[5].amount > 0) {
        this.winners[x].idgolfer = this.moneyForm.get('longdrive').value;
        this.winners[x].amount = +this.games[5].amount;
        this.winners[x].idgame = this.games[5];
        this.winners[x].idweek = this.week;
        this.winners[x].year = year;
        x++;
      }
      if (this.games[6].amount > 0) {
        this.winners[x] = this.moneyService.initializeMoney();
        this.winners[x].idgolfer = this.moneyForm.get('longputt').value;
        this.winners[x].amount = +this.games[6].amount;
        this.winners[x].idgame = this.games[6];
        this.winners[x].idweek = this.week;
        this.winners[x].year = year;
        x++;
      }
      if (this.games[7].amount > 0) {
        this.winners[x] = this.moneyService.initializeMoney();
        this.winners[x].idgolfer = this.moneyForm.get('pin1_2').value;
        this.winners[x].amount = +this.games[7].amount;
        this.winners[x].idgame = this.games[7];
        this.winners[x].idweek = this.week;
        this.winners[x].year = year;
        x++;
      }
      if (this.games[8].amount > 0) {
        this.winners[x] = this.moneyService.initializeMoney();
        this.winners[x].idgolfer = this.moneyForm.get('pin2_2').value;
        this.winners[x].amount = +this.games[8].amount;
        this.winners[x].idgame = this.games[8];
        this.winners[x].idweek = this.week;
        this.winners[x].year = year;
        x++;
      }
      //      console.log('Skins are ' + JSON.stringify(this.winners));

      this.scoreService.calcPoints(this.week, year);
      console.log('going to save score now');
      this.moneyService.saveWinners(this.winners, this.week).subscribe(
        () => this.onSaveComplete(),
        (error: any) => this.errorMessage = <any>error
      );
    }

  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.router.navigate(['/authenticated/dashboard']);
    // this.golferForm.reset();
    // this.router.navigate(['/registerResponse']);
  }

}
