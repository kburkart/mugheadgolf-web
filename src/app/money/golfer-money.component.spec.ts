import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GolferMoneyComponent } from './golfer-money.component';

describe('GolferMoneyComponent', () => {
  let component: GolferMoneyComponent;
  let fixture: ComponentFixture<GolferMoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GolferMoneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GolferMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
