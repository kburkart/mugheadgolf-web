import { Component, OnInit, Input, EventEmitter, OnDestroy, HostListener } from '@angular/core';
import { MoneyService } from './money.service';
import { IGolfer } from '../golfer/golfer';
import { IGolferMoney } from './golfer-money';
import { SettingsService } from '../settings/settings.service';
import { IGames } from '../settings/games';

@Component({
  selector: 'app-golfer-money',
  templateUrl: './golfer-money.component.html',
  styleUrls: ['./golfer-money.component.css']
})
export class GolferMoneyComponent implements OnInit {

  pageTitle = 'Golfer Money Winnings';
  errorMessage: string;

  golferMoney: IGolferMoney[];
  games: IGames[];
  numbers: number[];

  constructor(private moneyService: MoneyService,
    private settingsService: SettingsService) { }

  ngOnInit() {
    const year  = +localStorage.getItem('year');
    this.moneyService.getGolfersMoney(year)
    .subscribe(golferMoney => this.populateData(golferMoney),
      error => this.errorMessage = <any>error);
  }
  populateData(golferMoney: IGolferMoney[]): void {
    this.golferMoney = golferMoney;
    this.settingsService.getGames()
    .subscribe(games => this.games = games,
      error => this.errorMessage = <any>error);
  }

}
