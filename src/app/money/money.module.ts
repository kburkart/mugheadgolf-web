import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoneyComponent } from './money.component';
import { AddMoneyComponent } from './add-money.component';
import { GolferMoneyComponent } from './golfer-money.component';
import { WeeklyMoneyComponent } from './weekly-money.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [MoneyComponent, AddMoneyComponent, GolferMoneyComponent, WeeklyMoneyComponent]
})
export class MoneyModule { }
