export interface IGolferMoney {
    id: number;
    idgolfer: number;
    golfername: string;
    skins: number;
    pin1_v: number;
    pin2_v: number;
    lownet1: number;
    lownet2: number;
    longdrive: number;
    longputt: number;
    pin1_lr: number;
    pin2_lr: number;
    total: number;
}
