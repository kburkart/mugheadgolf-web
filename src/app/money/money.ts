import { IGames } from '../settings/games';
import { IGolfer } from '../golfer/golfer';

export interface IMoney {
    idmoney: number;
    amount: number;
    idgame: IGames;
    idgolfer: IGolfer;
    idweek: number;
    year: number;
}
