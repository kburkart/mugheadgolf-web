import { IGolfer } from './golfer';
import { IHandicap } from './handicap';

export interface IHandicapData {
    golfer: IGolfer;
    handicaps: IHandicap[];
}
