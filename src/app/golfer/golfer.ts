export interface IGolfer {
    idgolfer: number;
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    email: string;
    spouse: string;
    active: number;
    phone: string;
    currenthandicap: number;
    admin: boolean;
    autorefresh: boolean;

}
