import { Component, OnInit } from '@angular/core';
import { GolferService } from './golfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calc-handicaps',
  templateUrl: './calc-handicaps.component.html',
  styleUrls: ['./calc-handicaps.component.css']
})
export class CalcHandicapsComponent implements OnInit {

  errorMessage: string;

  constructor(private golferService: GolferService,
    private router: Router) { }

  ngOnInit() {
    this.calculateHandicaps();
  }

  calculateHandicaps(): void {
    this.golferService.calcHandicaps().subscribe(
      () => this.onSendComplete(),
      (error: any) => this.errorMessage = <any>error
      );
  }

  onSendComplete(): void {
    this.router.navigate(['/authenticated/handicaps']);
  }

}

