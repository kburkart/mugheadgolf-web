import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import { Observable } from 'rxjs/Observable';
import { GenericValidator } from '../shared/generic-validator';


import { Subscription } from 'rxjs/Subscription';

import { IGolfer } from './golfer';
import { GolferService } from './golfer.service';

function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    const emailControl = c.get('email');
    const confirmControl = c.get('confirmEmail');

    if (emailControl.pristine || confirmControl.pristine) {
        return null;
    }
    if (emailControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}

function passwordMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    const passwordControl = c.get('password');
    const confirmControl = c.get('confirmPassword');

    if (passwordControl.pristine || confirmControl.pristine) {
        return null;
    }

    if (passwordControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}

@Component({
    selector: 'app-add-golfer',
    templateUrl: './add-golfer.component.html',
    styleUrls: ['./add-golfer.component.css']
})
export class AddGolferComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    errorMessage: string;
    golferForm: FormGroup;
    golfer: IGolfer;
    emailMessage: String;
    pageTitle: string;

    private sub: Subscription;
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;


    // Use with the generic validation message class
    displayMessage: { [key: string]: string } = {};
    isRegister = true;


    constructor(private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private golferService: GolferService) {

        // Defines all of the validation messages for the form.
        // These could instead be retrieved from a file or database.
        this.validationMessages = {
            email: {
                required: 'Email is a required field.',
                email: 'Must enter a valid email.'
            },
            confirmEmail: {
                required: 'Confirmation Email is a required field.'
            },
            emailGroup: {
                match: 'Confirmation Email does not match.'
            },
            password: {
                required: 'Password is required a required field.',
                pattern: 'Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character'
            },
            passwordGroup: {
                match: 'Confirmation Password does not match.'
            },
            firstname: {
                required: 'First name is a required field.'
            },
            lastname: {
                required: 'Last name is a required field.'
            },
            dob: {
                required: 'Date of Birth is a required field',
                pattern: 'Date of Birth needs to be in the form MM/DD/YYYY.'
            },
            zip: {
                pattern: 'Use only 5 digit Zip code .'
            },
            username: {
                required: 'Username is a required field.'
            }
            ,
            phone: {
                required: 'Phone Number is a required field.',
                pattern: 'Phone number does not meet the pattern 1234567890, only use digits.'
            }
        };
        // Define an instance of the validator for use with this form,
        // passing in this form's set of validation messages.
        this.genericValidator = new GenericValidator(this.validationMessages);

    }

    ngOnInit(): void {
        this.golferForm = this.fb.group({
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            address: '',
            city: '',
            state: '',
            zip: ['', Validators.pattern('\\d{5}')],
            phone: ['', [Validators.required, Validators.pattern('\\d{10}')]],
            spouse: '',
            currenthandicap: '',
            // dob: ['', [Validators.required,
            //   Validators.pattern('\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])')]],
            emailGroup: this.fb.group({
                email: ['', [Validators.required, Validators.email]],
                confirmEmail: ['', Validators.required],
            }, { validator: emailMatcher }),
            username: ['', Validators.required],
            passwordGroup: this.fb.group({
                password: ['', [Validators.required,
           /* Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}')*/]],
                confirmPassword: ['', Validators.required],
            }, { validator: passwordMatcher }),
            active: '',
        });

        // Read the product Id from the route parameter
        this.sub = this.route.params.subscribe(
            params => {
                const id = +params['id'];
                if (id < 0) {
                    this.getGolfer(0);
                    this.isRegister = true;
                } else if (id === 0) {
                    this.isRegister = false;
                    this.getGolfer(0);
                } else {
                    this.isRegister = false;
                    this.getGolfer(id);
                }

            }
        );

    }

    // setMessage(c: AbstractControl): void {
    //     this.emailMessage = '';
    //     if ((c.touched || c.dirty) && c.errors) {
    //         this.emailMessage = Object.keys(c.errors).map(key =>
    //             this.validationMessages[key]).join(' ');
    //     }
    // }

    ngOnDestroy(): void {
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        const controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.golferForm.valueChanges, ...controlBlurs).debounceTime(1000).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.golferForm);
        });
    }

    saveGolfer(): void {

        if (this.golferForm.dirty && this.golferForm.valid) {
            // Copy the form values over the golfer object values

            const p = Object.assign({}, this.golfer, this.golferForm.value);
            const eForm = this.golferForm.get('emailGroup');
            const pForm = this.golferForm.get('passwordGroup');

            p.email = eForm.get('email').value;
            p.password = pForm.get('password').value;
            this.golferService.saveGolfer(p, this.isRegister)
                .subscribe(
                    () => this.onSaveComplete(),
                    (error: any) => this.errorMessage = <any>error
                );
        } else if (!this.golferForm.dirty) {
            this.onSaveComplete();
        }
    }

onSaveComplete(): void {
        // Reset the form to clear the flags
        this.golferForm.reset();
        if (this.isRegister) {
            this.router.navigate(['/registerResponse']);
        } else {
            this.router.navigate(['/authenticated/dashboard']);

        }
    }

    getGolfer(id: number): void {
        this.golferService.getGolfer(id)
            .subscribe(
                (golfer: IGolfer) => this.onGolferRetrieved(golfer),
                (error: any) => this.errorMessage = <any>error
            );
    }

    onGolferRetrieved(golfer: IGolfer): void {
        if (this.golferForm) {
            this.golferForm.reset();
        }
        this.golfer = golfer;

        if (this.golfer.idgolfer === 0) {
            if (this.isRegister) {
                this.pageTitle = 'Golfer Registration';
            } else {
                this.pageTitle = 'Add Golfer';
            }
        } else {
            this.pageTitle = 'Edit ' + this.golfer.firstname + ' ' + this.golfer.lastname;
        }

        this.golferForm.patchValue({
            firstname: this.golfer.firstname,
            lastname: this.golfer.lastname,
            address: this.golfer.address,
            city: this.golfer.city,
            state: this.golfer.state,
            zip: this.golfer.zip,
            phone: this.golfer.phone,
            spouse: this.golfer.spouse,
            username: this.golfer.username,
            active: this.golfer.active
        });
        const eForm = this.golferForm.get('emailGroup');
        eForm.patchValue({
            email: this.golfer.email,
            confirmEmail: this.golfer.email
        });
        const pForm = this.golferForm.get('passwordGroup');
        pForm.patchValue({
            password: this.golfer.password,
            confirmPassword: this.golfer.password

        });

    }
}
