import { IGolfer } from './golfer';

export interface IHandicap {
   idhandicap: number;
   year: number;
   idweek: number;
   idgolfer: IGolfer;
   handicap: number; 
}

