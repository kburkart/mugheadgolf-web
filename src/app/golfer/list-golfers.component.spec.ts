import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGolfersComponent } from './list-golfers.component';

describe('ListGolfersComponent', () => {
  let component: ListGolfersComponent;
  let fixture: ComponentFixture<ListGolfersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGolfersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGolfersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
