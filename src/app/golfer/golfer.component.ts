import { Component, OnInit } from '@angular/core';
import { GolferService } from './golfer.service';
import { IGolfer } from './golfer';
import { Criteria } from '../sortable/criteria';

@Component({
  selector: 'app-golfer',
  templateUrl: './golfer.component.html',
  styleUrls: ['./golfer.component.css']
})
export class GolferComponent implements OnInit {

  pageTitle = 'Golfer List';
  errorMessage: string;
  criteria: Criteria;

  golfers: IGolfer[];
  constructor(private golferService: GolferService) { }

  ngOnInit(): void {
    this.golferService.getGolfers()
      .subscribe(golfers => this.populateGolfers(golfers),
        error => this.errorMessage = <any>error);
  }

  populateGolfers(golfers: IGolfer[]) {
    this.golfers = golfers;
    this.golfers = this.getSortedGolfers({sortColumn: 'currenthandicap', sortDirection: 'desc', tableName: '1' });
  }

  onSorted($event, table: number) {
    this.getSortedGolfers($event);
  }

  getSortedGolfers(criteria: Criteria): IGolfer[] {
    return this.golfers.sort((a, b) => {
      let rc = 0;
      if (criteria.sortDirection === 'desc') {

        if (a[criteria.sortColumn] < b[criteria.sortColumn]) {
          rc = -1;
        } else if (a[criteria.sortColumn] > b[criteria.sortColumn]) {
          rc = 1;
        } else {
          rc = 0;
        }
      } else {
        if (a[criteria.sortColumn] < b[criteria.sortColumn]) {
          rc = 1;
        } else if (a[criteria.sortColumn] > b[criteria.sortColumn]) {
          rc = -1;
        } else {
          rc = 0;
        }
      }
      return rc;
    });
  }
}

