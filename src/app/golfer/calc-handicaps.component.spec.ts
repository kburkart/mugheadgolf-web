import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcHandicapsComponent } from './calc-handicaps.component';

describe('CalcHandicapsComponent', () => {
  let component: CalcHandicapsComponent;
  let fixture: ComponentFixture<CalcHandicapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcHandicapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcHandicapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
