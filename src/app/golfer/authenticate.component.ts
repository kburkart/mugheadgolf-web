import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { GolferService } from './golfer.service';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.css']
})
export class AuthenticateComponent implements OnInit, OnDestroy {

  errorMessage: string;
  private sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,  private golferService: GolferService
  ) { }

  ngOnInit() {
    // Read the product Id from the route parameter
    this.sub = this.route.params.subscribe(
      params => {
        const token = params['token'];
        this.authenticate(token);
      }
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  authenticate(token: string): void {
    this.golferService.authenticate(token)
      .subscribe(
      () => this.onAuthComplete(),
      (error: any) => this.errorMessage = <any>error
      );
  }

  onAuthComplete(): void {
    // Reset the form to clear the flags
    this.router.navigate(['/activationNotice']);
  }

}


