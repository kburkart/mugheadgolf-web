import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GolferService } from '../golfer/golfer.service';
import { IGolfer } from '../golfer/golfer';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-golfer',
  templateUrl: './edit-golfer.component.html',
  styleUrls: ['./edit-golfer.component.css']
})
export class EditGolferComponent implements OnInit {
  golfers: IGolfer[];
  errorMessage: string;
  golferForm: FormGroup;
  pageTitle: string;
  selectedGolfer: IGolfer;
  constructor(private fb: FormBuilder,
    private router: Router,
    private golferService: GolferService) {

    this.pageTitle = 'Edit Golfer';
  }

  ngOnInit() {
    this.golferForm = this.fb.group({
      golferlist: ''
    });
    this.golferForm.get('golferlist').valueChanges.subscribe((value: IGolfer) => this.editGolfer(value));
    this.getListOfGolfers();
  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
  }

  editGolfer(golfer: IGolfer) {
    this.router.navigate(['authenticated/addGolfer/' + golfer.idgolfer]);
  }
}
