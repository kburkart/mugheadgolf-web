import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { IGolfer } from './golfer';
import { IHandicapData } from './handicap-data';

@Injectable()
export class GolferService {

  private baseUrl = environment.url + 'golfer/';
  private registerUrl = this.baseUrl + 'register';
  private authUrl = this.baseUrl + 'confirmemail';
  private handicapUrl = environment.url + 'handicap/';

  constructor(private http: Http) { }


  getHandicaps(year: number): Observable<IHandicapData[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });


    const url = this.handicapUrl + 'year/' + year;
    return this.http.get(url, options)
      .map(response => response.json())
      // .do(data => console.log('getHandicaps: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  calcHandicaps(): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });


    const url = this.handicapUrl + 'calculate';
    return this.http.get(url, options)
      .map(response => response.text())
      // .do(data => console.log('getHandicaps: ' + JSON.stringify(data)))
      .catch(this.handleError);

  }

  getGolfers(): Observable<IGolfer[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });


    const url = this.baseUrl + 'findall';

    return this.http.get(url, options)
      .map(response => response.json())
//      .do(data => console.log('getGolfers: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }


  getGolfer(id: number): Observable<IGolfer> {
    if (id === 0) {
      return Observable.create((observer: any) => {
        observer.next(this.initializeGolfer());
        observer.complete();
      });
    }

    const url = this.baseUrl + 'find/' + id;
    return this.http.get(url)
      .map(response => response.json())
 //  .do(data => console.log('getGolfer: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }


  authenticate(token: string): Observable<Response> {
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // headers.append('Access-Control-Allow-Origin', '*');
    // const options = new RequestOptions({ headers: headers });
    const url = `${this.authUrl}/${token}`;
    return this.http.get(url)
      // .do(data => console.log('authenticate: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }



  deleteGolfer(id: number): Observable<Response> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.baseUrl + id;
    return this.http.delete(url, options)
      // .do(data => console.log('deleteGolfer: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveGolfer(golfer: IGolfer, isRegister: boolean): Observable<IGolfer> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    let url = this.baseUrl + 'add' ;
    if (golfer.idgolfer === 0) {
      golfer.idgolfer = undefined;
    }
    if (isRegister) {
      url = this.registerUrl;
    }

    return this.http.post(url, golfer, options)
    .map(this.extractData)
    // .do(data => console.log('createGolfer: ' + JSON.stringify(data)))
    .catch(this.handleError);
  }


  private extractData(response: Response) {
    const body = response.text;
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  initializeGolfer(): IGolfer {
    // Return an initialized object
    return {
      idgolfer: 0,
      firstname: null,
      lastname: null,
      username: null,
      password: null,
      address: null,
      city: null,
      state: null,
      zip: null,
      email: null,
      spouse: null,
      active: null,
      phone: null,
      currenthandicap: null,
      admin: false,
      autorefresh: true
    };
  }

}
