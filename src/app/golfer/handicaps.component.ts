import { Component, OnInit } from '@angular/core';
import { GolferService } from './golfer.service';
import { IGolfer } from './golfer';
import { IHandicapData } from './handicap-data';
import { SettingsService } from '../settings/settings.service';
import { ISettings } from '../settings/settings';
import { Criteria } from '../sortable/criteria';
import { SortService } from '../sortable/sort.service';

@Component({
  selector: 'app-handicaps',
  templateUrl: './handicaps.component.html',
  styleUrls: ['./handicaps.component.css']
})
export class HandicapsComponent implements OnInit {

  pageTitle = 'Golfer Weekly Handicaps';
  errorMessage: string;

  handicapData: IHandicapData[];
  settings: ISettings[];
  numbers: number[];
  criteria: Criteria;
  currentWeek = 0;

  constructor(private golferService: GolferService,
              private settingsService: SettingsService,
              private sortService: SortService) { }

  ngOnInit(): void {
    const year  = +localStorage.getItem('year');
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.settingsService.getSettings(year).subscribe(settings => this.populateData(year, settings.totalweeks),
      error => this.errorMessage = <any>error);


    }

  populateData(year: number, numWeeks: number) {
    this.numbers = Array.from(Array(numWeeks)).map((x, i) => i );
    this.golferService.getHandicaps(year)
          .subscribe(handicapData => this.populateHandicaps(handicapData),
            error => this.errorMessage = <any>error);
  }

  populateHandicaps(handicapData: IHandicapData[]) {
    this.handicapData = handicapData;
    this.handicapData = this.getSortedHandicaps({sortColumn: '' + (this.currentWeek - 1), sortDirection: 'desc', tableName: '1' });
   }

   onSorted($event, table: number) {
    this.getSortedHandicaps($event);
}

getSortedHandicaps(criteria: Criteria): IHandicapData[] {
    return this.handicapData.sort((a, b) => {
      let rc = 0;
      if (criteria.sortDirection === 'desc') {

        if (a.handicaps[criteria.sortColumn].handicap === null) {
          rc =  1;
        } else if (b.handicaps[criteria.sortColumn].handicap === null) {
          rc =  -1;
        } else if (a.handicaps[criteria.sortColumn].handicap < b.handicaps[criteria.sortColumn].handicap) {
          rc =  -1;
        } else if (a.handicaps[criteria.sortColumn].handicap > b.handicaps[criteria.sortColumn].handicap) {
          rc =  1;
        } else {
          rc =  0;
        }
      } else {
        if (b.handicaps[criteria.sortColumn].handicap === null) {
          rc =  -1;
        } else if (a.handicaps[criteria.sortColumn].handicap === null) {
          rc =  1;
        } else if (a.handicaps[criteria.sortColumn].handicap < b.handicaps[criteria.sortColumn].handicap) {
          rc =  1;
        } else if (a.handicaps[criteria.sortColumn].handicap > b.handicaps[criteria.sortColumn].handicap) {
          rc =  -1;
        } else {
          rc = 0;
        }
      }
      return rc;
    });
}

}
