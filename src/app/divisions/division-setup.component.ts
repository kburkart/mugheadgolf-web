import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { DivisionsService } from './divisions.service';
import { DivisionGolferService } from './division-golfer.service';
import { GolferService } from '../golfer/golfer.service';

import { IGolfer } from '../golfer/golfer';

import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { IDivision } from './division';
import { IDivisionGolfer } from './division-golfer';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';

@Component({
  selector: 'app-division-setup',
  templateUrl: './division-setup.component.html',
  styleUrls: ['./division-setup.component.css']
})
export class DivisionSetupComponent implements OnInit {

  pageTitle = 'Division Setup';
  public numberOfDivisions: number;
  divisionForm: FormGroup;
  errorMessage: string;
  divisionNames: string[] = ['Division 1', 'Division 2',
    'Division 3', 'Division 4', 'Division 5', 'Division 6', 'Division 7', 'Division 8'];

  public golfers: IGolfer[];
  public division: IDivision;
  public divisionGolfer: IDivisionGolfer;
  public divisionNumbers: any[] = [1,2,3,4,5,6,7,8];

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private divisionsService: DivisionsService,
    private divisionGolferService: DivisionGolferService,
    private golferService: GolferService) { }

  ngOnInit(): void {
    this.getListOfGolfers();
    this.divisionForm = this.fb.group({
      totalDivisions: ['', Validators.required],
      divisions: this.fb.array([])
    });


    this.getDivisions();

  }

  getDivisions() {
    const year  = +localStorage.getItem('year');
    this.divisionsService.getDivisionsByYear(year)
      .subscribe(
        (divisions: IDivision[]) => this.populateDivisions(divisions),
        (error: any) => this.errorMessage = <any>error
      );
  }

  populateDivisions(divisions: IDivision[]) {
    const size = divisions.length;
    this.numberOfDivisions = size;
    if (size === 0) {
      this.getDivisionFromService(0);
      this.getDivisionGolferFromService(0);
    } else {
      let control = <FormArray>this.divisionForm.get('totalDivisions');     
      control.setValue(this.divisionNumbers[+size-1]);
      control = <FormArray>this.divisionForm.get('divisions');     
      this.purgeForm(control);
      for (let i = 0; i <= size; i++) {
        control.push(this.buildDivisionWithInfo(i, divisions[i]));
        this.populateDivisionGolfers(i);
      }
    }
  }

  populateDivisionGolfers(index: number) : void{
    let division = this.divisionNumbers[index];
    this.divisionGolferService.getDivisionGolfersByDivision(division)
      .subscribe(
        (divisionGolfers: IDivisionGolfer[]) => this.addGolfersToDivisions(divisionGolfers, index),
        (error: any) => this.errorMessage = <any>error
      );
  }

  addGolfersToDivisions(divisionGolfers: IDivisionGolfer[], division: number){
    divisionGolfers.forEach(golfer => this.addGolferToDivision(division, golfer));
  }

  getDivisionFromService(id: number): void {
    this.divisionsService.getDivision(id)
      .subscribe(
        (division: IDivision) => this.onDivisionRetrieved(division),
        (error: any) => this.errorMessage = <any>error
      );
  }


  getDivisionGolferFromService(id: number): void {
    this.divisionGolferService.getDivisionGolfer(id)
      .subscribe(
        (divisionGolfer: IDivisionGolfer) => this.onDivisionGolferRetrieved(divisionGolfer),
        (error: any) => this.errorMessage = <any>error
      );
  }

  onSelect(number) {
    this.numberOfDivisions = number;
    const control = <FormArray>this.divisionForm.get('divisions');
    this.purgeForm(control);
    for (let i = 1; i <= number; i++) {
      control.push(this.buildDivision(i));
    }
  }


  buildDivision(index: number): FormGroup {
    let newGroup: FormGroup;
    newGroup = this.fb.group({
      id: index,
      name: ['', Validators.required],
      golferList: ['', Validators.required],
      golfers: this.fb.array([])
    });

    newGroup.get('name').valueChanges.subscribe(value => this.changeDivisionName(index, value));
    return newGroup;
  }

  buildDivisionWithInfo(index: number, division: IDivision): FormGroup {
    let newGroup: FormGroup;
    newGroup = this.fb.group({
      id: index,
      name: ['', Validators.required],
      golferList: ['', Validators.required],
      golfers: this.fb.array([])
    });
    this.divisionNames[index] = division.name;
    newGroup.get('name').setValue(division.name);
    newGroup.get('name').valueChanges.subscribe(value => this.changeDivisionName(index, value));
    return newGroup;
  }

  changeDivisionName(id: number, value) {
    this.divisionNames[id - 1] = value;
  }

  buildGolfers(golfer: IGolfer): FormGroup {
    return this.fb.group({
      id: new FormControl({ value: golfer.idgolfer, disabled: true }),
      golfer: new FormControl({ value: golfer.firstname + ' ' + golfer.lastname, disabled: true })
    });
  }

  addGolfer(i) {
    // control refers to your formarray
    let golfer: IGolfer;
    const control = this.divisionForm.get(`divisions.${i}.golferList`);
    const arr: FormArray = this.divisionForm.get(`divisions.${i}.golfers`) as FormArray;
    golfer = <IGolfer>control.value;
    arr.push(this.buildGolfers(golfer));
  }

  addGolferToDivision(i, golfer: IDivisionGolfer) {
    // control refers to your formarray
    const arr: FormArray = this.divisionForm.get(`divisions.${i}.golfers`) as FormArray;
    const numGolfers = this.golfers.length;
    let index = 0;
    for(let x = 0; x < numGolfers; x++){
      if(golfer.idgolfer === this.golfers[x].idgolfer){
        index = x;
        break;
      }
    }
    arr.push(this.buildGolfers(this.golfers[index]));
  }

  removeGolfer(i, j) {
    // control referrs to your formarray
    const arr: FormArray = this.divisionForm.get(`divisions.${i}.golfers`) as FormArray;
    // remove the chosen row
    arr.removeAt(j);
  }

  getDivision(form) {
    // console.log(form.get('divisions').controls);
    return form.controls.divisions.controls;
  }
  getGolfers(form) {
    // console.log(form.controls.golfers.controls);
    return form.controls.golfers.controls;
  }

  purgeForm(form: FormArray) {
    while (0 !== form.length) {
      form.removeAt(0);
    }
  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
  }

  onDivisionRetrieved(division: IDivision): void {
    this.division = division;
  }

  onDivisionGolferRetrieved(divisionGolfer: IDivisionGolfer): void {
    this.divisionGolfer = divisionGolfer;
  }


  get divisions(): FormArray {
    return this.divisionForm.get(`divisions`) as FormArray;
  }

  getDivGolfers(i) {
    return this.divisions.controls[i].get('golfers') as FormArray;
  }

  saveDivisionGolfers(i: number, division: IDivision) {
    const divGolfers = this.getDivGolfers(i);
    const year  = +localStorage.getItem('year');
    for (let j = 0; j < divGolfers.length; j++) {
      this.divisionGolfer.iddivision = division.iddivision;
      this.divisionGolfer.idgolfer = divGolfers.at(j).value.id;
      this.divisionGolfer.iddivisiongolfer = 0;
      this.divisionGolfer.year = year;
      this.divisionGolferService.saveDivisionGolfer(this.divisionGolfer).subscribe(
        (divisionGolfer: IDivisionGolfer) => this.onDivisionGolferRetrieved(divisionGolfer),
        (error: any) => this.errorMessage = <any>error
      );
    }
  }

  saveDivisions(): void {
    if (this.divisionForm.dirty && this.divisionForm.valid) {
      for (let i = 0; i < this.divisions.length; i++) {
        this.division = this.divisions.value[i];
        this.division.iddivision = 0;
        // this.division.name = this.divisions.at(i).value.name;
        this.divisionsService.saveDivision(this.division).subscribe(
          (division: IDivision) => this.saveDivisionGolfers(i, division),
          (error: any) => this.errorMessage = <any>error
        );
      }
      this.onSaveComplete();
    }
  }


  onSaveComplete(): void {
    // Reset the form to clear the flags
    //this.divisionForm.reset();
    this.router.navigate(['authenticated/dashboard']);
  }
}
