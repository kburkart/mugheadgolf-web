import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionSetupComponent } from './division-setup.component';

describe('DivisionSetupComponent', () => {
  let component: DivisionSetupComponent;
  let fixture: ComponentFixture<DivisionSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivisionSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
