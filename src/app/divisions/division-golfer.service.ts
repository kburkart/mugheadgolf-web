import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { IDivision } from './division';
import { IDivisionGolfer } from './division-golfer';
import { environment } from '../../environments/environment';

@Injectable()
export class DivisionGolferService {

  private baseUrl = environment.url + 'divisionGolfer/';

  constructor(private http: Http) { }

  getDivisionGolfers(): Observable<IDivisionGolfer[]> {
    const url = this.baseUrl + 'findall';

    return this.http.get(url)
        .map(response =>  response.json())
     //   .do(data => console.log('getDivisionGolfers: ' + JSON.stringify(data)))
        .catch(this.handleError);
}

getDivisionGolfersByDivision(id: number): Observable<IDivisionGolfer[]> {
    const url = this.baseUrl + 'division/' +  id;
    return this.http.get(url)
        .map(response =>  response.json())
//        .do(data => console.log('getDivisionGolfer: ' + JSON.stringify(data)))
        .catch(this.handleError);
}

getDivisionGolfersByGolfer(id: number): Observable<IDivisionGolfer> {
    const year = localStorage.getItem('year');
    const url = this.baseUrl + 'golfer/' +  id + '/' + year;

    return this.http.get(url)
        .map(response =>  response.json())
//        .do(data => console.log('getDivisionGolfer: ' + JSON.stringify(data)))
        .catch(this.handleError);
}

getDivisionGolfer(id: number): Observable<IDivisionGolfer> {
    if (id === 0) {
    return Observable.create((observer: any) => {
        observer.next(this.initializeDivisionGolfer());
        observer.complete();
    });
    }
    const url = this.baseUrl + 'find/' +  id ;
    return this.http.get(url)
        .map(response =>  response.json())
        //.do(data => console.log('getDivisionGolfer: ' + JSON.stringify(data)))
        .catch(this.handleError);
}

deleteDivisionGolfer(id: number): Observable<Response> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.baseUrl + 'delete/' +  id ;
    return this.http.delete(url, options)
//        .do(data => console.log('deleteDivisionGolfer: ' + JSON.stringify(data)))
        .catch(this.handleError);
}

saveDivisionGolfer(DivisionGolfer: IDivisionGolfer): Observable<IDivisionGolfer> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'save';

    if (DivisionGolfer.iddivisiongolfer === 0) {
        DivisionGolfer.iddivisiongolfer = undefined;
    }
    return this.http.post(url, DivisionGolfer, options)
        .map(response =>  response.json())
//        .do(data => console.log('saveDivisionGolfer: ' + JSON.stringify(data)))
        .catch(this.handleError);
    
}

private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
}

initializeDivisionGolfer(): IDivisionGolfer {
    // Return an initialized object

    return {
        iddivisiongolfer: 0,
        iddivision: null,
        idgolfer: null,
        year: null
    };
}

}
