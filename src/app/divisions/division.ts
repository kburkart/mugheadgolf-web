export interface IDivision {
    iddivision: number;
    name: string;
    year: number
}
