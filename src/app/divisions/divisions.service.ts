import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { IDivision } from './division';
import { IDivisionGolfer } from './division-golfer';

@Injectable()
export class DivisionsService {
    private baseUrl = environment.url + 'division/';

    constructor(private http: Http) { }

    getDivisions(): Observable<IDivision[]> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'findall';

        return this.http.get(url, options)
            .map(response => response.json())
            //.do(data => console.log('getDivisions: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getDivisionsByYear(year: number): Observable<IDivision[]> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'year/' + year;

        return this.http.get(url, options)
            .map(response => response.json())
            //.do(data => console.log('getDivisions: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getDivision(id: number): Observable<IDivision> {
        if (id === 0) {
            return Observable.create((observer: any) => {
                observer.next(this.initializeDivision());
                observer.complete();
            });
        }
        const url = this.baseUrl + 'find/' + id;
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.get(url, options)
            .map(response => response.json())
            //.do(data => console.log('getDivision: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteDivision(id: number): Observable<Response> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        const url = this.baseUrl + 'delete/' + id;
        return this.http.delete(url, options)
            //.do(data => console.log('deleteDivision: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveDivision(Division: IDivision): Observable<IDivision> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'save';

        if (Division.iddivision === 0) {
            Division.iddivision = undefined;
        }
        return this.http.post(url, Division, options)
            .map(response => response.json())
//            .do(data => console.log('saveDivision: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    initializeDivision(): IDivision {
        // Return an initialized object
        return {
            iddivision: 0,
            name: null,
            year: (new Date()).getFullYear()
        };
    }

}
