export interface IWeeklyStats {
    id: number;
    idgolfer: number;
    name: string;
    idweek: number;
    year: number;
    albatros: number;
    eagle: number;
    birdie: number;
    par: number;
    bogey: number;
    doubleBogey: number;
    other: number;
}
