import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GolferService } from '../golfer/golfer.service';
import { IGolfer } from '../golfer/golfer';
import { StatsService } from './stats.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { IWeeklyStats } from './weekly-stats';
import { SortService } from '../sortable/sort.service';
import { Criteria } from '../sortable/criteria';

@Component({
  selector: 'app-stats',
  templateUrl: './weekly-stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class WeeklyStatsComponent implements OnInit {

  golfers: IGolfer[];
  errorMessage: string;
  weeklyStats: IWeeklyStats[];
  statsForm: FormGroup;
  pageTitle: string;
  selectedGolfer: IGolfer;
  criteria: Criteria;

  constructor(private fb: FormBuilder,
    private golferService: GolferService,
    private statsService: StatsService,
    private sortService: SortService) { }

  ngOnInit() {
    this.statsForm = this.fb.group({
      golferlist: ''
    });
    this.statsForm.get('golferlist').valueChanges.subscribe((value: IGolfer) => this.populateStats(value));
    this.getListOfGolfers();

  }

  populateStats(golfer: IGolfer) {
    this.pageTitle = 'Weekly Stats for ' + golfer.firstname + ' ' + golfer.lastname;
    this.selectedGolfer = golfer;
    this.getStats(golfer);
  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
  }

  getStats(golfer: IGolfer) {
    const year  = +localStorage.getItem('year');
    this.statsService.getGolfersStats(golfer.idgolfer, year).subscribe(
      (weeklyStats: IWeeklyStats[]) => this.onStatsRetrieved(weeklyStats),
      (error: any) => this.errorMessage = <any>error
    );
  }

  onStatsRetrieved(weeklyStats: IWeeklyStats[]): void {
    this.weeklyStats = weeklyStats;
    this.weeklyStats = this.getSortedStats({ sortColumn: 'week', sortDirection: 'desc', tableName: '1' });

  }
  onSorted($event, table: number) {
    this.getSortedStats($event);
  }
  getSortedStats(criteria: Criteria): IWeeklyStats[] {
    return this.weeklyStats.sort((a, b) => {
      let rc = 0;
      if (criteria.sortDirection === 'desc') {

        if (criteria.sortColumn === 'albatros') {
          if (a.albatros < b.albatros) {
            rc = -1;
          } else if (a.albatros > b.albatros) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'eagle') {
          if (a.eagle < b.eagle) {
            rc = -1;
          } else if (a.eagle > b.eagle) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'birdie') {
          if (a.birdie < b.birdie) {
            rc = -1;
          } else if (a.birdie > b.birdie) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'par') {
          if (a.par < b.par) {
            rc = -1;
          } else if (a.par > b.par) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'bogey') {
          if (a.bogey < b.bogey) {
            rc = -1;
          } else if (a.bogey > b.bogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'double') {
          if (a.doubleBogey < b.doubleBogey) {
            rc = -1;
          } else if (a.doubleBogey > b.doubleBogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'other') {
          if (a.other < b.other) {
            rc = -1;
          } else if (a.other > b.other) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'week') {
          if (a.idweek < b.idweek) {
            rc = -1;
          } else if (a.idweek > b.idweek) {
            rc = 1;
          } else {
            rc = 0;
          }
        }

      } else {
        if (criteria.sortColumn === 'albatros') {
          if (a.albatros > b.albatros) {
            rc = -1;
          } else if (a.albatros < b.albatros) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'eagle') {
          if (a.eagle > b.eagle) {
            rc = -1;
          } else if (a.eagle < b.eagle) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'birdie') {
          if (a.birdie > b.birdie) {
            rc = -1;
          } else if (a.birdie < b.birdie) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'par') {
          if (a.par > b.par) {
            rc = -1;
          } else if (a.par < b.par) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'bogey') {
          if (a.bogey > b.bogey) {
            rc = -1;
          } else if (a.bogey < b.bogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'double') {
          if (a.doubleBogey > b.doubleBogey) {
            rc = -1;
          } else if (a.doubleBogey < b.doubleBogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'other') {
          if (a.other > b.other) {
            rc = -1;
          } else if (a.other < b.other) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'week') {
          if (a.idweek > b.idweek) {
            rc = -1;
          } else if (a.idweek < b.idweek) {
            rc = 1;
          } else {
            rc = 0;
          }
        }
      }
      return rc;
    });
  }
}
