export interface IStats {
    id: number;
    idgolfer: number;
    name: string;
    year: number;
    albatros: number;
    eagle: number;
    birdie: number;
    par: number;
    bogey: number;
    doubleBogey: number;
    other: number;
}
