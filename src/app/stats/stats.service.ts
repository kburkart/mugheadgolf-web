import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { IWeeklyStats } from './weekly-stats';
import { IStatsPie } from './stats-pie'
import { environment } from '../../environments/environment';

@Injectable()
export class StatsService {
  private statsUrl = environment.url + 'stats/';

  constructor(private http: Http) { }

  getGolfersStats(idgolfer: number, year: number): Observable<IWeeklyStats[]> {

    return this.http.get(this.statsUrl + 'golfer/' + idgolfer + '/' + year)
      .map((response: Response) => response.json())
      //.do(data => console.log('getStats: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getPieChartData(idgolfer: number, year: number): Observable<IStatsPie> {

    return this.http.get(this.statsUrl + 'find/' + year)
      .map((response: Response) => this.convertToGoogleChart(response.json(), idgolfer))
      // .do(data => console.log('getStats: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  convertToGoogleChart(stats: IWeeklyStats[], idgolfer: number): IStatsPie {
    let chartData: IStatsPie = this.initializeStatsPie();
    let league = [];
    let golfer = [];

    let albatros = 0;
    let eagle = 0;
    let birdie = 0;
    let par = 0;
    let bogey = 0;
    let double = 0;
    let other = 0;

      let g_albatros = 0;
      let g_eagle = 0;
      let g_birdie = 0;
      let g_par = 0;
      let g_bogey = 0;
      let g_double = 0;
      let g_other = 0;

    const size = stats.length;
    for (let x = 0; x < size; x++) {

      albatros += +stats[x].albatros;
      eagle += +stats[x].eagle;
      birdie += +stats[x].birdie;
      par += +stats[x].par;
      bogey += +stats[x].bogey;
      double += +stats[x].doubleBogey;
      other += +stats[x].other;

      if (stats[x].idgolfer === idgolfer) {
          g_albatros = stats[x].albatros;
          g_eagle = stats[x].eagle;
          g_birdie = stats[x].birdie;
          g_par = stats[x].par;
          g_bogey = stats[x].bogey;
          g_double = stats[x].doubleBogey;
          g_other = stats[x].other;
      }
    }

    chartData.LeagueData.push(['Type', 'Total']);
    chartData.LeagueData.push(['Albatros', albatros]);
    chartData.LeagueData.push(['Eagle', eagle]);
    chartData.LeagueData.push(['Birdie', birdie]);
    chartData.LeagueData.push(['Par', par]);
    chartData.LeagueData.push(['Bogey', bogey]);
    chartData.LeagueData.push(['Double', double]);
    chartData.LeagueData.push(['Other', other]);

    chartData.GolferData.push(['Type', 'Total']);
    chartData.GolferData.push(['Albatros', g_albatros]);
    chartData.GolferData.push(['Eagle', g_eagle]);
    chartData.GolferData.push(['Birdie', g_birdie]);
    chartData.GolferData.push(['Par', g_par]);
    chartData.GolferData.push(['Bogey', g_bogey]);
    chartData.GolferData.push(['Double', g_double]);
    chartData.GolferData.push(['Other', g_other]);
    return chartData;
  }
  getLeagueStats( year: number): Observable<IWeeklyStats[]> {

    return this.http.get(this.statsUrl + 'find/' + year)
      .map((response: Response) => response.json())
      // .do(data => console.log('getStats: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }
  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  initializeStatsPie(): IStatsPie {
    // Return an initialized object
    return {
      GolferData: [],
      LeagueData: []
    };
  }

}
