import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { StatsService } from './stats.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { IStats } from './stats';
import { SortService } from '../sortable/sort.service';
import { Criteria } from '../sortable/criteria';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  errorMessage: string;
  stats: IStats[];
  pageTitle: string;
  criteria: Criteria;

  constructor(
    private statsService: StatsService,
    private sortService: SortService) { }

  ngOnInit() {
   this.populateStats();
  }

  populateStats() {
    const year  = +localStorage.getItem('year');
    this.pageTitle = 'League Stats for ' + year;
    this.getStats(year);
  }


  getStats(year: number) {
    this.statsService.getLeagueStats(year).subscribe(
      (stats: IStats[]) => this.onStatsRetrieved(stats),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onStatsRetrieved(stats: IStats[]): void {
    this.stats = stats;
    this.stats = this.getSortedStats({ sortColumn: 'par', sortDirection: 'asc', tableName: '1' });

  }
  onSorted($event, table: number) {
    this.getSortedStats($event);
  }
  getSortedStats(criteria: Criteria): IStats[] {
    return this.stats.sort((a, b) => {
      let rc = 0;
      if (criteria.sortDirection === 'desc') {

        if (criteria.sortColumn === 'albatros') {
          if (a.albatros < b.albatros) {
            rc = -1;
          } else if (a.albatros > b.albatros) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'eagle') {
          if (a.eagle < b.eagle) {
            rc = -1;
          } else if (a.eagle > b.eagle) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'birdie') {
          if (a.birdie < b.birdie) {
            rc = -1;
          } else if (a.birdie > b.birdie) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'par') {
          if (a.par < b.par) {
            rc = -1;
          } else if (a.par > b.par) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'bogey') {
          if (a.bogey < b.bogey) {
            rc = -1;
          } else if (a.bogey > b.bogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'double') {
          if (a.doubleBogey < b.doubleBogey) {
            rc = -1;
          } else if (a.doubleBogey > b.doubleBogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'other') {
          if (a.other < b.other) {
            rc = -1;
          } else if (a.other > b.other) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'golfer') {
          if (a.idgolfer < b.idgolfer) {
            rc = -1;
          } else if (a.idgolfer > b.idgolfer) {
            rc = 1;
          } else {
            rc = 0;
          }
        }

      } else {
        if (criteria.sortColumn === 'albatros') {
          if (a.albatros > b.albatros) {
            rc = -1;
          } else if (a.albatros < b.albatros) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'eagle') {
          if (a.eagle > b.eagle) {
            rc = -1;
          } else if (a.eagle < b.eagle) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'birdie') {
          if (a.birdie > b.birdie) {
            rc = -1;
          } else if (a.birdie < b.birdie) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'par') {
          if (a.par > b.par) {
            rc = -1;
          } else if (a.par < b.par) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'bogey') {
          if (a.bogey > b.bogey) {
            rc = -1;
          } else if (a.bogey < b.bogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'double') {
          if (a.doubleBogey > b.doubleBogey) {
            rc = -1;
          } else if (a.doubleBogey < b.doubleBogey) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'other') {
          if (a.other > b.other) {
            rc = -1;
          } else if (a.other < b.other) {
            rc = 1;
          } else {
            rc = 0;
          }
        } else if (criteria.sortColumn === 'week') {
          if (a.idgolfer > b.idgolfer) {
            rc = -1;
          } else if (a.idgolfer < b.idgolfer) {
            rc = 1;
          } else {
            rc = 0;
          }
        }
      }
      return rc;
    });
  }
}
