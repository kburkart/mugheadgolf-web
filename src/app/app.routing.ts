import { Routes } from '@angular/router';

import { AuthenticatedUserComponent } from './authenticated-user/authenticated-user.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { SignInComponent } from './fw/users/sign-in/sign-in.component';
import { ForgotComponent } from './fw/users/sign-in/forgot.component';
import { RegisterUserComponent } from './fw/users/register-user/register-user.component';
import { AuthGuard } from './services/auth-guard.service';
import { AdminGuard } from './services/admin.guard';
import { GolferComponent } from './golfer/golfer.component';
import { MatchComponent } from './score/match.component';
import { GolferScoresComponent } from './score/golfer-scores.component';
import { GolferNetScoresComponent } from './score/golfer-net-scores.component';
import { HandicapsComponent } from './golfer/handicaps.component';
import { WeeklyScheduleComponent } from './schedule/weekly-schedule.component';
import { CreateScheduleComponent } from './schedule/create-schedule.component';
import { GolferScheduleComponent } from './schedule/golfer-schedule.component';
import { WeeklyScoresComponent } from './score/weekly-scores.component';
import { EditScoreComponent } from './score/edit-score.component';
import { StandingsComponent } from './standings/standings.component';
import { StatsComponent } from './stats/stats.component';
import { FoursomeComponent } from './foursome/foursome.component';
import { FoursomeScoreComponent } from './score/foursome-score.component';
import { WeeklyStatsComponent } from './stats/weekly-stats.component';
import { MoneyComponent } from './money/money.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { TutorialTeetimesComponent } from './tutorial/tutorial-teetimes.component';
import { AddGolferComponent } from './golfer/add-golfer.component';
import { EditGolferComponent } from './golfer/edit-golfer.component';
import { AuthenticateComponent } from './golfer/authenticate.component';
import { RegisterResponseComponent } from './register-response/register-response.component';
import { ActivationNoticeComponent } from './activation-notice/activation-notice.component';
import { DivisionsComponent } from './divisions/divisions.component';
import { DivisionSetupComponent } from './divisions/division-setup.component';
import { SettingsComponent } from './settings/settings.component';
import { AddMoneyComponent } from './money/add-money.component';
import { WeeklyMoneyComponent } from './money/weekly-money.component';
import { GolferMoneyComponent } from './money/golfer-money.component';
import { EmailComponent } from './email/email.component';
import { SendScheduleComponent } from './email/send-schedule.component';
import { FoodComponent } from './food/food.component';
import { OrderComponent } from './order/order.component';
import { CalcHandicapsComponent } from './golfer/calc-handicaps.component';
import { BracketLowComponent } from './tourny/bracket-low.component';
import { BracketHighComponent } from './tourny/bracket-high.component';
import { TourneyLowComponent } from './tourny/tourney-low.component';
import { TourneyHighComponent } from './tourny/tourney-high.component';
import { TourneyNetComponent } from './tourny/tourney-net.component';
import { WagerComponent } from './wager/wager.component';
import { WagerResultsComponent } from './wager/wager-results.component';
import { YearComponent } from './year/year.component';


export const appRoutes: Routes = [
  { path: 'signin', component: SignInComponent },
  { path: 'tutorial', component: TutorialComponent },
  { path: 'tutorial-teetimes', component: TutorialTeetimesComponent },
  { path: 'forgot', component: ForgotComponent },
  { path: 'register/:id', component: AddGolferComponent },
  { path: 'registerResponse', component: RegisterResponseComponent },
  { path: 'activationNotice', component: ActivationNoticeComponent },
  { path: 'authenticate/:token', component: AuthenticateComponent },
  {
    path: 'authenticated', component: AuthenticatedUserComponent, canActivate: [AuthGuard],
    children: [
      {
        path: '', canActivateChild: [AuthGuard],
        children: [
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
          { path: 'dashboard', component: DashboardComponent },
          { path: 'division', component: DivisionsComponent },
          { path: 'golfers', component: GolferComponent },
          { path: 'scores', component: GolferScoresComponent },
          { path: 'match/:idschedule', component: MatchComponent },
          { path: 'foursomeScore/:idfoursome', component: FoursomeScoreComponent },
          { path: 'handicaps', component: HandicapsComponent },
          { path: 'weeklySchedule', component: WeeklyScheduleComponent },
          { path: 'foursomes', component: FoursomeComponent },
          { path: 'golferSchedule', component: GolferScheduleComponent },
          { path: 'weeklyScores', component: WeeklyScoresComponent },
          { path: 'golferScores', component: GolferScoresComponent },
          { path: 'golferNetScores', component: GolferNetScoresComponent },
          { path: 'standings', component: StandingsComponent },
          { path: 'stats', component: StatsComponent },
          { path: 'weeklyStats', component: WeeklyStatsComponent },
          { path: 'weeklyMoney', component: WeeklyMoneyComponent },
          { path: 'golferMoney', component: GolferMoneyComponent },
          { path: 'food', component: FoodComponent },
          { path: 'order', component: OrderComponent },
          { path: 'wagerResults', component: WagerResultsComponent },
          { path: 'wager', component: WagerComponent },
          { path: 'tourneyLow', component: TourneyLowComponent },
          { path: 'tourneyHigh', component: TourneyHighComponent },
          { path: 'tourneyNet', component: TourneyNetComponent },
          { path: 'year', component: YearComponent },
          { path: '', canActivate: [AdminGuard],
            children: [
              { path: 'bracketLow', component: BracketLowComponent },
              { path: 'bracketHigh', component: BracketHighComponent },
              { path: 'editScore', component: EditScoreComponent },
              { path: 'addWinners', component: AddMoneyComponent },
              { path: 'settings', component: SettingsComponent },
              { path: 'addGolfer/:id', component: AddGolferComponent },
              { path: 'editGolfer', component: EditGolferComponent },
              { path: 'createSchedule', component: CreateScheduleComponent },
              { path: 'divisionSetup', component: DivisionSetupComponent },
              { path: 'email', component: EmailComponent },
              { path: 'sendSchedule', component: SendScheduleComponent },
              { path: 'calcHandicaps', component: CalcHandicapsComponent }
            ]
          }
        ]
      }
    ]
  },
  { path: '', component: SignInComponent },
  { path: '**', component: SignInComponent }
];
