import { ICourse } from '../course/course';

export interface ISettings {
    idsettings: number;
    useghin: number;
    numofrounds: number;
    useallfortourney: number;
    maxallowedhc: number;
    maxallowedpar3: number;
    maxallowedpar4: number;
    maxallowedpar5: number;
    percentage: number;
    year: number;
    totalweeks: number;
    idcourse: ICourse;
    message: string;
    showmessage: boolean;
    foodemail: string;
    foodphone: string;
    orderday: number;
    orderstart: number;
    orderend: number;
    courseemail: string;

}
