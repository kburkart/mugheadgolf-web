import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SettingsService } from './settings.service';
import { ISettings } from './settings';
import { IGames } from './games';
import { Observable } from 'rxjs/Observable';
import { GenericValidator } from '../shared/generic-validator';
import { ICourse } from '../course/course';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements AfterViewInit, OnInit {
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  // Use with the generic validation message class
  displayMessage: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;


  pageTitle = 'League Settings';
  errorMessage: string;
  settingsForm: FormGroup;
  settings: ISettings;
  games: IGames[];
  courses: ICourse[];
  myCourse: number;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService) {
    this.validationMessages = {
      foodemail: {
        email: 'Invalid Email.'
      },
      courseemail: {
        email: 'Invalid Email.'
      },
      foodphone: {
        pattern: 'Phone number does not meet the pattern 1234567890, only use digits.'
      },
      totalweeks: {
        required: 'Total weeks to be played is required.',
      },
      numofrounds: {
        required: 'Number of Rounds to use for Handicap is required.'
      },
      percentage: {
        required: 'Percentage is required for Handicap Calculations.'
      },
      maxallowedpar3: {
        required: 'Max value for par 3 is required.'
      },
      maxallowedpar4: {
        required: 'Max value for par 4 is required.'
      },
      maxallowedpar5: {
        required: 'Max value for par 5 is required.'
      },
      maxallowedhc: {
        required: 'Max allowed must be set, set to 0 to set max for each par level.'
      }
    };

    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngOnInit(): void {
    this.myCourse = 1;
    this.settingsForm = this.fb.group({
      totalweeks: ['', Validators.required],
      foodphone: ['', Validators.pattern('\\d{10}')],
      foodemail: ['', Validators.email],
      courseemail: ['', Validators.email],
      orderstart: '',
      orderend: '',
      orderday: '',
      useghin: '',
      numofrounds: '',
      useallfortourney: '',
      maxallowedhc: '',
      maxallowedpar3: '',
      maxallowedpar4: '',
      maxallowedpar5: '',
      percentage: ['', Validators.required],
      skins: '',
      pin1: '',
      pin2: '',
      lownet1: '',
      lownet2: '',
      longdrive: '',
      longputt: '',
      selectedCourse: ''
    });


    this.getCourses();
  }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    const controlBlurs: Observable<any>[] = this.formInputElements
      .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

    // Merge the blur event observable with the valueChanges observable
    Observable.merge(this.settingsForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.settingsForm);
    });
  }

  onSettingsRetrieved(settings: ISettings): void {
    if (this.settingsForm) {
      this.settingsForm.reset();
    }
    this.settings = settings;

    if (this.settings.idsettings === 0) {
      this.pageTitle = 'Add Settings';
    } else {
      this.pageTitle = 'Edit Settings: ' + this.settings.year;
    }

    // Update the data on the form
    let ghin = false;
    let allScores = false;

    if (this.settings.useghin === 1) {
      ghin = true;
    }

    if (this.settings.useallfortourney === 1) {
      allScores = true;
    }
    if (this.settings.idcourse){
      this.myCourse = this.settings.idcourse.idcourse;
    }

    this.settingsForm.patchValue({
      totalweeks: this.settings.totalweeks,
      useghin: ghin,
      numofrounds: this.settings.numofrounds,
      useallfortourney: allScores,
      maxallowedhc: this.settings.maxallowedhc,
      maxallowedpar3: this.settings.maxallowedpar3,
      maxallowedpar4: this.settings.maxallowedpar4,
      maxallowedpar5: this.settings.maxallowedpar5,
      percentage: this.settings.percentage,
      selectedCourse: this.settings.idcourse,
      foodemail: this.settings.foodemail,
      foodphone: this.settings.foodphone,
      orderstart: this.settings.orderstart,
      orderday: this.settings.orderday,
      orderend: this.settings.orderend,
      courseemail: this.settings.courseemail
    });
    this.settingsService.getGames()
      .subscribe(games => this.onGamesRetrieved(games),
      error => this.errorMessage = <any>error);


  }

  onGamesRetrieved(games: IGames[]): void {
    if (games && games.length) {
      this.settingsForm.patchValue({
        skins: this.games[0].amount,
        pin1: this.games[1].amount,
        pin2: this.games[2].amount,
        lownet1: this.games[3].amount,
        lownet2: this.games[4].amount,
        longdrive: this.games[5].amount,
        longputt: this.games[6].amount
      });
    } else {
      console.log('Games is Empty');
    }

  }

  getCourses(): void {
    this.settingsService.getCourses()
    .subscribe(courses => this.onCoursesRetrieved(courses),
    error => this.errorMessage = <any>error);
  }

  onCoursesRetrieved(courses: ICourse[]): void {
    this.courses = courses;
    const year  = +localStorage.getItem('year');
    this.settingsService.getSettings(year)
      .subscribe(settings => this.onSettingsRetrieved(settings),
      error => this.errorMessage = <any>error);

  }


  saveSettings(): void {
    if (this.settingsForm.dirty && this.settingsForm.valid) {
      // Copy the form values over the product object values

      this.settings.courseemail = this.settingsForm.get('courseemail').value;
      this.settings.foodemail = this.settingsForm.get('foodemail').value;
      this.settings.foodphone = this.settingsForm.get('foodphone').value;
      this.settings.orderday = this.settingsForm.get('orderday').value;
      this.settings.orderstart = this.settingsForm.get('orderstart').value;
      this.settings.orderend = this.settingsForm.get('orderend').value;
      if (this.settingsForm.get('useghin').value === true) {
        this.settings.useghin = 1;
      } else {
        this.settings.useghin = 0;
      }
      this.settings.totalweeks = this.settingsForm.get('totalweeks').value;
      this.settings.percentage = this.settingsForm.get('percentage').value;
      this.settings.idcourse = this.settingsForm.get('selectedCourse').value;

      if (!this.settings.useghin) {
      this.settings.maxallowedhc = this.settingsForm.get('maxallowedhc').value;
      this.settings.numofrounds = this.settingsForm.get('numofrounds').value;
      this.settings.maxallowedpar3 = this.settingsForm.get('maxallowedpar3').value;
      this.settings.maxallowedpar4 = this.settingsForm.get('maxallowedpar4').value;
      this.settings.maxallowedpar5 = this.settingsForm.get('maxallowedpar5').value;
      } else {
        this.settings.maxallowedhc = 0;
        this.settings.numofrounds = 0;
        this.settings.maxallowedpar3 = 0;
        this.settings.maxallowedpar4 = 0;
        this.settings.maxallowedpar5 = 0;
      }
      this.settings.showmessage = false;
      this.games[0].amount = this.settingsForm.get('skins').value;
      this.games[1].amount = this.settingsForm.get('pin1').value;
      this.games[2].amount = this.settingsForm.get('pin2').value;
      this.games[3].amount = this.settingsForm.get('lownet1').value;
      this.games[4].amount = this.settingsForm.get('lownet2').value;
      this.games[5].amount = this.settingsForm.get('longdrive').value;
      this.games[6].amount = this.settingsForm.get('longputt').value;

      this.games[0].year = +localStorage.getItem('year');
      this.games[1].year = +localStorage.getItem('year');
      this.games[2].year = +localStorage.getItem('year');
      this.games[3].year = +localStorage.getItem('year');
      this.games[4].year = +localStorage.getItem('year');
      this.games[5].year = +localStorage.getItem('year');
      this.games[6].year = +localStorage.getItem('year');

      if (this.settingsForm.get('useallfortourney').value === true) {
        this.settings.useallfortourney = 1;
      } else {
        this.settings.useallfortourney = 0;
      }

      this.settingsService.saveSettings(this.settings)
        .subscribe(
        () => this.onSaveSettingsComplete(),
        (error: any) => this.errorMessage = <any>error
        );
    } else if (!this.settingsForm.dirty) {
    }
  }



  onSaveSettingsComplete(): void {
    this.settingsService.saveGames(this.games)
      .subscribe(
      () => this.onSaveGamesComplete(),
      (error: any) => this.errorMessage = <any>error
      );

  }
onSaveGamesComplete(): void {
    this.router.navigate(['/authenticated/dashboard']);
  }

}
