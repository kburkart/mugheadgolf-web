import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { ISettings } from './settings';
import { IGames } from './games';
import { ICourse } from '../course/course';

@Injectable()
export class SettingsService {

  private baseUrl = environment.url + 'settings/';
  private gamesUrl = environment.url + 'games/';
  private courseUrl = environment.url + 'course/';
  constructor(private http: Http) { }


  getSettings(year: number): Observable<ISettings> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl + 'find/' + year;

    return this.http.get(url, options)
      .map(response => response.json())
 //   .do(data => console.log('getGolfers: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getCourses(): Observable<ICourse[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.courseUrl + 'all';

    return this.http.get(url, options)
      .map(response => response.json())
//    .do(data => console.log('getGames: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getGames(): Observable<IGames[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.gamesUrl + 'all';

    return this.http.get(url, options)
      .map(response => response.json())
//    .do(data => console.log('getGames: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveSettings(settings: ISettings): Observable<ISettings> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    if (settings.idsettings === 0) {
      settings.idsettings = undefined;
    }
    const url = this.baseUrl + 'save';
    return this.http.post(url, settings, options)
      .map(response => response.json())
//    .do(data => console.log('saveSettings: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveGames(games: IGames[]): Observable<IGames[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.gamesUrl + 'save';
    return this.http.post(url, games, options)
      .map(response => response.json())
//    .do(data => console.log('saveSettings: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }



  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  initializeSettings(): ISettings {
    // Return an initialized object
    const currentYear = (new Date()).getFullYear();

    return {
      idsettings: 0,
      useghin: 1,
      numofrounds: 3,
      useallfortourney: 1,
      maxallowedhc: 0,
      maxallowedpar3: 6,
      maxallowedpar4: 7,
      maxallowedpar5: 8,
      percentage: .96,
      year: currentYear,
      totalweeks: 16,
      idcourse: null,
      message: null,
      showmessage: false,
      foodemail: null,
      foodphone: null,
      orderday: 1,
      orderstart: 17,
      orderend: 20,
      courseemail: null
    };
  }

}

