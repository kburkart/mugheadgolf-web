export interface IGames {
    idgame: number;
    name: string;
    amount: number;
    year: number;
}
