import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { IGolfer } from '../golfer/golfer';
import { IScore } from '../score/score';
import { ISchedule } from './schedule';
import { IWeeklySchedule } from './weekly-schedule';

@Injectable()
export class ScheduleService {

  private scheduleUrl = environment.url + 'schedule/';
  private scoreUrl = environment.url + 'score/schedule/';
  constructor(private http: Http) { }

  // private createSchedule(golfer: IGolfer, options: RequestOptions): Observable<IGolfer> {
  //   golfer.idgolfer = undefined;
  //   return this.http.post(this.registerUrl, golfer, options)
  //     .map(this.extractData)
  //     .do(data => console.log('createGolfer: ' + JSON.stringify(data)))
  //     .catch(this.handleError);
  // }

  getSchedule(year: number): Observable<IWeeklySchedule[]> {
    return this.http.get(this.scheduleUrl + 'year/' + year)
      .map((response: Response) => this.mapWeeklySchedule(response.json()))
      // .do(data => console.log('getScedules: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getScheduleById(id: number): Observable<ISchedule> {
    return this.http.get(this.scheduleUrl + '/' + id)
      .map((response: Response) => response.json())
     // .do(data => console.log('getScedules: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }


  createSchedule(year: number): Observable<any[]> {
    return this.http.get(this.scheduleUrl + 'create/' + year)
    // .map((response: Response) => response.json())
     // .do(data => console.log('getScedules: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getGolferSchedule(year: number, golfer: number): Observable<ISchedule[]> {
    return this.http.get(this.scheduleUrl + 'golfer/' + golfer + '/' + year)
      .map((response: Response) => response.json())
// .do(data => console.log('getScedules: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getMatchScores(id: number): Observable<IScore[]> {
    return this.http.get(this.scoreUrl + id)
      .map((response: Response) => response.json())
      // .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getCurrentWeek(year: number): Observable<number> {
      return this.http.get(this.scheduleUrl + 'currentWeek/' + year)
        .map((response: Response) => response.json())
    //    .do(data => console.log('currentweek: ' + JSON.stringify(data)))
        .catch(this.handleError);
  }
"{"
  saveBacknine(schedule: ISchedule): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.scheduleUrl + 'saveBacknine';
    return this.http.post(url, schedule, options)
    //  .map((response: Response) => response.json())
      // .do(data => console.log('createScorehole: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  save(schedule: ISchedule): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.scheduleUrl + 'save';
    return this.http.post(url, schedule, options)
  //    .map((response: Response) => response.json())
  //    .do(data => console.log('save schedule: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  swapGolfers(week: number, year: number, golfer1: number, golfer2: number): Observable<IWeeklySchedule[]> {
    return this.http.get(this.scheduleUrl + 'swap/' + year + '/' + week + '/' + golfer1 + '/' + golfer2)
    .map((response: Response) => response.json())
   // .do(data => console.log('getScedules: ' + JSON.stringify(data)))
    .catch(this.handleError);
  }

  toggleAbsent(schedule: number, golfer: number): Observable<IWeeklySchedule[]> {
    const year  = +localStorage.getItem('year');
    return this.http.get(this.scheduleUrl + 'toggleAbsent/' + year + '/' + golfer + '/' + schedule)
      .map((response: Response) => response.json())
      // .do(data => console.log('getScedules: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }


  toWeeklySchedule(r: any): IWeeklySchedule {
    const result = <IWeeklySchedule> ({
      week: r.week,
      schedules: this.mapSchedule(r.schedules),
    });
  //  console.log('Parsed item:', result);
    return result;
  }

  mapWeeklySchedule(data: Array<any>): IWeeklySchedule[] {
    return data.map(item => this.toWeeklySchedule(item));
  }

  toSchedule(r: any): ISchedule {
    const result = <ISchedule> ({
      idschedule: r.idschedule,
      year: r.year,
      idweek: r.idweek,
      points1: r.points1,
      points2: r.points2,
      active: r.active,
      idcourse: r.idcourse,
      idgolfer1: r.idgolfer1,
      idgolfer2: r.idgolfer2,
      handicap1: r.handicap1,
      handicap2: r.handicap2,
      absent1: r.absent1,
      absent2: r.absent2
    });
//    console.log('Parsed item:', result);
    return result;
  }


  mapSchedule(data: Array<any>): ISchedule[] {
   // console.log('getScedules: ' + JSON.stringify(data));
    return data.map(item => this.toSchedule(item));
  }



  private extractData(response: Response) {
      let body = response.text;
      return body || {};
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  initializeScedule(): ISchedule {
    // Return an initialized object
    return {
      idschedule: 0,
      year: null,
      idweek: null,
      points1: 0,
      points2: 0,
      active: null,
      idcourse: null,
      backnine: 0,
      idgolfer1: null,
      idgolfer2: null,
      handicap1: null,
      handicap2: null,
      absent1: false,
      absent2: false
    };
  }

}
