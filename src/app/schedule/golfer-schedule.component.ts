import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GolferService } from '../golfer/golfer.service';
import { IGolfer } from '../golfer/golfer';
import { ISchedule } from './schedule';
import { ScheduleService } from './schedule.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';



@Component({
  selector: 'app-golfer-schedule',
  templateUrl: './golfer-schedule.component.html',
  styleUrls: ['./golfer-schedule.component.css']
})
export class GolferScheduleComponent implements OnInit {

  golfers: IGolfer[];
  errorMessage: string;
  schedules: ISchedule[];
  scheduleForm: FormGroup;
  pageTitle: string;
  selectedGolfer: IGolfer;


  constructor(private fb: FormBuilder,
    private golferService: GolferService,
    private scheduleService: ScheduleService) {

    this.schedules = undefined;
    this.pageTitle = 'Golfers Schedule';
  }

  ngOnInit() {
    this.scheduleForm = this.fb.group({
      golferlist: ''
    });
    this.scheduleForm.get('golferlist').valueChanges.subscribe((value: IGolfer) => this.populateSchedule(value));
    this.getListOfGolfers();
  }

  populateSchedule(golfer: IGolfer) {
    this.pageTitle = 'Weekly Schedule for ' + golfer.firstname + ' ' + golfer.lastname;
    this.selectedGolfer = golfer;
    this.getSchedules(golfer);
  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
  }

  getSchedules(golfer: IGolfer) {
    const year  = +localStorage.getItem('year');
    this.scheduleService.getGolferSchedule(year, golfer.idgolfer).subscribe(
      (schedules: ISchedule[]) => this.onSchedulesRetrieved(schedules),
      (error: any) => this.errorMessage = <any>error
    );
  }

  onSchedulesRetrieved(schedules: ISchedule[]): void {
    this.schedules = schedules;
  }

}
