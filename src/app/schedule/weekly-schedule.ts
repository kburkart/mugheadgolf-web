import { ISchedule } from './schedule';

export interface IWeeklySchedule {
    week: number;
    schedules: ISchedule[];
}
