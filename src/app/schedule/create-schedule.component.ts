import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ScheduleService } from './schedule.service';

@Component({
  selector: 'app-create-schedule',
  templateUrl: './create-schedule.component.html',
  styleUrls: ['./create-schedule.component.css']
})
export class CreateScheduleComponent implements OnInit {

  errorMessage: string;

  constructor(private route: ActivatedRoute,
      private router: Router,
      private scheduleService: ScheduleService
  ) { }


   ngOnInit() {
    console.log('we are in create schedule');
      this.createSchedule();
  }


  createSchedule(): void {
    const year  = +localStorage.getItem('year');
    this.scheduleService.createSchedule(year)
      .subscribe(
      () => this.onCreateComplete(),
      (error: any) => this.errorMessage = <any>error
      );
  }

  onCreateComplete(): void {
    this.router.navigate(['/authenticated/weeklySchedule']);
  }

}
