import { IGolfer } from '../golfer/golfer';
import { ICourse } from '../course/course';

export interface ISchedule {
    idschedule: number;
    year: number;
    idweek: number;
    points1: number;
    points2: number;
    active: number;
    idcourse: ICourse;
    backnine: number;
    idgolfer1: IGolfer;
    idgolfer2: IGolfer;
    handicap1: number;
    handicap2: number;
    absent1: boolean;
    absent2: boolean;
}
