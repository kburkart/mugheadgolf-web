import {Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName} from '@angular/forms';

import {ActivatedRoute, Router} from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import {ScheduleService} from './schedule.service';

import {IGolfer} from '../golfer/golfer';

import {NumberValidators} from '../shared/number.validator';
import {GenericValidator} from '../shared/generic-validator';
import {ISchedule} from './schedule';
import {IWeeklySchedule} from './weekly-schedule';
import {Console} from '@angular/core/src/console';
import {ScreenService} from '../fw/services/screen.service';
import * as Hammer from 'hammerjs';

@Component({
  selector: 'app-weekly-schedule',
  templateUrl: './weekly-schedule.component.html',
  styleUrls: ['./weekly-schedule.component.css']
})
export class WeeklyScheduleComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, {read: ElementRef}) formInputElements: ElementRef[];

  toggle = true;
  status = 'Enable';
  event: string;
  pressed: any = {};
  errorMessage: string;
  scheduleForm: FormGroup;
  schedules: ISchedule[];
  weeklySchedules: IWeeklySchedule[];
  emailMessage: String;
  currentWeek: number;
  isAdmin: boolean;
  user: number;
  golfer1: IGolfer;
  golfer2: IGolfer;
  element1: HTMLElement;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private scheduleService: ScheduleService) {
  }

  ngOnInit() {
    this.isAdmin = (localStorage.getItem('isAdmin') === 'true');
    this.user = +localStorage.getItem('idgolfer');
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.populateSchedule();
    this.scheduleForm = this.fb.group({});
  }

  ngAfterViewInit(): void {
  }

  gotoMatch(idschedule: number) {
    this.router.navigate(['/authenticated/match', idschedule]);
  }

  longPress(evt: any, golfer: IGolfer, week: number) {
    const element = document.elementFromPoint(evt.center.x, evt.center.y) as HTMLElement;

    const color = element.style.backgroundColor;
    if (color) {
      element.style.backgroundColor = '';
      this.golfer1 = null;
    } else {
      if (this.golfer1) {
        this.golfer2 = golfer;
        this.swapGolfers2(this.golfer1, this.golfer2, week);
        this.golfer1 = null;
        this.golfer2 = null;
        this.element1.style.backgroundColor = '';
      } else {
        element.style.backgroundColor = 'darkseagreen';
        this.golfer1 = golfer;
        this.element1 = element;
      }
    }
  }

  toggleAbsent(golfer: IGolfer, schedule: ISchedule) {
    //    if (golfer.idgolfer === schedule.idgolfer1.idgolfer) {
    //      if (schedule.absent1 === true) {
    //        schedule.absent1 = false;
    //      } else {
    //        schedule.absent1 = true;
    //      }
    //    } else {
    //      if (schedule.absent2 === true) {
    //        schedule.absent2 = false;
    //      } else {
    //        schedule.absent2 = true;
    //      }
    //    }
    // //   console.log('golfer ' + golfer.firstname + ' ' + golfer.lastname + ' toggle absent');
    this.scheduleService.toggleAbsent(schedule.idschedule, golfer.idgolfer)
      .subscribe(weeklySchedules => this.weeklySchedules = weeklySchedules,
        (error: any) => this.errorMessage = <any>error
      );
  }

  populateSchedule() {
    this.getSchedule();

  }

  getSchedule(): void {
    const year = +localStorage.getItem('year');
    console.log('Current Year is ' + year);
    this.scheduleService.getSchedule(year)
      .subscribe(weeklySchedules => this.weeklySchedules = weeklySchedules,
        (error: any) => this.errorMessage = <any>error
      );
  }

  isGolferAdminOrUser(golfer: IGolfer): boolean {
    if (this.isAdmin || this.isUserGolfer(golfer)) {
      return true;
    } else {
      return false;
    }
  }

  isUserGolfer(golfer: IGolfer): boolean {
    let isUser = false;
    if (golfer.idgolfer === this.user) {
      isUser = true;
    }
    //   console.log('isUserGolfer is' + isUser);
    return isUser;

  }


  swapGolfers($event: any, golfer: IGolfer, week: number) {
    const golfer1 = $event.dragData;
    const answer = confirm('Are you sure you want to swap ' + golfer1.firstname + ' ' +
            golfer1.lastname + ' with ' + golfer.firstname + ' ' + golfer.lastname + '?');

    if (answer) {
      const year = +localStorage.getItem('year');
      this.scheduleService.swapGolfers(year, week, golfer.idgolfer, golfer1.idgolfer)
        .subscribe(weeklySchedules => this.weeklySchedules = weeklySchedules,
          (error: any) => this.errorMessage = <any>error
        );
    }
  }

  swapGolfers2(golfer1: IGolfer, golfer2: IGolfer, week: number) {
    const answer = confirm('Are you sure you want to swap ' + golfer1.firstname + ' ' +
      golfer1.lastname + ' with ' + golfer2.firstname + ' ' + golfer2.lastname + '?');

    if (answer) {
      const year = +localStorage.getItem('year');
      this.scheduleService.swapGolfers(year, week, golfer2.idgolfer, golfer1.idgolfer)
        .subscribe(weeklySchedules => this.weeklySchedules = weeklySchedules,
          (error: any) => this.errorMessage = <any>error
        );
    }
  }

  // toSchedule(r: any): ISchedule {
  //   const result = <ISchedule> ({
  //     idschedule: r.idschedule,
  //     year: r.year,
  //     idweek: r.idweek,
  //     points1: r.points1,
  //     points2: r.points2,
  //     active: r.active,
  //     idcourse: r.idcourse,
  //     idgolfer1: r.idgolfer1,
  //     idgolfer2: r.idgolfer2,
  //   });
  //    console.log('Parsed item:', result);
  //   return result;
  // }


}
