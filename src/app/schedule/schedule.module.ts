import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeeklyScheduleComponent } from './weekly-schedule.component';
import { GolferScheduleComponent } from './golfer-schedule.component';
import { FwModule } from '../fw/fw.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateScheduleComponent } from './create-schedule.component';
import { NgDragDropModule } from 'ng-drag-drop';


@NgModule({
  imports: [
    CommonModule,
    FwModule,
    FormsModule,
    ReactiveFormsModule,
    NgDragDropModule.forRoot()
  ],
  declarations: [WeeklyScheduleComponent, GolferScheduleComponent, CreateScheduleComponent]
})
export class ScheduleModule { }
