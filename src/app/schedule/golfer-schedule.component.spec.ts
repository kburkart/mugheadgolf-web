import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GolferScheduleComponent } from './golfer-schedule.component';

describe('GolferScheduleComponent', () => {
  let component: GolferScheduleComponent;
  let fixture: ComponentFixture<GolferScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GolferScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GolferScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
