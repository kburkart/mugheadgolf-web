import { Component, OnInit } from '@angular/core';

import { FrameworkConfigService } from '../services/framework-config.service';
import { UserApi } from '../users/user-api';


@Component({
  selector: 'fw-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  firstname: string;
  lastname: string;
  autoRefresh: boolean;
  
  constructor(public frameworkConfigService: FrameworkConfigService,
              private userApi: UserApi) { }

  ngOnInit() {
    const refreshOn = localStorage.getItem('autoRefresh');
    if ( refreshOn === 'true') {
      this.autoRefresh = true;
    } else {
      this.autoRefresh = false;
    }
  }

  toggleAutoRefresh() {
    const refreshOn = localStorage.getItem('autoRefresh');
    if ( refreshOn === 'true') {
      this.autoRefresh = false;
      localStorage.setItem('autoRefresh', 'false');
    } else {
      this.autoRefresh = true;
      localStorage.setItem('autoRefresh', 'true');
    }
    this.userApi.toggleAutoRefresh().subscribe((data) => {});
  }
  signOut() {
    this.userApi.signOut();
  }

}
