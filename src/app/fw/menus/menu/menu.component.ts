import { Component, OnInit } from '@angular/core';

import { MenuService } from '../../services/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fw-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(public menuService: MenuService,
    private router: Router) { }

  ngOnInit() {
  }

  orderFood() {
    this.menuService.showingLeftSideMenu = false;
    this.router.navigate(['/authenticated/food']);
  }

}
