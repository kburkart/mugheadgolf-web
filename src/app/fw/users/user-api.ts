import { Observable } from 'rxjs/Observable';
import { IGolfer } from '../../golfer/golfer';


export abstract class UserApi {
    signIn: (username: string, password: string, rememberMe: boolean) => Observable<any>;
    forgot: (phone: string) => Observable<any>;
    signOut: () => Observable<any>;
    toggleAutoRefresh: () => Observable<any>;
    setupLocalStorage: () => void;

    abstract firstname: string;
    abstract lastname: string;
    abstract phone: string;

    // changePassword :
}
