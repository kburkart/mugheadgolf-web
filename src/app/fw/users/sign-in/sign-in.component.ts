import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { UserApi } from '../user-api';
import { ScoreService } from '../../../score/score.service';
import { MoneyService } from '../../../money/money.service';
import { PointsService } from '../../../standings/points.service';
import { ScheduleService } from '../../../schedule/schedule.service';
import { DivisionGolferService } from '../../../divisions/division-golfer.service';
import { DivisionsService } from '../../../divisions/divisions.service';
 import { IDivisionGolfer } from '../../../divisions/division-golfer';
import { IDivision } from '../../../divisions/division';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { IPointTotal } from '../../../standings/pointTotal';
import { IGolferMoney } from '../../../money/golfer-money';
import { IGolfer } from '../../../golfer/golfer';
import { ISchedule } from '../../../schedule/schedule';

@Component({
  selector: 'fw-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {

  formError: string;
  submitting = false;
  golfer: IGolfer;

  constructor(private userApi: UserApi) { }

  onSubmit(signInForm: NgForm) {

    if (signInForm.valid) {

      console.log('submitting...', signInForm);
      this.submitting = true;
      this.formError = null;

      this.userApi.signIn(signInForm.value.username, signInForm.value.password, signInForm.value.rememberMe)
        .subscribe((data) => {
          this.golfer = data;
          // console.log('auto refresh is  ', localStorage.getItem('autoRefresh'));

          this.userApi.setupLocalStorage();
        },
          (err) => {
            this.submitting = false;
            this.formError = err;
          }
        );
    }

  }

}

