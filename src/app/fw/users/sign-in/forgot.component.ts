import { Component, OnInit } from '@angular/core';
import { UserApi } from '../user-api';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

  formError: string;
  submitting = false;

  constructor(private userApi: UserApi,
    private router: Router) {
    
   }
   onSubmit(signInForm: NgForm) {

    if (signInForm.valid) {

      console.log('submitting...', signInForm);
      this.submitting = true;
      this.formError = null;

      this.userApi.forgot(signInForm.value.phone)
        .subscribe((data) => {this.router.navigate(['signin']);},
          (err) => {
            this.submitting = false;
            this.formError = err;
          }
        );
    }

  }

  ngOnInit() {
  }

}
