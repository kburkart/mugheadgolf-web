import { Component, OnInit, NgModule, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { AnonymousSubscription } from 'rxjs/Subscription';
import { TickerApi } from '../ticker/ticker-api';



@Component({
  selector: 'fw-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.css'],
})
export class StatusBarComponent implements OnInit, OnDestroy, AfterContentInit {

  tickerText: string;
  private sub: Subscription;
  private timerSubscription: AnonymousSubscription;
  errorMessage: string;
  autoRefresh: boolean;
  dayOfWeek: number;
  hour: number;

  constructor(private tickerApi: TickerApi) { }

  ngOnInit() {
    this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
    this.dayOfWeek = (new Date()).getDay();
    this.hour = (new Date()).getHours();

  }

  ngAfterContentInit() {

    this.tickerApi.getMessages().subscribe(message => {
      const messages = message.json();
      this.tickerText = '';
      messages.forEach(element => {
        this.tickerText += (element + '   ');
      });
      this.subscribeToData();
    });
  }

  getTickerMessage(): void {
    if ((this.autoRefresh) && (this.dayOfWeek === 1) && (this.hour > 16 && this.hour < 21)) {
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
      this.dayOfWeek = (new Date()).getDay();
      this.hour = (new Date()).getHours();
      this.tickerApi.getMessages().subscribe(message => {
        const messages = message.json();
        this.tickerText = '';
        messages.forEach(element => {
          this.tickerText += (element + '   ');
        });
        this.subscribeToData();
      },
        (error: any) => this.errorMessage = <any>error
      );

    } else {
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
      this.dayOfWeek = (new Date()).getDay();
      this.hour = (new Date()).getHours();
    }
  }
  private subscribeToData(): void {
    const time = (new Date()).toLocaleTimeString();
    this.timerSubscription = Observable.timer(120000).first().subscribe(() => this.getTickerMessage());
  }
  unsubscribe(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
  ngOnDestroy() {
    this.unsubscribe();

  }

}
