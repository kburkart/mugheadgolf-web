import { Observable } from 'rxjs/Observable';


export abstract class TickerApi {
    getMessages: () => Observable<any>;
    abstract message: String;
}
