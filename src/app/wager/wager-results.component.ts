import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


import { IGolfer } from '../golfer/golfer';

import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { Console } from '@angular/core/src/console';
import { WagerService } from './wager.service';
import { ScheduleService } from '../schedule/schedule.service';
import { GolferService } from '../golfer/golfer.service';
import { IPickFour } from './pick-four';
import { ISchedule } from '../schedule/schedule';
import { IWeeklySchedule } from '../schedule/weekly-schedule';
import { ScoreService } from '../score/score.service';


@Component({
  selector: 'app-wager-results',
  templateUrl: './wager-results.component.html',
  styleUrls: ['./wager-results.component.css']
})
export class WagerResultsComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  errorMessage: string;
  pick1: number;
  pick2: number;
  pick3: number;
  pick4: number;
  wagerResultsForm: FormGroup;
  schedules: ISchedule[];
  pickFours: IPickFour[];
  emailMessage: String;
  currentWeek: number;
  user: number;
  currentGolfer: IGolfer;
  selectedWeek: number;
  pageTitle: String;
  public golfers: IGolfer[];
  weeklySchedules: IWeeklySchedule[];
  hasWeekStarted: Boolean;
  year: number;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private wagerService: WagerService,
    private scheduleService: ScheduleService,
    private scoreService: ScoreService,
    private golferService: GolferService) { }

  ngOnInit() {

    this.year  = +localStorage.getItem('year');
    this.user = +localStorage.getItem('idgolfer');
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.selectedWeek = this.currentWeek;
    this.getSchedule();
    this.wagerResultsForm = this.fb.group({
    });

  }

  ngAfterViewInit(): void {
  }

  getSchedule(): void {
    this.scheduleService.getSchedule(this.year)
      .subscribe(weeklySchedules => {
        this.weeklySchedules = weeklySchedules;
        this.populateSelectedWeek();
      },
      (error: any) => this.errorMessage = <any>error
      );
  }

  populateSelectedWeek() {
    this.wagerService.getWeeklyPickFourWager(this.year, this.selectedWeek)
      .subscribe(pickfours => { this.pickFours = pickfours;
      },
      (error: any) => this.errorMessage = <any>error
      );
  }

  switchTabs(weeklySchedule) {
    this.selectedWeek = weeklySchedule.week;
    this.populateSelectedWeek();
    this.pageTitle = 'Pick Four Results For Week ' + this.selectedWeek;
  }

  calculateTotals(): void {
    this.wagerService.calculateTotals(this.year, this.selectedWeek)
    .subscribe(success => {if (success === true) {this.populateSelectedWeek(); } });
  }

  onBetComplete(): void {
    this.router.navigate(['/authenticated/wagerResults']);
  }


  initializePickFour(): IPickFour {
    // Return an initialized object
    return {
      idpickfour: 0,
      idgolfer: null,
      pick1: null,
      pick2: null,
      pick3: null,
      pick4: null,
      score1: 0,
      score2: 0,
      score3: 0,
      score4: 0,
      total: null,
      year: null,
      week: null
    };
  }

}
