import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WagerResultsComponent } from './wager-results.component';

describe('WagerResultsComponent', () => {
  let component: WagerResultsComponent;
  let fixture: ComponentFixture<WagerResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WagerResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WagerResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
