import { Injectable } from '@angular/core';
import { IPickFour } from './pick-four';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WagerService {
  private pickfourUrl = environment.url + 'pickfour/';

  constructor(private http: HttpClient) { }


  getGolferPickFourWager(golfer: number, year: number, week: number): Observable<IPickFour> {
    const url = this.pickfourUrl + 'golfer/' + week + '/' + year + '/' + golfer;
    console.log('Url is ' + url);
    return this.http.get<IPickFour>(url);

  }

  getWeeklyPickFourWager(year: number, week: number): Observable<IPickFour[]> {

    return this.http.get<IPickFour[]>(this.pickfourUrl + 'week/' + week + '/' + year);

  }

  calculateTotals(year: number, week: number): Observable<Boolean> {

    return this.http.get<Boolean>(this.pickfourUrl + 'calculate/' + week + '/' + year);

  }

  savePickFourWager(bet: IPickFour): Observable<IPickFour> {
    console.log('output is ' + JSON.stringify(bet));
    if (bet.idpickfour === 0) {
      bet.idpickfour = undefined;
    }
    return this.http.post<IPickFour>(this.pickfourUrl + 'save/', bet);
  }

}
