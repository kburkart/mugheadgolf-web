import { IGolfer } from '../golfer/golfer';

export interface IPickFour {
    idpickfour: number;
    idgolfer: IGolfer;
    pick1: IGolfer;
    pick2: IGolfer;
    pick3: IGolfer;
    pick4: IGolfer;
    score1: number;
    score2: number;
    score3: number;
    score4: number;
    total: String;
    year: number;
    week: number;
}
