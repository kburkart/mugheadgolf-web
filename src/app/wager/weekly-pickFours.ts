import { IPickFour } from './pick-four';

export interface IWeeklyPickFours {
    index: number;
    scores: IPickFour[];
}
