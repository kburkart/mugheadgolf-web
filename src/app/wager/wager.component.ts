
import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


import { IGolfer } from '../golfer/golfer';

import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { Console } from '@angular/core/src/console';
import { WagerService } from './wager.service';
import { ScheduleService } from '../schedule/schedule.service';
import { GolferService } from '../golfer/golfer.service';
import { IPickFour } from './pick-four';
import { ISchedule } from '../schedule/schedule';
import { IWeeklySchedule } from '../schedule/weekly-schedule';
import { ScoreService } from '../score/score.service';


@Component({
  selector: 'app-wager',
  templateUrl: './wager.component.html',
  styleUrls: ['./wager.component.css']
})
export class WagerComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  errorMessage: string;
  pick1: number;
  pick2: number;
  pick3: number;
  pick4: number;
  wagerForm: FormGroup;
  schedules: ISchedule[];
  pickFour: IPickFour;
  emailMessage: String;
  currentWeek: number;
  user: number;
  currentGolfer: IGolfer;
  selectedWeek: number;
  pageTitle: String;
  public golfers: IGolfer[];
  weeklySchedules: IWeeklySchedule[];
  hasWeekStarted: Boolean;
  year: number;
  currentWagerId: number;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private wagerService: WagerService,
    private scheduleService: ScheduleService,
    private scoreService: ScoreService,
    private golferService: GolferService) { }

  ngOnInit() {

    this.year  = +localStorage.getItem('year');
    this.intializeBet();
    this.user = +localStorage.getItem('idgolfer');
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.selectedWeek = this.currentWeek;
    this.getListOfGolfers();
    this.wagerForm = this.fb.group({
      golferList1: ['', Validators.required],
      golferList2: ['', Validators.required],
      golferList3: ['', Validators.required],
      golferList4: ['', Validators.required],
      golfers: this.fb.array([])
    });

  }

  ngAfterViewInit(): void {
  }

  wagerAllowed(): Boolean {
    if (this.wagerForm.valid && !this.hasWeekStarted && ! this.doublePicks()) {
      return true;
    } else {
      return false;
    }
  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
    this.getCurrentGolfer();
  }

  getCurrentGolfer(): void {
    this.golfers.forEach(golfer => {
      if (golfer.idgolfer === this.user) {
        this.currentGolfer = golfer;
      }
    });
    this.getSchedule();
  }

  getSchedule(): void {
    this.scheduleService.getSchedule(this.year)
      .subscribe(weeklySchedules => {
        this.weeklySchedules = weeklySchedules;
        this.populateSelectedWeek();
      },
      (error: any) => this.errorMessage = <any>error
      );
  }

  populateSelectedWeek() {
    this.wagerService.getGolferPickFourWager(this.user, this.year, this.selectedWeek)
      .subscribe(pickfour => {
        if (pickfour === null) {
           this.intializeBet();
           this.pick1 = null;
           this.pick2 = null;
           this.pick3 = null;
           this.pick4 = null;
           this.currentWagerId = 0;
        } else {
          console.log('Pick Four is here');
          this.pick1 = pickfour.pick1.idgolfer;
          this.pick2 = pickfour.pick2.idgolfer;
          this.pick3 = pickfour.pick3.idgolfer;
          this.pick4 = pickfour.pick4.idgolfer;
          this.currentWagerId = pickfour.idpickfour;
          this.pickFour = pickfour;
        }
      },
      (error: any) => this.errorMessage = <any>error
      );
      this.hasGolfWeekStarted();
  }

  hasGolfWeekStarted() {
    this.scoreService.hasGolfWeekStarted(this.year, this.selectedWeek)
    .subscribe(hasStarted => this.hasWeekStarted = hasStarted);
  }

  switchTabs(weeklySchedule) {
    this.selectedWeek = weeklySchedule.week;
    this.populateSelectedWeek();
    this.pageTitle = 'Pick Four Wagers For Week ' + this.currentWeek;
  }

  doublePicks(): Boolean {
      if ((this.pick1 === this.pick2 ||
        this.pick1 === this.pick3 ||
        this.pick1 === this.pick4 ||
        this.pick2 === this.pick3 ||
        this.pick2 === this.pick4 ||
        this.pick3 === this.pick4 )
         && (this.pick1 !== null &&
         this.pick2 !== null &&
         this.pick3 !== null &&
         this.pick4 !== null )) {
          return true;
        } else {
          return false;
        }

  }

  intializeBet() {
    this.pickFour = this.initializePickFour();
    this.pickFour.idgolfer = this.currentGolfer;
    this.pickFour.year = +localStorage.getItem('year');
  }

  placeWager(): void {
    this.golfers.forEach(golfer => {
      if (golfer.idgolfer === this.pick1) {
        this.pickFour.pick1 = golfer;
      }
      if (golfer.idgolfer === this.pick2) {
        this.pickFour.pick2 = golfer;
      }
      if (golfer.idgolfer === this.pick3) {
        this.pickFour.pick3 = golfer;
      }
      if (golfer.idgolfer === this.pick4) {
        this.pickFour.pick4 = golfer;
      }
     });
     this.pickFour.week = this.selectedWeek;
     this.pickFour.idgolfer = this.currentGolfer;
     this.pickFour.idpickfour = this.currentWagerId;
    this.wagerService.savePickFourWager(this.pickFour)
    .subscribe(
        () => this.onBetComplete(),
        (error: any) => this.errorMessage = <any>error
    );
  }

  onBetComplete(): void {
    this.router.navigate(['/authenticated/wagerResults']);
  }


  initializePickFour(): IPickFour {
    // Return an initialized object
    return {
      idpickfour: 0,
      idgolfer: null,
      pick1: null,
      pick2: null,
      pick3: null,
      pick4: null,
      score1: 0,
      score2: 0,
      score3: 0,
      score4: 0,
      total: null,
      year: null,
      week: null
    };
  }

}
