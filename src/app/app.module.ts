import {BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CourseModule } from './course/course.module';
import { ScheduleModule } from './schedule/schedule.module';
import { MoneyModule } from './money/money.module';
import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { GolferComponent } from './golfer/golfer.component';
import { MatchComponent } from './score/match.component';
import { HandicapsComponent } from './golfer/handicaps.component';
import { WeeklyScheduleComponent } from './schedule/weekly-schedule.component';
import { GolferScheduleComponent } from './schedule/golfer-schedule.component';
import { WeeklyScoresComponent } from './score/weekly-scores.component';
import { EditScoreComponent } from './score/edit-score.component';
import { GolferScoresComponent } from './score/golfer-scores.component';
import { GolferNetScoresComponent } from './score/golfer-net-scores.component';
import { StandingsComponent } from './standings/standings.component';
import { MoneyComponent } from './money/money.component';
import { FwModule } from './fw/fw.module';
import { appRoutes } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthenticatedUserComponent } from './authenticated-user/authenticated-user.component';
import { UserService } from './services/user.service';
import { UserApi } from './fw/users/user-api';
import { TickerService } from './services/ticker.service';
import { TickerApi } from './fw/ticker/ticker-api';
import { AuthGuard } from './services/auth-guard.service';
import { AppDataService } from './services/app-data.service';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterResponseComponent } from './register-response/register-response.component';
import { ActivationNoticeComponent } from './activation-notice/activation-notice.component';
import { DivisionsComponent } from './divisions/divisions.component';
import { DivisionSetupComponent } from './divisions/division-setup.component';
import { DivisionsService } from './divisions/divisions.service';
import { DivisionGolferService } from './divisions/division-golfer.service';
import { ScoreService } from './score/score.service';
import { ScheduleService } from './schedule/schedule.service';
import { ScreenService } from './fw/services/screen.service';
import { CourseService } from './course/course.service';
import { SettingsService } from './settings/settings.service';
import { MoneyService } from './money/money.service';
import { GoogleComboChartService } from './services/google-combo-chart.service';
import { GooglePieChartService } from './services/google-pie-chart.service';
import { ComboChartComponent } from './dashboard/Charts/combochart.component';
import { PieChartComponent } from './dashboard/Charts/piechart.component';
import { EmailComponent } from './email/email.component';
import { EmailService } from './email/email.service';
import { environment } from '../environments/environment';
import { AdminGuard } from './services/admin.guard';
import { TutorialComponent } from './tutorial/tutorial.component';
import { TutorialTeetimesComponent } from './tutorial/tutorial-teetimes.component';
import { FoodComponent } from './food/food.component';
import { OrderComponent } from './order/order.component';
import { Foodorder } from './order/foodorder';
import { FoodService } from './food/food.service';
import { OrderService } from './order/order.service';
import { SendScheduleComponent } from './email/send-schedule.component';
import { SortableColumnComponent } from './sortable/sortable-column.component';
import { SortableTableDirective } from './sortable/sortable-table.directive';
import { SortService } from './sortable/sort.service';
import { PointsService } from './standings/points.service';
import { EditGolferComponent } from './golfer/edit-golfer.component';
import { AuthenticateComponent } from './golfer/authenticate.component';
import { AddGolferComponent } from './golfer/add-golfer.component';
import { ListGolfersComponent } from './golfer/list-golfers.component';
import { GolferService } from './golfer/golfer.service';
import { NgDragDropModule } from 'ng-drag-drop';
import { CalcHandicapsComponent } from './golfer/calc-handicaps.component';
import { StatsComponent } from './stats/stats.component';
import { WeeklyStatsComponent } from './stats/weekly-stats.component';
import { StatsService } from './stats/stats.service';
import { BracketLowComponent } from './tourny/bracket-low.component';
import { BracketHighComponent } from './tourny/bracket-high.component';
import { TourneyService } from './tourny/tourney.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SlidePanelComponent } from './tourny/slide-panel/slide-panel.component';
import { TourneyLowComponent } from './tourny/tourney-low.component';
import { TourneyHighComponent } from './tourny/tourney-high.component';
import { MatDialogModule } from '@angular/material';
import { TourneyNetComponent } from './tourny/tourney-net.component';
import { WagerComponent } from './wager/wager.component';
import { WagerResultsComponent } from './wager/wager-results.component';
import { YearComponent } from './year/year.component';
import { HttpClientModule } from '@angular/common/http';
import { WagerService } from './wager/wager.service';
import { FoursomeComponent } from './foursome/foursome.component';
import { FoursomeScoreComponent } from './score/foursome-score.component';
import { FoursomeService } from './foursome/foursome.service';


import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
    'press': { time: 1000 }  // set press delay for 1 second
  };
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SettingsComponent,
    AuthenticatedUserComponent,
    RegisterResponseComponent,
    ActivationNoticeComponent,
    DivisionsComponent,
    DivisionSetupComponent,
    ComboChartComponent,
    PieChartComponent,
    EmailComponent,
    TutorialComponent,
    TutorialTeetimesComponent,
    FoodComponent,
    OrderComponent,
    StandingsComponent,
    SendScheduleComponent,
    SortableColumnComponent,
    SortableTableDirective,
    MatchComponent,
    WeeklyScoresComponent,
    GolferScoresComponent,
    GolferNetScoresComponent,
    EditScoreComponent,
    EditGolferComponent,
    AuthenticateComponent,
    AddGolferComponent,
    ListGolfersComponent,
    HandicapsComponent,
    GolferComponent,
    CalcHandicapsComponent,
    StatsComponent,
    WeeklyStatsComponent,
    BracketLowComponent,
    BracketHighComponent,
    SlidePanelComponent,
    TourneyLowComponent,
    TourneyHighComponent,
    TourneyNetComponent,
    WagerComponent,
    WagerResultsComponent,
    YearComponent,
    FoursomeComponent,
    FoursomeScoreComponent
  ],
  imports: [
    FormsModule,
    HttpModule,
    MatDialogModule,
    FwModule,
    BrowserModule,
    CourseModule,
    ScheduleModule,
    MoneyModule,
    AdminModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    NgDragDropModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig },
    UserService,
    { provide: UserApi, useExisting: UserService },
    TickerService,
    { provide: TickerApi, useExisting: TickerService },
    AuthGuard,
    AdminGuard,
    AppDataService,
    DivisionsService,
    DivisionGolferService,
    ScoreService,
    ScheduleService,
    ScreenService,
    CourseService,
    SortService,
    SettingsService,
    MoneyService,
    TourneyService,
    GoogleComboChartService,
    GooglePieChartService,
    EmailService,
    FoodService,
    OrderService,
    PointsService,
    Foodorder,
    GolferService,
    StatsService,
    WagerService,
    FoursomeService
  ],
  exports: [
    SortableColumnComponent,
    SortableTableDirective
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    // console.log('Base url :' + environment.url +
    //         ' production? ' +  environment.production +
    //         ' env: ' + environment.env);
  }
 }
