export class Criteria {
    sortColumn: string;
    sortDirection: string;
    tableName: string;
}
