import { Component, OnInit } from '@angular/core';
import { OrderService } from './order.service';
import { Foodorder } from './foodorder';
import { IOrderitem } from './orderitem';
import { IMenuextra } from '../food/menuextra';
import { IMenu } from '../food/menu';
import { IOrderitemextra } from './orderitemextra';
import { IOrderiteminstruction } from './orderiteminstruction';
import { IMenuinstruction } from '../food/menuinstruction';
import { Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { SettingsService } from '../settings/settings.service';
import { ISettings } from '../settings/settings';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
  providers: [CurrencyPipe]
})
export class OrderComponent implements OnInit {

  pageTitle = 'Food Order Details';
  errorMessage: string;
  totalCost: number;
  settings: ISettings;
  foodOrder: Foodorder;

  constructor(private orderService: OrderService,
    private settingsService: SettingsService,
    private router: Router,
    public order: Foodorder,
    private cp: CurrencyPipe) {}

  ngOnInit() {
      const year  = +localStorage.getItem('year');
      this.settingsService.getSettings(year)
        .subscribe(settings => this.settings = settings,
        error => this.errorMessage = <any>error);
      this.updateTotal();

  }

  matchMenuItem(orderItemextra: IOrderitemextra, menuItemextra: IMenuextra) {
    return +orderItemextra.idmenuextra !== +menuItemextra.idmenuextra;
  }

  changeExtras(evt, orderItem: IOrderitem, extra: IMenuextra) {
    const isChecked = evt.target.checked;
    if (isChecked) {
      const orderitemextra = this.initializeExtra();
      orderitemextra.idorderitem = orderItem.idorderitem;
      orderitemextra.idmenuextra = extra;
      orderItem.orderitemextrasCollection.push(orderitemextra);
    } else {
      orderItem.orderitemextrasCollection = orderItem.orderitemextrasCollection.filter(el => this.matchMenuItem(el, extra));
    }
    this.updateTotal();
  }

  changeInstruction(orderItem: IOrderitem, instruct: IMenuinstruction) {
    const orderiteminstruction = this.initializeInstruction();
    orderiteminstruction.idorderitem = orderItem.idorderitem;
    orderiteminstruction.idmenuinstruction = instruct;
    orderItem.orderiteminstructionCollection = [];
    orderItem.orderiteminstructionCollection.push(orderiteminstruction);
    this.updateTotal();
  }


  initializeInstruction(): IOrderiteminstruction {
    return {
      idorderiteminstructions: 0,
      idorderitem: null,
      idmenuinstruction: null
    };
  }
  initializeExtra(): IOrderitemextra {
    return {
      idorderitemextra: 0,
      idorderitem: null,
      idmenuextra: null
    };
  }

  updateTotal() {
    this.totalCost = 0;
    if (this.order.orderitemCollection) {
      this.order.orderitemCollection.forEach(item => this.totalCost += this.calcItemTotal(item));
    }
    this.order.totalcost = this.totalCost;

  }

  calcItemTotal(orderItem: IOrderitem): number {
    let itemCost = +orderItem.idmenu.cost;
    const orderItemextras = orderItem.orderitemextrasCollection;
    if (orderItemextras) {
      orderItemextras.forEach(extra => itemCost += +extra.idmenuextra.cost);
    }
    if (orderItem.orderiteminstructionCollection[0]) {
      itemCost += +orderItem.orderiteminstructionCollection[0].idmenuinstruction.cost;
    }
    return itemCost;
  }

  returnToMenu() {
    this.router.navigate(['/authenticated/food']);
  }

  completeOrder() {
    const d = new Date();
    const day = d.getDay();
    const hour = d.getHours();
    if (day === this.settings.orderday && (hour >= this.settings.orderstart && hour <= this.settings.orderend)) {
      if (confirm('Are you sure you want to place the order?')) {
      this.turnOrderToString();
        this.orderService.saveOrder(this.order, true).subscribe(
          order => this.onOrderComplete(order),
          (error: any) => this.errorMessage = <any>error
        );
      } else {
        // Do nothing
      }
    } else {
      alert('Food Ordering only allowed Monday from 5pm - 9pm ');
    }
  }

  turnOrderToString() {

    let orderString = 'Order for ' + this.order.idgolfer.firstname + ' ' + this.order.idgolfer.lastname + '\n';
    orderString += 'Order Number ' + this.order.idorder + '\n\n';
    this.order.orderitemCollection.forEach(element => {
      orderString += element.idmenu.menuitem + '\n';
      if (element.orderiteminstructionCollection && (element.orderiteminstructionCollection.length > 0))  {
        orderString += '\tInstructions:\n';
        element.orderiteminstructionCollection.forEach(instruct => {
          orderString += '\t\t' + instruct.idmenuinstruction.name + '\n';
        });
      }

      if (element.orderitemextrasCollection && (element.orderitemextrasCollection.length > 0)) {
        orderString += '\tExtras:\n';
        element.orderitemextrasCollection.forEach(extra => {
        orderString += '\t\t' + extra.idmenuextra.name + '\n';
        });
      }
    });
    orderString += 'Total Cost: \t\t' + this.cp.transform(this.order.totalcost, 'USD');
    this.order.description = orderString;
    // console.log(orderString);

  }

  onOrderComplete(order: Foodorder): void {
    alert('Your order has been placed - order id is ' + order.idorder);
    this.router.navigate(['/authenticated/dashboard']);
    //    console.log('Scores have been saved.');
  }
}
