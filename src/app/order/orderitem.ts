import { IMenu } from '../food/menu';
import { IOrderitemextra } from './orderitemextra';
import { IOrderiteminstruction } from './orderiteminstruction';

export interface IOrderitem {
    idorderitem: number;
    idorder: number;
    idmenu: IMenu;
    orderitemextrasCollection: IOrderitemextra[];
    orderiteminstructionCollection: IOrderiteminstruction[];
}
