import { IOrderitem } from './orderitem';
import { IMenuextra } from '../food/menuextra';

export interface IOrderitemextra {
    idorderitemextra: number;
    idorderitem: number;
    idmenuextra: IMenuextra;
}