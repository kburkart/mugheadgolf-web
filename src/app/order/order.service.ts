import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';
import {Foodorder } from './foodorder';

@Injectable()
export class OrderService {

  private baseUrl = environment.url + 'order/';

  constructor(private http: Http) { }

  saveOrder(order: Foodorder, placeorder: boolean): Observable<Foodorder> {
    console.log('save order');
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });


    const url = this.baseUrl + 'save/' + placeorder;

    return this.http.post(url, order, options)
      .map(response => response.json())
      // .do(data => console.log('get Menu: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }
  private extractData(response: Response) {
    console.log(JSON.stringify(response));
    return response.json();
  }
  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
