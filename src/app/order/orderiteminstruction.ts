import { IOrderitem } from './orderitem';
import { IMenuinstruction } from '../food/menuinstruction';

export interface IOrderiteminstruction {

    idorderiteminstructions: number;
    idorderitem: number;
    idmenuinstruction: IMenuinstruction;

}
