import { IMenu } from '../food/menu';
import { IOrderitem } from './orderitem';
import { IOrderitemextra } from './orderitemextra';
import { IOrderiteminstruction } from './orderiteminstruction';
import { IGolfer } from '../golfer/golfer';
import { Injectable } from '@angular/core';

@Injectable()
export class Foodorder {
    idorder: number;
    idgolfer: IGolfer;
    ordertime: Date;
    totalcost: number;
    confirmed: boolean;
    orderitemCollection: IOrderitem[];
    description: string;

}

