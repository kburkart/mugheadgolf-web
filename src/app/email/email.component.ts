import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'; import { EmailService } from './email.service';
import 'rxjs/add/operator/debounceTime';
import { Observable } from 'rxjs/Observable';

import { GenericValidator } from '../shared/generic-validator';

import { Subscription } from 'rxjs/Subscription';
import { IEmailMessage } from './email-message';
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit, AfterViewInit {
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  errorMessage: string;
  emailForm: FormGroup;
  email: IEmailMessage;
  emailSubject: String;
  emailMessage: String;
  displayMessage: { [key: string]: string } = {};
  pageTitle: String = 'Mughead Email';

  private validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;


  constructor(private emailService: EmailService, private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) {
      this.validationMessages = {
        subject: {
            required: 'Subject is a required field.'
        },
        confirmEmail: {
            required: 'Message is a required field.'
        }
      };
      this.genericValidator = new GenericValidator(this.validationMessages);
    }

  ngOnInit() {
    this.emailForm = this.fb.group({
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
    // Watch for the blur event from any input element on the form.
    const controlBlurs: Observable<any>[] = this.formInputElements
        .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

    // Merge the blur event observable with the valueChanges observable
    Observable.merge(this.emailForm.valueChanges, ...controlBlurs).debounceTime(1000).subscribe(value => {
        this.displayMessage = this.genericValidator.processMessages(this.emailForm);
    });
}
  sendEmail() {
    // console.log('The subject is' + this.emailSubject);
    // console.log('The message is ' + this.emailMessage);
    this.email = this.emailService.initializeEmail();
    this.email.message = this.emailMessage;
    this.email.subject = this.emailSubject;
    this.emailService.sendGroupEmail(this.email).subscribe(
       () => this.onSaveComplete(),
      (error: any) => this.errorMessage = <any>error
  );
  }
  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.emailForm.reset();

        this.router.navigate(['/authenticated/dashboard']);

}

}
