import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmailService } from './email.service';

@Component({
  selector: 'app-send-schedule',
  templateUrl: './send-schedule.component.html',
  styleUrls: ['./send-schedule.component.css']
})
export class SendScheduleComponent implements OnInit {
  errorMessage: string;

  constructor(private router: Router,
              private emailService: EmailService) { }

  ngOnInit() {
    this.sendWeeklySchedule();
  }
  sendWeeklySchedule(): void {
    this.emailService.sendWeeklySchedule().subscribe(
      () => this.onSendComplete(),
      (error: any) => this.errorMessage = <any>error
      );
  }

  onSendComplete(): void {
    this.router.navigate(['/authenticated/dashboard']);
  }

}
