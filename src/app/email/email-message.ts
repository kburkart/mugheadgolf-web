export interface IEmailMessage {
    subject: String;
    message: String;
}
