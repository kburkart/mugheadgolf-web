import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';
import { IEmailMessage } from './email-message';

@Injectable()
export class EmailService {
  private baseUrl = environment.url + 'email';

  constructor(private http: Http) { }

  sendGroupEmail(email: IEmailMessage): Observable<String> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const url = this.baseUrl;

   return this.http.post(this.baseUrl, email, options)
        .map(response => response.text())
      //  .do(data => console.log('getGolfer: ' + JSON.stringify(data)))
        .catch(this.handleError);
}

sendWeeklySchedule(): Observable<any> {
  const headers = new Headers({ 'Content-Type': 'application/json' });
  const options = new RequestOptions({ headers: headers });
  const url = this.baseUrl + '/schedule';

 return this.http.get(url, options)
      .map(response => response.text())
    //  .do(data => console.log('getGolfer: ' + JSON.stringify(data)))
      .catch(this.handleError);
}

private handleError(error: Response): Observable<any> {
  // in a real world app, we may send the server to some remote logging infrastructure
  // instead of just logging it to the console
  console.error(error);
  return Observable.throw(error.json().error || 'Server error');
}

initializeEmail(): IEmailMessage {
  // Return an initialized object
  return {
    subject: null,
    message: null
  };
}
}
