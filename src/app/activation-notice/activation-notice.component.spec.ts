import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivationNoticeComponent } from './activation-notice.component';

describe('ActivationNoticeComponent', () => {
  let component: ActivationNoticeComponent;
  let fixture: ComponentFixture<ActivationNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivationNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivationNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
