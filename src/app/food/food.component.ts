import { Component, OnInit } from '@angular/core';
import { IMenu } from './menu';
import { FoodService } from './food.service';
import { Foodorder } from '../order/foodorder';
import { IOrderitem } from '../order/orderitem';
import { GolferService } from '../golfer/golfer.service';
import { IGolfer } from '../golfer/golfer';
import { filter } from 'rxjs/operator/filter';
import { Router } from '@angular/router';
import { OrderService } from '../order/order.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {
  pageTitle = 'Dickmann\'s Menu';
  errorMessage: string;

  menu: IMenu[];
  golferId: number;
  itemNumber: number;


  constructor(private foodService: FoodService,
              private golferService: GolferService,
              private orderService: OrderService,
              private router: Router,
              private order: Foodorder) { }
  
    ngOnInit() {
    this.itemNumber = 1;
    this.golferId = +localStorage.getItem('idgolfer');
    this.foodService.getMenu()
    .subscribe(menu => this.setMenu(menu),
    error => this.errorMessage = <any>error);

  }

  setMenu(menu: IMenu[]) {
    this.menu = menu;
    this.golferService.getGolfer(this.golferId).subscribe(golfer => this.setGolfer(golfer),
    error => this.errorMessage = <any>error);
  }

  setGolfer(golfer: IGolfer) {
    this.order.idgolfer = golfer;
    this.order.confirmed = false;
    this.order.orderitemCollection = [];
    this.order.ordertime = null;
    this.order.totalcost = 0;
    this.order.idorder = undefined;
    this.orderService.saveOrder(this.order, false).subscribe(order => this.setOrder(order),
    error => this.errorMessage = <any>error);
  }

  setOrder(order: Foodorder) {
    this.order.idorder = order.idorder;
  }

  updateOrder(evt, menuItem: IMenu) {
    const isChecked = evt.target.checked;
    if (isChecked) {
      const orderItem = this.initializeOrderItem();
      orderItem.idorder = this.order.idorder;
      orderItem.idorderitem = this.itemNumber;
      this.itemNumber++;
      orderItem.idmenu = menuItem;
      this.order.orderitemCollection.push(orderItem);
    } else {
      this.order.orderitemCollection = this.order.orderitemCollection.filter(el => this.matchMenuItem(el, menuItem));
    }

  }

  matchMenuItem(orderItem: IOrderitem, menuItem: IMenu) {
    console.log('order ' + orderItem.idmenu.idmenu + ' menu ' + menuItem.idmenu);
    return +orderItem.idmenu.idmenu !== +menuItem.idmenu;
  }

  initializeOrderItem(): IOrderitem {
    return{
      idorderitem: null,
      idorder: null,
      idmenu: null,
      orderitemextrasCollection: [],
      orderiteminstructionCollection: []
    };
  }

  continueOrder() {
//    console.log('the order is' + JSON.stringify(this.order));
    this.router.navigate(['/authenticated/order']);
  }

}
