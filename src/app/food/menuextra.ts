export interface IMenuextra {
    idmenuextra: number;
    name: string;
    cost: number;
}
