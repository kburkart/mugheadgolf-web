export interface IMenuinstruction {
    idmenuinstruction: number;
    name: string;
    cost: number;
}
