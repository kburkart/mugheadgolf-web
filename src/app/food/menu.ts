
import { IMenuextra } from './menuextra';
import { IMenuinstruction } from './menuinstruction';


export interface IMenu {
    idmenu: number;
    menuitem: string;
    cost: number;
    menuextrasCollection: IMenuextra[];
    menuinstructionsCollection: IMenuinstruction[];
}
