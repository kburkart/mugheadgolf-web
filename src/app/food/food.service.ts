import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';
import { IMenu } from './menu';

@Injectable()
export class FoodService {

  private baseUrl = environment.url + 'menu/';

  constructor(private http: Http) { }

  getMenu(): Observable<IMenu[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });


    const url = this.baseUrl + 'all';

    return this.http.get(url, options)
      .map(response => response.json())
    //  .do(data => console.log('get Menu: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }


}
