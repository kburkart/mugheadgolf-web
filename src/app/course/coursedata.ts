import { ICourse } from './course';
import { ICourseholes } from './coursehole';


export interface ICoursedata {
    course: ICourse;
    courseholes: ICourseholes[];
}
