import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { ICoursedata } from './coursedata';


@Injectable()
export class CourseService {

  private courseUrl = environment.url + 'course/';
  constructor(private http: Http) { }

  getCourseData(id: number): Observable<ICoursedata> {
    return this.http.get(this.courseUrl + 'data/' + id)
      .map((response: Response) => response.json())
    //  .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }



  private extractData(response: Response) {
      let body = response.text;
      return body || {};
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  // initializeScedule(): ISchedule {
  //   // Return an initialized object
  //   return {
  //     idschedule: 0,
  //     year: null,
  //     idweek: null,
  //     points1: null,
  //     points2: null,
  //     active: null,
  //     idcourse: null,
  //     idgolfer1: null,
  //     idgolfer2: null
  //   };
  // }

}
