import { ICourse } from "./course";

export interface ICourseholes {
    idcoursehole: ICourse;
    hole: number;
    handicap: number;
    length: number;
    par: number;
}
