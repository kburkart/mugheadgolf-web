export interface ICourse {
    idcourse: number;
    name: string;
    tee: string;
    slope: number;
    rating: number;
}
