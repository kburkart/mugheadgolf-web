export interface ISeed {
    id: number;
    idgolfer: number;
    golfer: string;
    handicap: number;
    totalpoints: number;
}
