import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourneyNetComponent } from './tourney-net.component';

describe('TourneyNetComponent', () => {
  let component: TourneyNetComponent;
  let fixture: ComponentFixture<TourneyNetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourneyNetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourneyNetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
