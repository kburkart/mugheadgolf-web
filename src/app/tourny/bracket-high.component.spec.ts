import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BracketHighComponent } from './bracket-high.component';

describe('BracketHighComponent', () => {
  let component: BracketHighComponent;
  let fixture: ComponentFixture<BracketHighComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BracketHighComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BracketHighComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
