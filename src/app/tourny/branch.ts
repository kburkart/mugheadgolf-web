import { IGolfer } from '../golfer/golfer';

export interface IBranch {
    idtourney: number;
    idgolfer: IGolfer;
    year: number;
    highHandicap: boolean;
    branch: number;
    handicap: number;
    points: number;
    nextBranch: number;
}
