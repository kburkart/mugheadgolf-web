import { ISeed } from './seed';

export class Entry {
    constructor(public seed: ISeed, public state = 'left') { }

    isLeftVisible = true;

}
