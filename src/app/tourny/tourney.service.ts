import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { ISeed } from './seed';
import { IBranch } from './branch';
import { environment } from '../../environments/environment';
import { ITourneyNet } from './tourney-net';

@Injectable()
export class TourneyService {
  private lowSeedsUrl = environment.url + 'seedLow/all';
  private highSeedsUrl = environment.url + 'seedHigh/all';
  private handicapUrl = environment.url + 'schedule/weekHC/';
  private pointsUrl = environment.url + 'points/golferWeek/';

  private tourneyNetUrl = environment.url + 'tourneynet/all';
  private tourneyLowUrl = environment.url + 'tourney/bracket/false';
  private tourneyHighUrl = environment.url + 'tourney/bracket/true';
  private branchSaveUrl = environment.url + 'tourney/saveBranch';

  constructor(private http: Http) { }


  getTournetNetData(): Observable<ITourneyNet[]> {

    return this.http.get(this.tourneyNetUrl)
      .map((response: Response) => response.json())
      // .do(data => console.log('getStats: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getLowBracketSeeds(): Observable<ISeed[]> {

    return this.http.get(this.lowSeedsUrl)
      .map((response: Response) => response.json())
      // .do(data => console.log('getStats: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }
  getHighBracketSeeds(): Observable<ISeed[]> {

    return this.http.get(this.highSeedsUrl)
      .map((response: Response) => response.json())
      // .do(data => console.log('getStats: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }
  getLowBracket(): Observable<IBranch[]> {

    return this.http.get(this.tourneyLowUrl)
      .map((response: Response) => response.json())
      .do(data => console.log('getBracketLow: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getHighBracket(): Observable<IBranch[]> {

    return this.http.get(this.tourneyHighUrl)
      .map((response: Response) => response.json())
      .do(data => console.log('getBracketHigh: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getUserHandicap(golfer: number, week: number, year: number): Observable<number> {

    return this.http.get(this.handicapUrl + week + '/' + year + '/' + golfer)
      .map((response: Response) => response.json())
      .do(data => console.log('get handicap: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getUserpoints(golfer: number, week: number, year: number): Observable<number> {

    return this.http.get(this.pointsUrl + golfer + '/' + week + '/' + year)
      .map((response: Response) => response.json())
      .do(data => console.log('get points: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveBranch(branch: IBranch): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.branchSaveUrl;
    return this.http.post(url, branch, options)
      // .map((response: Response) => response.json())
      // .do(data => console.log('createScorehole: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
}
}
