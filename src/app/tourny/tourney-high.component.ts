import { Component, OnInit } from '@angular/core';

import { TourneyService } from './tourney.service';
import { IBranch } from './branch';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'app-tourney-high',
  templateUrl: './tourney-high.component.html',
  styleUrls: ['./bracket.component.css']
})
export class TourneyHighComponent implements OnInit {

  branches: IBranch[];
  errorMessage: String;
  isDataAvailable = false;
  currentWeek = 0;
  isAdmin = false;
  constructor(private tourneyService: TourneyService) { }

  ngOnInit() {
    this.currentWeek = +localStorage.getItem('currentWeek') - 1;
    this.isAdmin = (localStorage.getItem('isAdmin') === 'true');
    this.getBracketData();
  }

  getBracketData() {
    const requests: any[] = [];
    requests[0] = this.tourneyService.getHighBracket();
    forkJoin(requests).subscribe(
      results => this.processResults(results),
      (error: any) => this.errorMessage = <any>error
    );
  }

  processResults(results: any[]) {
    this.branches = results[0];
    this.branches.sort(function (branch1, branch2) {
      if (branch1.branch < branch2.branch) {
        return -1;
      } else if (branch1.branch > branch2.branch) {
        return 1;
      } else {
        return 0;
      }
    });
    this.isDataAvailable = true;
  }

  calculateWinner(branch: IBranch) {

    if (this.isAdmin) {
      const answer = confirm('Do you want to calculate match winner?');
      if (answer === true) {
        const index = branch.branch - 1;
        let branch1 = null;
        let branch2 = null;
        if (this.isEven(index)) {
          branch1 = branch;
          branch2 = this.branches[index + 1];
        } else {
          branch1 = this.branches[index - 1];
          branch2 = branch;
        }
        const requests: any[] = [];

        requests[0] = this.tourneyService.getUserpoints(branch1.idgolfer.idgolfer, this.currentWeek, 2018);
        requests[1] = this.tourneyService.getUserpoints(branch2.idgolfer.idgolfer, this.currentWeek, 2018);
        forkJoin(requests).subscribe(
          results => this.processPoints(branch1, branch2, results),
          (error: any) => this.errorMessage = <any>error
        );
      }
    }
  }

  isEven(num: number): boolean {
    return num % 2 === 0;
  }

  processPoints(branch1: IBranch, branch2: IBranch, results: any[]) {
    const next = branch1.nextBranch - 1;
    branch1.points = results[0];
    branch2.points = results[1];
    if (branch1.points === branch2.points) {
      const winner = confirm('Tiebreaker, Hit Ok if ' + branch1.idgolfer.firstname + ' ' + branch1.idgolfer.lastname + ' was the winner');
      if (winner) {
        this.branches[next].idgolfer = branch1.idgolfer;
        branch1.points = 5.1;
      } else {
        this.branches[next].idgolfer = branch2.idgolfer;
        branch2.points = 5.1;

      }
    } else if (branch1.points > branch2.points) {
      this.branches[next].idgolfer = branch1.idgolfer;
    } else {
      this.branches[next].idgolfer = branch2.idgolfer;
    }
    this.tourneyService.saveBranch(branch1).subscribe(
      () => console.log('branch saved'),
      (error: any) => this.errorMessage = <any>error
    );
    this.tourneyService.saveBranch(branch2).subscribe(
      () => console.log('branch saved'),
      (error: any) => this.errorMessage = <any>error
    );
    this.getHC(this.branches[next]);
  }
  getHC(branch: IBranch) {
    this.tourneyService.getUserHandicap(branch.idgolfer.idgolfer, 16, 2018).subscribe(hc => this.processHC(branch, hc),
      (error: any) => this.errorMessage = <any>error
    );
  }
  processHC(branch: IBranch, hc: number) {
    branch.handicap = hc;
    this.tourneyService.saveBranch(branch).subscribe(
      () => console.log('branch saved'),
      (error: any) => this.errorMessage = <any>error
    );

  }
  isWinner(branch: IBranch): boolean {
    const index = branch.branch - 1;
    let branch1 = null;
    if (this.isEven(index)) {
      branch1 = this.branches[index + 1];
    } else {
      branch1 = this.branches[index - 1];
    }
    if (branch.points > branch1.points) {
      return true;
    }
    return false;
  }

}
