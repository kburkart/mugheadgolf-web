import { TestBed, inject } from '@angular/core/testing';

import { BracketService } from './bracket.service';

describe('BracketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BracketService]
    });
  });

  it('should be created', inject([BracketService], (service: BracketService) => {
    expect(service).toBeTruthy();
  }));
});
