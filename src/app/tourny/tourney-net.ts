export interface ITourneyNet {
    id: number;
    golfer: String;
    round1: number;
    round2: number;
    round3: number;
    round4: number;
    toPar: number;
}
