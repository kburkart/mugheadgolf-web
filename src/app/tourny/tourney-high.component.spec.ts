import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourneyHighComponent } from './tourney-high.component';

describe('TourneyHighComponent', () => {
  let component: TourneyHighComponent;
  let fixture: ComponentFixture<TourneyHighComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourneyHighComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourneyHighComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
