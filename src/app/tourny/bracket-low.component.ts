import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { Entry } from './entry';
import { TourneyService } from './tourney.service';
import { ISeed } from './seed';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'app-bracket-low',
  templateUrl: './bracket-low.component.html',
  styleUrls: ['./bracket.component.css'],
  animations: [
    trigger('slide', [
      state('left', style({ transform: 'translateX(0)' })),
      state('right', style({ transform: 'translateX(-50%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class BracketLowComponent implements OnInit {

  lowSeeds: ISeed[];
  lowEntries: Entry[];
  errorMessage: String;
  isDataAvailable = false;
  constructor(private tourneyService: TourneyService) {}

  ngOnInit() {
    this.getBracketData();
  }

  getBracketData() {
    const requests: any[] = [];
    requests[0] = this.tourneyService.getLowBracketSeeds();
    forkJoin(requests).subscribe(
      results => this.processResults(results),
      (error: any) => this.errorMessage = <any>error
    );
  }

  processResults(results: any[]) {
    this.lowSeeds = results[0];
    this.lowEntries = [];
    let index = 0;

    this.lowSeeds.forEach(seed => {
      index = this.lowEntries.push(new Entry(seed, 'left'));
    });

     this.isDataAvailable = true;
  }
}
