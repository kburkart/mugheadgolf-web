import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BracketLowComponent } from './bracket-low.component';

describe('BracketLowComponent', () => {
  let component: BracketLowComponent;
  let fixture: ComponentFixture<BracketLowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BracketLowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BracketLowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
