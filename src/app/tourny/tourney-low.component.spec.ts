import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourneyLowComponent } from './tourney-low.component';

describe('TourneyLowComponent', () => {
  let component: TourneyLowComponent;
  let fixture: ComponentFixture<TourneyLowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourneyLowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourneyLowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
