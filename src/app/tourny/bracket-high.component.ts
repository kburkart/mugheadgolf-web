import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { Entry } from './entry';
import { TourneyService } from './tourney.service';
import { ISeed } from './seed';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'app-bracket-high',
  templateUrl: './bracket-high.component.html',
  styleUrls: ['./bracket.component.css'],
  animations: [
    trigger('slide', [
      state('left', style({ transform: 'translateX(0)' })),
      state('right', style({ transform: 'translateX(-50%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class BracketHighComponent implements OnInit {

  highSeeds: ISeed[];
  highEntries: Entry[];
  errorMessage: String;
  isDataAvailable = false;
  constructor(private tourneyService: TourneyService) {}

  ngOnInit() {
    this.getBracketData();
  }

  getBracketData() {
    const requests: any[] = [];
    requests[0] = this.tourneyService.getHighBracketSeeds();
    forkJoin(requests).subscribe(
      results => this.processResults(results),
      (error: any) => this.errorMessage = <any>error
    );
  }

  processResults(results: any[]) {
    this.highSeeds = results[0];
    this.highEntries = [];
    let index = 0;
    this.highSeeds.forEach(seed => {
      index = this.highEntries.push(new Entry(seed, 'left'));
    });

     this.isDataAvailable = true;
  }
}
