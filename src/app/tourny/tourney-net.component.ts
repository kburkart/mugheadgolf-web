import { Component, OnInit } from '@angular/core';
import { ITourneyNet } from './tourney-net';
import { TourneyService } from './tourney.service';

@Component({
  selector: 'app-tourney-net',
  templateUrl: './tourney-net.component.html',
  styleUrls: ['./bracket.component.css']
})
export class TourneyNetComponent implements OnInit {

  golferData: ITourneyNet[];
  errorMessage: String;

  constructor(private tourneyService: TourneyService) { }

  ngOnInit() {
    this.getTourneyData();
  }

  getTourneyData() {

    this.tourneyService.getTournetNetData()
    .subscribe(data => this. populateData(data),
    error => this.errorMessage = <any>error);
  }

  populateData(data: ITourneyNet[]) {
    this.golferData = data;
  }

}
