import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { IPoints } from './points';
import { IPointTotal } from './pointTotal';
import { environment } from '../../environments/environment';
import { isNull } from 'util';

@Injectable()
export class PointsService {
    private baseUrl = environment.url + 'points/';

    constructor(private http: Http) { }

    getPoints(idgolfer: number): Observable<IPoints[]> {
      const year = localStorage.getItem('year');

        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'golfer/' + idgolfer + '/' + year;

        return this.http.get(url, options)
            .map(response => response.json())
            // .do(data => console.log('get points: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    
    getAllPoints(): Observable<IPoints[]> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'all';

        return this.http.get(url, options)
            .map(response => response.json())
            // .do(data => console.log('get points: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getGolferTotalPoints(idgolfer: number): Observable<number> {
        const year = localStorage.getItem('year');
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'total/' + idgolfer + '/' + year;

        return this.http.get(url, options)
            .map(this.extractPoints)
            // .do(data => console.log('get points: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getDivisionLeaderPoints(iddivision: number): Observable<IPointTotal> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const url = this.baseUrl + 'divisionleader/' + iddivision;

        return this.http.get(url, options)
            .map(this.extractTotalPoints)
//  .do(data => console.log('get points: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractPoints(response: Response) {
        if (response.text() === '') {
             return 0;
        } else {
          return response.json();
        }
    }

    private extractTotalPoints(response: Response) {
        let points = {} as IPointTotal;
        if (response.text() === '') {
          points.totalpoints = 0;
          points.name = '';
          return points;
        } else {
            points = response.json();
            if ( isNull(points.totalpoints)) {
                points.totalpoints = 0;
            }
          return points;
        }
    }

    private handleError(error: Response): Observable<any> {
      // in a real world app, we may send the server to some remote logging infrastructure
      // instead of just logging it to the console
      console.error(error);
      return Observable.throw(error.json().error || 'Server error');
  }

}
