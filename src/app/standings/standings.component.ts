import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';



import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { DivisionsService } from '../divisions/divisions.service';
import { DivisionGolferService } from '../divisions/division-golfer.service';
import { GolferService } from '../golfer/golfer.service';

import { IGolfer } from '../golfer/golfer';


import { IDivision } from '../divisions/division';
import { IDivisionGolfer } from '../divisions/division-golfer';
import { PointsService } from './points.service';
import { IPoints } from './points';
import { SettingsService } from '../settings/settings.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Criteria } from '../sortable/criteria';
import { FwModule } from '../fw/fw.module';


@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})

export class StandingsComponent implements OnInit {

  public divisions: IDivision[];
  public divisionGolfers: any[] = [];
  public golferPoints: any[] = [];
  public golfers: any[] = [[], [], [], [],[],[],[]];
  public points: IPoints[] = [];
  public numbers: number[];
  public criteria: Criteria;
  errorMessage: string;
  pageTitle: string;

  constructor(private divisionsService: DivisionsService,
    private divisionGolferService: DivisionGolferService,
    private pointsService: PointsService,
    private settingsService: SettingsService) { }

  ngOnInit() {
    this.pageTitle = 'Standings';
    const year  = +localStorage.getItem('year');
    this.settingsService.getSettings(year).subscribe(settings => this.populateData(year, settings.totalweeks),
      error => this.errorMessage = <any>error);

  }

  populateData(year: number, numWeeks: number) {
    this.numbers = Array.from(Array(numWeeks)).map((x, i) => i);
    this.getDivisions(year);

  }

  getDivisions(year): void {
    this.divisionsService.getDivisionsByYear(year)
      .subscribe(
        (divisions: IDivision[]) => this.populateDivisions(divisions),
        (error: any) => this.errorMessage = <any>error
      );
  }

  populateDivisions(divisions: IDivision[]) {
    this.divisions = divisions;
    const size = divisions.length;
    for (let x = 0; x < size; x++) {
      this.getDivisionGolfers(x);
    }
  }

  getDivisionGolfers(index: number): void {
    const division = this.divisions[index];
    this.divisionGolferService.getDivisionGolfersByDivision(division.iddivision)
      .subscribe(
        (divisionGolfers: IDivisionGolfer[]) => this.populateDivisionGolfers(index, divisionGolfers),
        (error: any) => this.errorMessage = <any>error
      );
  }

  populateDivisionGolfers(index: number, divisionGolfers: IDivisionGolfer[]): void {
    //  this.divisionGolfers[index] = new Array().fill(divisionGolfers);
    const size = divisionGolfers.length;
    const requests: any[] = [];

    for (let x = 0; x < size; x++) {
      requests[x] = this.pointsService.getPoints(divisionGolfers[x].idgolfer);
    }

    forkJoin(requests).subscribe(
      results => this.processResults(index, results),
      (error: any) => this.errorMessage = <any>error
    );

  }

  processResults(index: number, results: any[]) {
    const size = results.length;
    for (let x = 0; x < size; x++) {
      this.populatePoints(index, x, results[x]);
    }
    const division = '' + index;
    this.golfers[index] = this.getSortedPoints({sortColumn: '16', sortDirection: 'asc', tableName: '1' }, index);
  }

  populatePoints(x: number, y: number, points: IPoints[]) {
    this.golfers[x][y] = this.addSumToPoints(points);
  }

  addSumToPoints(points: IPoints[]): IPoints[] {
    const size = points.length;
    let total = 0;
    for (let x = 0; x < size; x++) {
      total += +points[x].points;
    }
    const totalPoints: IPoints = this.initIPoints();
    totalPoints.points = +total;
    points[size] = totalPoints;
    return points;

  }
  initIPoints(): IPoints {
    return {
      id: 0,
      iddivision: null,
      idgolfer: null,
      name: null,
      week: null,
      points: null,
    };
  }

  onSorted($event, division: number) {
    this.criteria = $event;
    if (+this.criteria.tableName === division) {
      this.getSortedPoints($event, division);
    }
  }

  getSortedPoints(criteria: Criteria, division: number): IPoints[] {
    return this.golfers[division].sort((a, b) => {
      if (criteria.sortDirection === 'desc') {
        if (a[criteria.sortColumn].points < b[criteria.sortColumn].points) {
          return -1;
        } else if (a[criteria.sortColumn].points > b[criteria.sortColumn].points) {
          return 1;
        } else {
          return 0;
        }
      } else {
        if (a[criteria.sortColumn].points < b[criteria.sortColumn].points) {
          return 1;
        } else if (a[criteria.sortColumn].points > b[criteria.sortColumn].points) {
          return -1;
        } else {
          return 0;
        }
      }
    });
  }
}
