export interface IPoints {
    id: number;
    iddivision: number;
    idgolfer: number;
    name: string;
    week: number;
    points: number;
}

