export interface IPointTotal {
    id: number;
    iddivision: number;
    idgolfer: number;
    name: string;
    totalpoints: number;
}
