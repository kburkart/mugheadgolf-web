import { ComboChartConfig } from '../models/combo-chart-config';
import { Component, OnInit } from '@angular/core';
import { PieChartConfig } from '../models/pie-chart-config';
import { IStatsPie } from '../stats/stats-pie';
import { StatsService } from '../stats/stats.service';
import { GooglePieChartService } from '../services/google-pie-chart.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'Dashboard';

  data1: any[];
  config1: PieChartConfig;
  elementId1: String;

  data2: any[];
  config2: PieChartConfig;
  elementId2: String;
  pieData: IStatsPie;
  errorMessage: String;
  showChart = false;
  leagueAvg: number;
  golferAvg: number;
  divisionLeader: String;
  leaderPoints: number;
  yourPoints: number;
  yourMoney: number;
  divisionName: String;
  currentOpponent: String;
  currentOpponentHC: number;
  lastOpponent: String;
  lastOpponentPoints: number;
  lastPoints: number;
  yourHC: number;
  opponentAbsent: String;
  currentWeek: number;
  currentMatch: number;
  lastMatch: number;

  constructor(
    private statsService: StatsService,
    private pieChartService: GooglePieChartService,
    private route: ActivatedRoute,
    private router: Router,
) { }

  ngOnInit(): void {
    this.yourMoney = +localStorage.getItem('winnings');
    this.yourPoints = +localStorage.getItem('totalpoints');
    this.leaderPoints = +localStorage.getItem('divisionLeaderPoints');
    this.divisionLeader = localStorage.getItem('divisionLeader');
    this.divisionName = localStorage.getItem('divisionName');
    this.golferAvg = +localStorage.getItem('golferAvg');
    this.leagueAvg = +localStorage.getItem('leagueAvg');
    this.currentOpponent = localStorage.getItem('currentOpponent');
    this.currentOpponentHC = +localStorage.getItem('opponentHandicap');
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.lastOpponent = localStorage.getItem('lastOpponent');
    this.opponentAbsent = localStorage.getItem('opponentAbsent');
    this.lastOpponentPoints = +localStorage.getItem('lastOpponentPoints');
    this.lastPoints = +localStorage.getItem('lastPoints');
    this.yourHC = +localStorage.getItem('currentHandicap');
    this.currentMatch = +localStorage.getItem('currentMatch');
    this.lastMatch = +localStorage.getItem('lastMatch');
    const year  = +localStorage.getItem('year');
    this.populateData(year, +localStorage.getItem('idgolfer'));
  }

  gotoMatch(idschedule: number) {
//    console.log('goto match ' + idschedule);
    if (idschedule) {
      this.router.navigate(['/authenticated/match', idschedule]);
    }
  }

  populateData(year: number, idgolfer: number) {
    this.statsService.getPieChartData(idgolfer, year).subscribe(
      (pieData: IStatsPie) => {
                                this.pieData = pieData;
                                //Piechart1 Data & Config
                                this.data1 = this.pieData.GolferData;
                                this.config1 = new PieChartConfig('Golfer Totals', 0);
                                this.elementId1 = 'myPieChart1';

                                //Piechart2 Data & Config
                                this.data2 = this.pieData.LeagueData;
                                this.config2 = new PieChartConfig('League Totals', 0);
                                this.elementId2 = 'myPieChart2';
                                this.showChart = true;
                              },
      (error: any) => this.errorMessage = <any>error
    );
  }

}
