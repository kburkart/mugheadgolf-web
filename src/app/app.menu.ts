import {MenuItem} from './fw/services/menu.service';

export let initialMenuItems: Array<MenuItem> = [
  {
    text: 'Dashboard',
    icon: 'glyphicon glyphicon-dashboard',
    route: 'authenticated/dashboard',
    submenu: null
  },
  {
    text: 'Golfers',
    icon: 'glyphicon glyphicon-flag',
    route: null,
    submenu: [
      {
        text: 'Golfers',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/golfers',
        submenu: null
      },
      {
        text: 'Handicaps',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/handicaps',
        submenu: null
      }
    ],
  },
  {
    text: 'Schedule',
    icon: 'glyphicon glyphicon-calendar',
    route: null,
    submenu: [
      {
        text: 'Weekly Scores',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/weeklyScores',
        submenu: null
      },
      {
        text: 'Weekly Schedule',
        icon: 'glyphicon glyphicon-calendar',
        route: 'authenticated/weeklySchedule',
        submenu: null
      },
      {
        text: 'Weekly Tee Times',
        icon: 'glyphicon glyphicon-time',
        route: 'authenticated/foursomes',
        submenu: null
      },
      {
        text: 'Golfer Schedule',
        icon: 'glyphicon glyphicon-calendar',
        route: 'authenticated/golferSchedule',
        submenu: null
      }
    ],
  },
  {
    text: 'Scores',
    icon: 'glyphicon glyphicon-flag',
    route: null,
    submenu: [
      {
        text: 'Golfer Scores',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/golferScores',
        submenu: null
      },
      {
        text: 'Golfer Net Scores',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/golferNetScores',
        submenu: null
      }

    ],
  },
  {
    text: 'Stats',
    icon: 'glyphicon glyphicon-stats',
    route: null,
    submenu: [
      {
        text: 'Standings',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/standings',
        submenu: null
      },
      {
        text: 'Golfer Money',
        icon: 'glyphicon glyphicon-usd',
        route: 'authenticated/golferMoney',
        submenu: null
      },
      {
        text: 'Weekly Money',
        icon: 'glyphicon glyphicon-usd',
        route: 'authenticated/weeklyMoney',
        submenu: null
      },
      {
        text: 'Golfer Stats',
        icon: 'glyphicon glyphicon-stats',
        route: 'authenticated/weeklyStats',
        submenu: null
      },
      {
        text: 'League Stats',
        icon: 'glyphicon glyphicon-stats',
        route: 'authenticated/stats',
        submenu: null
      },
    ]
  },
  {
    text: 'Tourney',
    icon: 'glyphicon glyphicon-flag',
    route: null,
    submenu: [
      {
        text: 'Bracket Low Handicap',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/tourneyLow',
        submenu: null
      },
      {
        text: 'Bracket High Handicap',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/tourneyHigh',
        submenu: null
      },
      {
        text: 'Low Net',
        icon: 'glyphicon glyphicon-flag',
        route: 'authenticated/tourneyNet',
        submenu: null
      }
    ],
  },
  {
    text: 'Wager',
    icon: 'glyphicon glyphicon-tags',
    route: null,
    submenu: [
      {
        text: 'Pick Four Bet',
        icon: 'glyphicon glyphicon-tags',
        route: 'authenticated/wager',
        submenu: null
      },
      {
        text: 'Results',
        icon: 'glyphicon glyphicon-piggy-bank',
        route: 'authenticated/wagerResults',
        submenu: null
      }

    ],
  },

  {
    text: 'Admin',
    icon: 'glyphicon glyphicon-wrench',
    route: null,
    submenu: [
      {
        text: 'Calculate Handicaps',
        icon: 'glyphicon glyphicon-pencil',
        route: 'authenticated/calcHandicaps',
        submenu: null
      },
      {
        text: 'Select Year',
        icon: 'glyphicon glyphicon-time',
        route: 'authenticated/year',
        submenu: null
      },
      {
        text: 'Add Weekly Winners',
        icon: 'glyphicon glyphicon-cog',
        route: 'authenticated/addWinners',
        submenu: null
      },
      {
        text: 'Send Group Email',
        icon: 'glyphicon glyphicon-envelope',
        route: 'authenticated/email',
        submenu: null
      },
      {
        text: 'Golfer',
        icon: 'glyphicon glyphicon-th-list',
        route: null,
        submenu: [
          {
            text: 'Add Golfer',
            icon: 'glyphicon glyphicon-th-list',
            route: 'authenticated/addGolfer/0',
            submenu: null
          },
          {
            text: 'Edit Golfer',
            icon: 'glyphicon glyphicon-cog',
            route: 'authenticated/editGolfer',
            submenu: null
          }
        ]
      },
      {
        text: 'Course',
        icon: 'glyphicon glyphicon-flag',
        route: null,
        submenu: [
          {
            text: 'Add Course',
            icon: 'glyphicon glyphicon-th-list',
            route: 'authenticated/settings',
            submenu: null
          },
          {
            text: 'Edit Course',
            icon: 'glyphicon glyphicon-cog',
            route: 'authenticated/settings',
            submenu: null
          }
        ]
      },
      {
        text: 'Scores',
        icon: 'glyphicon glyphicon-cog',
        route: null,
        submenu: [
          {
            text: 'Edit Score',
            icon: 'glyphicon glyphicon-cog',
            route: 'authenticated/editScore',
            submenu: null
          }
        ]
      },
      {
        text: 'Schedules',
        icon: 'glyphicon glyphicon-cog',
        route: null,
        submenu: [
          {
            text: 'Create Schedule',
            icon: 'glyphicon glyphicon-th-list',
            route: 'authenticated/createSchedule',
            submenu: null
          },
          {
            text: 'Edit Schedule',
            icon: 'glyphicon glyphicon-cog',
            route: 'authenticated/settings',
            submenu: null
          }
        ]
      },
      {
        text: 'Division Setup',
        icon: 'glyphicon glyphicon-cog',
        route: 'authenticated/divisionSetup',
        submenu: null
      },
      {
        text: 'League Setup',
        icon: 'glyphicon glyphicon-cog',
        route: 'authenticated/settings',
        submenu: null
      },
      {
        text: 'Send Weekly Schedule',
        icon: 'glyphicon glyphicon-envelope',
        route: 'authenticated/sendSchedule',
        submenu: null
      }

    ]
  }
];
