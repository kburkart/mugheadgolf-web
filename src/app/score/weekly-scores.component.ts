import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ScheduleService } from '../schedule/schedule.service';

import { IGolfer } from '../golfer/golfer';

import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { ScoreService } from './score.service';
import { IScoredata } from './scoredata';
import { IWeeklyScores } from './weekly-scores';
import { Console } from '@angular/core/src/console';
import { ScreenService } from '../fw/services/screen.service';
import { IWeeklySchedule } from '../schedule/weekly-schedule';

import { AnonymousSubscription } from 'rxjs/Subscription';
import { IScorehole } from './scorehole';

@Component({
  selector: 'app-weekly-scores',
  templateUrl: './weekly-scores.component.html',
  styleUrls: ['./weekly-scores.component.css']
})
export class WeeklyScoresComponent implements OnInit, OnDestroy {

  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  weeklyScores: IWeeklyScores[];
  scoresForm: FormGroup;
  errorMessage: string;
  weeklySchedules: IWeeklySchedule[];
  pageTitle: string;
  start: number;
  currentWeek: number;
  selectedWeek: number;

  private timerSubscription: AnonymousSubscription;
  private postsSubscription: AnonymousSubscription;
  private autoRefresh: boolean;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private scoreService: ScoreService,
    private scheduleService: ScheduleService) { }

  ngOnInit() {
    this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
    this.start = 0;
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.selectedWeek = this.currentWeek;
   // console.log('Scores are for ' + this.selectedWeek);
    this.getSchedule();
    this.scoresForm = this.fb.group({
    });
  }

  public ngOnDestroy(): void {
    this.unsubscribe();
  }

  unsubscribe(): void {
    if (this.postsSubscription) {
      this.postsSubscription.unsubscribe();
    }
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  switchTabs(weeklySchedule) {
    this.selectedWeek = weeklySchedule.week;
    this.unsubscribe();
    this.populateSelectedWeek();
    this.pageTitle = 'Scores For Week ' + this.currentWeek;
  }

  populateSelectedWeek() {
    const year  = +localStorage.getItem('year');
    this.postsSubscription = this.scoreService.getWeeklyScores(year, this.selectedWeek)
      .subscribe(weeklyScores => {
        this.weeklyScores = weeklyScores;
        this.subscribeToData();
      },
      (error: any) => this.errorMessage = <any>error
      );

  }
  populateScores() {
    if (this.autoRefresh && (this.currentWeek === this.selectedWeek)) {
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
    const year  = +localStorage.getItem('year');
    this.postsSubscription = this.scoreService.getWeeklyScores(year, this.selectedWeek)
      .subscribe(weeklyScores => {
        this.weeklyScores = weeklyScores;
        this.subscribeToData();
      },
      (error: any) => this.errorMessage = <any>error
      );
    } else {
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
      this.subscribeToData();
    }
  }



  getSchedule(): void {
    const year  = +localStorage.getItem('year');
    this.scheduleService.getSchedule(year)
      .subscribe(weeklySchedules => {
        this.weeklySchedules = weeklySchedules;
        this.populateSelectedWeek();
      },
      (error: any) => this.errorMessage = <any>error
      );

  }

  getTotal(scoreholes: IScorehole[]): number {
    let total = 0;
    scoreholes.forEach(score => total += score.score);
    return total;
  }

  private subscribeToData(): void {
    this.timerSubscription = Observable.timer(15000).first().subscribe(() => this.populateScores());
  }
}
