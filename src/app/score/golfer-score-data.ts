import { IGolfer } from '../golfer/golfer';
import { IScore } from './score';

export interface IGolferScoreData {
    golfer: IGolfer;
    scores: IScore[];
}
