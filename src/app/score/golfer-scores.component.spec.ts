import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GolferScoresComponent } from './golfer-scores.component';

describe('GolferScoresComponent', () => {
  let component: GolferScoresComponent;
  let fixture: ComponentFixture<GolferScoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GolferScoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GolferScoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
