import { IScoredata } from './scoredata';

export interface IWeeklyScores {
    index: number;
    scores: IScoredata[];
}
