import { IGolfer } from '../golfer/golfer';
import { ISchedule } from '../schedule/schedule';

export interface IScore {
    idscore: number;
    score: number;
    net: number;
    adjustedscore: number;
    useforhandicap: number;
    handicapdifferential: number;
    idgolfer: IGolfer;
    idschedule: ISchedule;
}
