import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingsService } from '../settings/settings.service';

import { GolferService } from '../golfer/golfer.service';
import { IGolfer } from '../golfer/golfer';
import { ScoreService } from './score.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-score',
  templateUrl: './edit-score.component.html',
  styleUrls: ['./edit-score.component.css']
})
export class EditScoreComponent implements OnInit {

  golfers: IGolfer[];
  errorMessage: string;
  scoreForm: FormGroup;
  selectedWeek: number;
  selectedGolfer: IGolfer;
  pageTitle = 'Edit Scores';
  numbers: number[];

  constructor(private fb: FormBuilder,
    private golferService: GolferService,
    private settingsService: SettingsService,
    private scoreService: ScoreService) { }

  ngOnInit() {
    this.scoreForm = this.fb.group({
      golferlist: '',
      week: ''
    });

    const year  = +localStorage.getItem('year');
    this.settingsService.getSettings(year).subscribe(settings => this.populateData(settings.totalweeks),
      error => this.errorMessage = <any>error);

  }

  populateData( numWeeks: number) {
    this.numbers = Array.from(Array(numWeeks)).map((x, i) => i);
    this.getListOfGolfers();
  }
  getListOfGolfers(): void {
    this.golferService.getGolfers().subscribe(
      (golfers: IGolfer[]) => this.onGolfersRetrieved(golfers),
      (error: any) => this.errorMessage = <any>error
    );
  }
  onGolfersRetrieved(golfers: IGolfer[]): void {
    this.golfers = golfers;
    this.scoreForm.get('golferlist').valueChanges.subscribe((golfer: IGolfer) => {
      this.selectedGolfer = golfer;
      this.populateScore();
    });

    this.scoreForm.get('week').valueChanges.subscribe((week: number) => {
      this.selectedWeek = week;
      this.populateScore();
    });


  }
  populateScore(): void {
    if ((this.selectedWeek !== undefined) && (this.selectedGolfer !== undefined)) {
      // console.log('selected golfer is ' + this.selectedGolfer.lastname);
      // console.log('selected week is ' + this.selectedWeek);
    }
  }
}
