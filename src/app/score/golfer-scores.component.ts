import { Component, OnInit, Input, EventEmitter, OnDestroy, HostListener } from '@angular/core';
import { ScoreService } from './score.service';
import { IGolfer } from '../golfer/golfer';
import { IScore } from './score';
import { IGolferScoreData } from './golfer-score-data';
import { SortService } from '../sortable/sort.service';
import { Subscription } from 'rxjs/Subscription';
import { Criteria } from '../sortable/criteria';


@Component({
  selector: 'app-golfer-scores',
  templateUrl: './golfer-scores.component.html',
  styleUrls: ['./golfer-scores.component.css']
})
export class GolferScoresComponent implements OnInit, OnDestroy {
  pageTitle = 'Golfer Weekly Scores';
  errorMessage: string;

  golferScoreData: IGolferScoreData[];
  numbers: number[];
  blankRows: number[];
  criteria: Criteria;

  constructor(private scoreService: ScoreService,
              private sortService: SortService) { }

  ngOnInit(): void {
    this.numbers = Array.from(Array(16)).map((x, i) => i);
    const year  = +localStorage.getItem('year');
    this.scoreService.getGolfersScores(year)
      .subscribe(golferScoreData => this. populateScores(golferScoreData),
      error => this.errorMessage = <any>error);
  }

  populateScores(scoreData: IGolferScoreData[]) {
    this.golferScoreData = scoreData;
    this.golferScoreData.forEach(golferData => {

      const scores = golferData.scores;
      scores.forEach(score => {
        if (score.score === 0) {
          score.score = null;
        }
      });
      const numBlankScores = 16 - scores.length;
      for ( let x = 0; x < numBlankScores; x++) {
        const score = this.initializeScore();
        scores.push(score);
      }
      const avgScore = this.initializeScore();
      avgScore.score = this.getAvg(scores);
      scores.push(avgScore);
      golferData.scores = scores;

    });
    this.golferScoreData = this.getSortedScores({sortColumn: '16', sortDirection: 'desc', tableName: '1' });
  }

  getAvg(scores: IScore[]): number {

    let total = 0;
    let totalScores = scores.length;

    scores.forEach(score => {
      if (score.score > 0) {
        total += score.score;
      } else {
        totalScores--;
      }
    });
    let average = null;
    if (totalScores > 0) {
      average = total / totalScores;
    }
    return average;
  }

  ngOnDestroy(): void {
  }

    initializeScore(): IScore {
      return {
    idscore: 0,
    score: null,
    net: null,
    adjustedscore: 0,
    useforhandicap: 0,
    handicapdifferential: 0,
    idgolfer: null,
    idschedule: null
    };
}

  onSorted($event, table: number) {
      this.getSortedScores($event);
  }

  getSortedScores(criteria: Criteria): IGolferScoreData[] {
      return this.golferScoreData.sort((a, b) => {
        let rc = 0;
        if (criteria.sortDirection === 'desc') {

          if (a.scores[criteria.sortColumn].score === null) {
            rc =  1;
          } else if (b.scores[criteria.sortColumn].score === null) {
            rc =  -1;
          } else if (a.scores[criteria.sortColumn].score < b.scores[criteria.sortColumn].score) {
            rc =  -1;
          } else if (a.scores[criteria.sortColumn].score > b.scores[criteria.sortColumn].score) {
            rc =  1;
          } else {
            rc =  0;
          }
        } else {
          if (b.scores[criteria.sortColumn].score === null) {
            rc =  -1;
          } else if (a.scores[criteria.sortColumn].score === null) {
            rc =  1;
          } else if (a.scores[criteria.sortColumn].score < b.scores[criteria.sortColumn].score) {
            rc =  1;
          } else if (a.scores[criteria.sortColumn].score > b.scores[criteria.sortColumn].score) {
            rc =  -1;
          } else {
            rc = 0;
          }
        }
        return rc;
      });
  }
}


