import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { SlicePipe } from '@angular/common';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ScoreService } from './score.service';
import { CourseService } from '../course/course.service';
import { GolferService } from '../golfer/golfer.service';

import { IGolfer } from '../golfer/golfer';
import { IScore } from './score';
import { IScoredata } from './scoredata';
import { ICoursedata } from '../course/coursedata';
import { ICourse } from '../course/course';
import { ICourseholes } from '../course/coursehole';
import { IScorehole } from './scorehole';
import { forkJoin } from 'rxjs/observable/forkJoin';


import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { ISchedule } from '../schedule/schedule';
import { ScheduleService } from '../schedule/schedule.service';
import { AnonymousSubscription } from 'rxjs/Subscription';
import {EmptyObservable} from 'rxjs/observable/EmptyObservable';
import { IFoursome } from '../foursome/foursome';
import { FoursomeService } from '../foursome/foursome.service';

@Component({
  selector: 'app-foursome-score',
  templateUrl: './foursome-score.component.html',
  styleUrls: ['./foursome-score.component.css']
})
export class FoursomeScoreComponent implements OnInit, OnDestroy {

  scoreForm: FormGroup;
  errorMessage: string;
  titleBar1: String;
  titleBar2: String;
  name1: string;
  name2: string;

  isAdmin: boolean;
  isBye1: boolean;
  isBye2: boolean;

  isAbsent1: boolean;
  isAbsent2: boolean;

  hc1: number;
  hc2: number;
  public golfers: IGolfer[];
  public schedule1: ISchedule;
  public schedule2: ISchedule;
  public foursome: IFoursome;

  private sub: Subscription;


  private coursedata: ICoursedata;
  private scoredata: IScoredata[];
  public cssTotal1: string;
  public cssTotal2: string;


  private holes1: IScorehole[];
  private holes2: IScorehole[];

  private cssHoles1: string[];
  private cssHoles2: string[];

  private hcHole1: number[];
  private hcHole2: number[];

  private net1: number[];
  private net2: number[];

  private points1: number[];
  private points2: number[];

  public total1: number;
  public total2: number;

  private netTotal1: number;
  private netTotal2: number;

  private xtraStroke1: number;
  private xtraStroke2: number;

  private strokes1: number;
  private strokes2: number;

  public pointTotal1: number;
  public pointTotal2: number;

  private id1: number[];
  private id2: number[];
  public firstname1: string;
  public firstname2: string;
  titleBar3: String;
  titleBar4: String;
  name3: string;
  name4: string;

  isBye3: boolean;
  isBye4: boolean;

  isAbsent3: boolean;
  isAbsent4: boolean;

  hc3: number;
  hc4: number;
  public schedule3: ISchedule;
  public schedule4: ISchedule;

  public cssTotal3: string;
  public cssTotal4: string;


  private holes3: IScorehole[];
  private holes4: IScorehole[];

  private cssHoles3: string[];
  private cssHoles4: string[];

  private hcHole3: number[];
  private hcHole4: number[];

  private net3: number[];
  private net4: number[];

  private points3: number[];
  private points4: number[];

  public total3: number;
  public total4: number;

  private netTotal3: number;
  private netTotal4: number;

  private xtraStroke3: number;
  private xtraStroke4: number;

  private strokes3: number;
  private strokes4: number;

  public pointTotal3: number;
  public pointTotal4: number;

  private id3: number[];
  private id4: number[];
  public firstname3: string;
  public firstname4: string;
  private start: number;
  private useBack: boolean;
  public totalYards: number;
  public totalPar: number;
  public inOut: string;
  private timerSubscription: AnonymousSubscription;
  private autoRefresh: boolean;
  private canSave1: boolean;
  private canSave2: boolean;
  private canSave3: boolean;
  private canSave4: boolean;


  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private scoreService: ScoreService,
    private golferService: GolferService,
    private foursomeService: FoursomeService,
    private courseService: CourseService,
    private scheduleService: ScheduleService) { }




  ngOnInit(): void {

    this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
    this.isAdmin = (localStorage.getItem('isAdmin') === 'true');
    this.titleBar1 = '';
    this.titleBar2 = '';
    this.titleBar3 = '';
    this.titleBar4 = '';
    this.holes1 = [null, null, null, null, null, null, null, null, null];
    this.holes2 = [null, null, null, null, null, null, null, null, null];
    this.holes3 = [null, null, null, null, null, null, null, null, null];
    this.holes4 = [null, null, null, null, null, null, null, null, null];

    this.useBack = false;
    this.total1 = 0;
    this.total2 = 0;
    this.hc1 = 0;
    this.hc2 = 0;
    this.pointTotal1 = 0;
    this.pointTotal2 = 0;
    this.cssHoles1 = ['centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone'];
    this.cssHoles2 = ['centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone'];
    this.id1 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.id2 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.hcHole1 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.hcHole2 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.points1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.points2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.net1 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.net2 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.start = 0;
    this.strokes1 = 0;
    this.strokes2 = 0;
    this.canSave1 = false;
    this.canSave2 = false;

    this.total3 = 0;
    this.total4 = 0;
    this.hc3 = 0;
    this.hc4 = 0;
    this.pointTotal3 = 0;
    this.pointTotal4 = 0;
    this.cssHoles3 = ['centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone'];
    this.cssHoles4 = ['centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone'];
    this.id3 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.id4 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.hcHole3 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.hcHole4 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.points3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.points4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.net3 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.net4 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.start = 0;
    this.strokes3 = 0;
    this.strokes4 = 0;
    this.canSave3 = false;
    this.canSave4 = false;

    this.scoreForm = this.fb.group({
      useforhc1: '',
      useforhc2: '',
      useforhc3: '',
      useforhc4: '',
      backnine: '',
      matchholes: this.fb.array([]),
    });

    for (let i = 0; i < 9; i++) {
      this.addHole(i);
    }
    this.sub = this.route.params.subscribe(
      params => {
        const idfoursome = +params['idfoursome'];
        this.getFoursomeInfo(idfoursome);
      });

  }

  private subscribeToData(): void {
    this.timerSubscription = Observable.timer(10000).first().subscribe(() => this.getFoursomeScores());
  }

  unsubscribe(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  getFoursomeInfo(id: number) {
    this.foursomeService.getFoursomeById(id)
      .subscribe(foursome => this.getData(foursome),
        (error: any) => this.errorMessage = <any>error
      );
  }


  getData(foursome: IFoursome) {
    this.schedule1 = foursome.idschedule1;
    this.schedule2 = foursome.idschedule2;
    const schedule1 = this.schedule1;
    const schedule2 = this.schedule2;
    const requests: any[] = [];
    requests[0] = this.scoreService.getMatchScore(schedule1.idschedule, schedule1.idgolfer1.idgolfer);
    requests[1] = this.scoreService.getMatchScore(schedule1.idschedule, schedule1.idgolfer2.idgolfer);
    requests[2] = this.scoreService.getMatchScore(schedule2.idschedule, schedule2.idgolfer1.idgolfer);
    requests[3] = this.scoreService.getMatchScore(schedule2.idschedule, schedule2.idgolfer2.idgolfer);
    requests[4] = this.courseService.getCourseData(schedule1.idcourse.idcourse);
    forkJoin(requests).subscribe(
      results => this.processResults(results),
      (error: any) => this.errorMessage = <any>error
    );
  }

  canSave() {
    return this.canSave1;
  }

  getFoursomeScores() {
    const time = (new Date()).toLocaleTimeString();

    if (this.autoRefresh) {
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
//      console.log('updating scores ' + time);
    const requests: any[] = [];
    requests[0] = this.scoreService.getMatchScore(this.schedule1.idschedule, this.schedule1.idgolfer1.idgolfer);
    requests[1] = this.scoreService.getMatchScore(this.schedule1.idschedule, this.schedule1.idgolfer2.idgolfer);
    requests[2] = this.scoreService.getMatchScore(this.schedule2.idschedule, this.schedule2.idgolfer1.idgolfer);
    requests[3] = this.scoreService.getMatchScore(this.schedule2.idschedule, this.schedule2.idgolfer2.idgolfer);
    forkJoin(requests).subscribe(
      results => this.processScores(results),
      (error: any) => this.errorMessage = <any>error
    );
    } else {
//      console.log('not updating scores ' + time);
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
      this.subscribeToData();
    }
  }


  processScores(results: any[]) {
    const scoreData = [results[0], results[1], results[2], results[3]];
    this.setScoreData(scoreData);
    this.populateHoleScore();
    this.subscribeToData();

  }

  processResults(results: any[]) {
    const scoreData = [results[0], results[1], results[2], results[3]];
    this.setScoreData(scoreData);
    this.setCourseData(results[4]);
    this.subscribeToData();
    let par = 0;
    let yards = 0;
    let start = 0;
    let end = 9;
    const holes = this.coursedata.courseholes;
    if (this.useBack) {
      start = 9;
      end = 18;
    }
    for (let x = start; x < end; x++) {
      par += +(holes[x].par);
      yards += +(holes[x].length);
    }
    this.totalYards = yards;
    this.totalPar = par;
  }

  setScoreData(scoredata: IScoredata[]): void {

    this.scoredata = scoredata;
    this.schedule1 = scoredata[0].score.idschedule;
    this.schedule2 = scoredata[2].score.idschedule;
    if (this.schedule1.backnine === 1) {
      this.start = 9;
      this.useBack = true;
      this.scoreForm.get('backnine').patchValue(true);
      this.inOut = 'In';

    } else {
      this.start = 0;
      this.useBack = false;
      this.scoreForm.get('backnine').patchValue(false);
      this.inOut = 'Out';

    }

    if (scoredata[0].score.useforhandicap === 1) {
      this.scoreForm.get('useforhc1').patchValue(true);
    } else {
      this.scoreForm.get('useforhc1').patchValue(false);
    }
    if (scoredata[1].score.useforhandicap === 1) {
      this.scoreForm.get('useforhc2').patchValue(true);
    } else {
      this.scoreForm.get('useforhc2').patchValue(false);
    }
    if (scoredata[2].score.useforhandicap === 1) {
      this.scoreForm.get('useforhc3').patchValue(true);
    } else {
      this.scoreForm.get('useforhc3').patchValue(false);
    }
    if (scoredata[3].score.useforhandicap === 1) {
      this.scoreForm.get('useforhc4').patchValue(true);
    } else {
      this.scoreForm.get('useforhc4').patchValue(false);
    }
    this.scoreForm.get('backnine').valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.backNineIndicador(value));

    // first match
    this.firstname1 = scoredata[0].score.idgolfer.firstname;
    this.firstname2 = scoredata[1].score.idgolfer.firstname;
    this.hc1 = Math.round(this.schedule1.handicap1);
    this.hc2 = Math.round(this.schedule1.handicap2);
      this.isAbsent1 = this.schedule1.absent1;
      this.isAbsent2 = this.schedule1.absent2;
    if (scoredata[0].score.idgolfer.idgolfer === 0) {
      this.isBye1 = true;
    } else {
      this.isBye1 = false;
    }
    if (scoredata[1].score.idgolfer.idgolfer === 0) {
      this.isBye2 = true;
    } else {
      this.isBye2 = false;
    }

    this.name1 = this.schedule1.idgolfer1.firstname + ' ' + this.schedule1.idgolfer1.lastname;
    this.name2 = this.schedule1.idgolfer2.firstname + ' ' + this.schedule1.idgolfer2.lastname;


    if (!this.isBye1 && !this.isBye2) {
      this.strokes1 = +this.hc1 - +this.hc2;
      if (+this.strokes1 < 0) {
        this.strokes1 = 0;
      }
      this.strokes2 = +this.hc2 - +this.hc1;
      if (+this.strokes2 < 0) {
        this.strokes2 = 0;
      }
    } else {
      this.strokes1 = 0;
      this.strokes2 = 0;
    }

    // console.log('hc1 is ' + this.hc1);
    // console.log('hc2 is ' + this.hc2);
    // console.log('strokes1 is ' + this.strokes1);
    // console.log('strokes2 is ' + this.strokes2);

     this.xtraStroke1 = Math.floor(this.strokes1 / 9);
     this.xtraStroke2 = Math.floor(this.strokes2 / 9);

    // second match
    this.firstname3 = scoredata[2].score.idgolfer.firstname;
    this.firstname4 = scoredata[3].score.idgolfer.firstname;
    this.hc3 = Math.round(this.schedule2.handicap1);
    this.hc4 = Math.round(this.schedule2.handicap2);
    // console.log('hc3 is ' + this.hc3);
    // console.log('hc4 is ' + this.hc4);
    this.isAbsent3 = this.schedule2.absent1;
    this.isAbsent4 = this.schedule2.absent2;
    if (scoredata[2].score.idgolfer.idgolfer === 0) {
      this.isBye3 = true;
    } else {
      this.isBye3 = false;
    }
    if (scoredata[3].score.idgolfer.idgolfer === 0) {
      this.isBye4 = true;
    } else {
      this.isBye4 = false;
    }

    this.name3 = this.schedule2.idgolfer1.firstname + ' ' + this.schedule2.idgolfer1.lastname;
    this.name4 = this.schedule2.idgolfer2.firstname + ' ' + this.schedule2.idgolfer2.lastname;


    if (!this.isBye3 && !this.isBye4) {
      this.strokes3 = +this.hc3 - +this.hc4;
      if (+this.strokes3 < 0) {
        this.strokes3 = 0;
      }
      this.strokes4 = +this.hc4 - +this.hc3;
      if (+this.strokes4 < 0) {
        this.strokes4 = 0;
      }
    } else {
      this.strokes3 = 0;
      this.strokes4 = 0;
    }

    // console.log('hc3 is ' + this.hc3);
    // console.log('hc4 is ' + this.hc4);
    // console.log('strokes3 is ' + this.strokes3);
    // console.log('strokes4 is ' + this.strokes4);

     this.xtraStroke3 = Math.floor(this.strokes3 / 9);
     this.xtraStroke4 = Math.floor(this.strokes4 / 9);

  }

  setCourseData(coursedata: ICoursedata): void {
    this.coursedata = coursedata;
    this.calculateHCHoles();
    this.calculateNetHoles();
    this.populateHoleScore();
    this.setupValueChangeSubs();

  }

  calculateHCHoles(): void {
// first match
    const r1 = +this.strokes1 % 9;
    const r2 = +this.strokes2 % 9;

//    console.log('r1 is ' + r1);
//    console.log('r2 is ' + r2);

    let index = 0;
    if (this.useBack) {
      index = 9;
    }
    for (let i = 0; i < 9; i++) {
      const hc = this.coursedata.courseholes[i + index].handicap;

 //     console.log('handicap is ' + hc)
      if (+r1 >= +hc) {
        this.hcHole1[i] = 1;
      } else {
        this.hcHole1[i] = 0;
      }
      if (+r2 >= +hc) {
        this.hcHole2[i] = 1;
      } else {
        this.hcHole2[i] = 0;
      }
    }
    // second match
    const r3 = +this.strokes3 % 9;
    const r4 = +this.strokes4 % 9;

    // console.log('r3 is ' + r3);
    // console.log('r4 is ' + r4);

    index = 0;
    if (this.useBack) {
      index = 9;
    }
    for (let i = 0; i < 9; i++) {
      const hc = this.coursedata.courseholes[i + index].handicap;

  //    console.log('handicap is ' + hc)
      if (+r3 >= +hc) {
        this.hcHole3[i] = 1;
      } else {
        this.hcHole3[i] = 0;
      }
      if (+r4 >= +hc) {
        this.hcHole4[i] = 1;
      } else {
        this.hcHole4[i] = 0;
      }
    }

  }
//  FIRST MATCH
  calculatePoint(hole: number) {
    let score1 = this.holes1[hole].score;
    let score2 = this.holes2[hole].score;

    if ((+score1 !== 0) && (+score2 !== 0)) {

      score1 -= (+this.xtraStroke1 + this.hcHole1[hole]);
      score2 -= (+this.xtraStroke2 + this.hcHole2[hole]);

         // console.log('score1 is ' + score1);
         // console.log('score2 is ' + score2);
      if (+score1 === +score2) {
        this.points1[hole] = .5;
        this.points2[hole] = .5;
      } else if (+score1 > +score2) {
        this.points1[hole] = 0;
        this.points2[hole] = 1;
      } else {
        this.points1[hole] = 1;
        this.points2[hole] = 0;
      }
    }
    this.cssHoles1[hole] = this.getCSS('1', hole);
    this.cssHoles2[hole] = this.getCSS('2', hole);

    //  SECOND MATCH
    let score3 = this.holes3[hole].score;
    let score4 = this.holes4[hole].score;

    if ((+score3 !== 0) && (+score4 !== 0)) {

      // console.log('xtraStroke3 is ' + this.xtraStroke3);
      // console.log('xtraStroke4 is ' + this.xtraStroke4);
      //
      // console.log('hcHole3 is ' + this.hcHole3[hole]);
      // console.log('hcHole4 is ' + this.hcHole4[hole]);

      score3 -= (+this.xtraStroke3 + this.hcHole3[hole]);
      score4 -= (+this.xtraStroke4 + this.hcHole4[hole]);

      // console.log('score3 is ' + score3);
      // console.log('score4 is ' + score4);

      if (+score3 === +score4) {
        this.points3[hole] = .5;
        this.points4[hole] = .5;
      } else if (+score3 > +score4) {
        this.points3[hole] = 0;
        this.points4[hole] = 1;
      } else {
        this.points3[hole] = 1;
        this.points4[hole] = 0;
      }
    }
    this.cssHoles3[hole] = this.getCSS('3', hole);
    this.cssHoles4[hole] = this.getCSS('4', hole);


  }
  calculateNetHoles(): void {

    const xtraStroke1 = Math.floor(+this.hc1 / 9);
    const xtraStroke2 = Math.floor(+this.hc2 / 9);

    this.net1.forEach((value, key) => {
      this.net1[key] = -xtraStroke1;
    });
    this.net2.forEach((value, key) => {
      this.net2[key] = -xtraStroke2;
    });

    const R1 = Math.floor(+this.hc1 % 9);
    const R2 = Math.floor(+this.hc2 % 9);

    // console.log('Extra 1 is ' + xtraStroke1);
    // console.log('Extra 2 is ' + xtraStroke2);
    // console.log('R 1 is ' + R1);
    // console.log('R 2 is ' + R2);

    let index = 0;
    if (this.useBack) {
      index = 9;
    }
    for (let i = 0; i < 9; i++) {
      const hc = this.coursedata.courseholes[i + index].handicap;

      if (+R1 >= +hc) {
        this.net1[i] -= 1;
      }
      if (+R2 >= +hc) {
        this.net2[i] -= 1;
      }

    }

    // SECOND MATCH

    const xtraStroke3 = Math.floor(+this.hc3 / 9);
    const xtraStroke4 = Math.floor(+this.hc4 / 9);

    this.net3.forEach((value, key) => {
      this.net3[key] = -xtraStroke3;
    });
    this.net4.forEach((value, key) => {
      this.net4[key] = -xtraStroke4;
    });

    const R3 = Math.floor(+this.hc3 % 9);
    const R4 = Math.floor(+this.hc4 % 9);

    // console.log('Extra 3 is ' + xtraStroke3);
    // console.log('Extra 4 is ' + xtraStroke4);
    // console.log('R 3 is ' + R3);
    // console.log('R 4 is ' + R4);

    index = 0;
    if (this.useBack) {
      index = 9;
    }
    for (let i = 0; i < 9; i++) {
      const hc = this.coursedata.courseholes[i + index].handicap;

      if (+R3 >= +hc) {
        this.net3[i] -= 1;
      }
      if (+R4 >= +hc) {
        this.net4[i] -= 1;
      }

    }
  }

  addHole(i): void {

    const control = <FormArray>this.scoreForm.get('matchholes');
    control.push(this.createHole(i));

  }

  populateHoleScore() {


    let numDBScore1: number;
    let numDBScore2: number;


    if (this.scoredata[0].hasOwnProperty('scoreholes')) {
      numDBScore1 = this.scoredata[0].scoreholes.length;
    } else {
      numDBScore1 = 0;
    }

    if (this.scoredata[1].hasOwnProperty('scoreholes')) {
      numDBScore2 = this.scoredata[1].scoreholes.length;
    } else {
      numDBScore2 = 0;
    }

    for (let x = 0; x < numDBScore1; x++) {
      this.holes1[x] = this.scoredata[0].scoreholes[x];
      const array = this.scoreForm.get('matchholes') as FormArray;
      const score = array.at(x).get('score1');
      score.patchValue(this.holes1[x].score);
    }
    for (let x = 0; x < numDBScore2; x++) {
      this.holes2[x] = this.scoredata[1].scoreholes[x];
      const array = this.scoreForm.get('matchholes') as FormArray;
      const score = array.at(x).get('score2');
      score.patchValue(this.holes2[x].score);
    }

    for (let x = +numDBScore1; x < 9; x++) {
      this.holes1[x] = this.initHole(x, 0);
    }
    for (let x = +numDBScore2; x < 9; x++) {
      this.holes2[x] = this.initHole(x, 1);
    }


    // Second Match

    let numDBScore3: number;
    let numDBScore4: number;


    if (this.scoredata[2].hasOwnProperty('scoreholes')) {
      numDBScore3 = this.scoredata[2].scoreholes.length;
    } else {
      numDBScore3 = 0;
    }

    if (this.scoredata[3].hasOwnProperty('scoreholes')) {
      numDBScore4 = this.scoredata[3].scoreholes.length;
    } else {
      numDBScore4 = 0;
    }


    for (let x = 0; x < numDBScore3; x++) {
      this.holes3[x] = this.scoredata[2].scoreholes[x];
      const array = this.scoreForm.get('matchholes') as FormArray;
      const score = array.at(x).get('score3');
      score.patchValue(this.holes3[x].score);
    }
    for (let x = 0; x < numDBScore4; x++) {
      this.holes4[x] = this.scoredata[3].scoreholes[x];
      const array = this.scoreForm.get('matchholes') as FormArray;
      const score = array.at(x).get('score4');
      score.patchValue(this.holes4[x].score);
    }

    for (let x = +numDBScore3; x < 9; x++) {
      this.holes3[x] = this.initHole(x, 2);
    }
    for (let x = +numDBScore4; x < 9; x++) {
      this.holes4[x] = this.initHole(x, 3);
    }

    for (let x = 0; x < 9; x++) {
      this.calculatePoint(x);
    }

    this.updateTotals();

  }
  setupValueChangeSubs() {
    for (let x = 0; x < 9; x++) {
      this.calculatePoint(x);
      const array = this.scoreForm.get('matchholes') as FormArray;
      let score = array.at(x).get('score1');
      score.valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.changeScore1(x, value));
      score = array.at(x).get('score2');
      score.valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.changeScore2(x, value));
      score = array.at(x).get('score3');
      score.valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.changeScore3(x, value));
      score = array.at(x).get('score4');
      score.valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.changeScore4(x, value));

    }

  }


  createHole(i): FormGroup {
    let newGroup: FormGroup;
    newGroup = this.fb.group({
      score1: ['', this.myRequired(this.isBye1)],
      score2: ['', this.myRequired(this.isBye2)],
      score3: ['', this.myRequired(this.isBye3)],
      score4: ['', this.myRequired(this.isBye4)]
    });

    return newGroup;

  }

  initHole(hole: number, golfer: number): IScorehole {
    return {
      idscorehole: 0,
      idscore: this.scoredata[golfer].score,
      hole: +hole + 1,
      score: null,
      net: null,
      adjustedscore: null,
      type: null
    };
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.unsubscribe();

  }

  myRequired(isBye: boolean) {
    return function (input: FormControl) {
      if ((input.value && !isNaN(input.value)) || isBye) {
        return null;
      } else {
        return { 'required': true };
      }
    };
  }

  getControlName(c: AbstractControl): string | null {
    const formGroup = c.parent.controls;
    return Object.keys(formGroup).find(name => c === formGroup[name]) || null;
  }

  backNineIndicador(isChecked): void {

    if (isChecked) {
      this.useBack = true;
      this.start = 9;
      this.schedule1.backnine = 1;
      this.schedule2.backnine = 1;

    } else {
      this.useBack = false;
      this.start = 0;
      this.schedule1.backnine = 0;
      this.schedule2.backnine = 0;

    }

    const requests: any[] = [];
    requests[0] = this.scheduleService.saveBacknine(this.schedule1);
    requests[1] = this.scheduleService.saveBacknine(this.schedule2);
    forkJoin(requests).subscribe(
      () => this.onSaveBacknineComplete(),
      (error: any) => this.errorMessage = <any>error
    );
  }

  onSaveBacknineComplete(): void {
    //    console.log('Backnine saved, recalculating points');
    this.calculateHCHoles();
    this.calculateNetHoles();
    this.updateNet();
    for (let x = 0; x < 9; x++) {
      this.calculatePoint(x);
    }
    this.updateTotals();
  }

  saveHole(hole: number, golfer: number): void {


    // Copy the form values over the golfer object values
    let scorehole: IScorehole;

    switch (golfer) {
      case 0:
        scorehole = this.holes1[hole];
        break;
      case 1:
        scorehole = this.holes2[hole];
        break;
      case 2:
        scorehole = this.holes3[hole];
        break;
      case 3:
        scorehole = this.holes4[hole];
        break;
    }

    // console.log('saving hole ' + JSON.stringify(scorehole));
    this.scoreService.saveScorehole(scorehole)
      .subscribe(
        (newScorehole) => this.onSaveHoleComplete(newScorehole, hole, golfer),
        (error: any) => this.errorMessage = <any>error
      );
  }

  onSaveHoleComplete(scorehole: IScorehole, hole: number, golfer: number): void {
    switch (golfer) {
      case 0:
        this.holes1[hole] = scorehole;
        break;
      case 1:
        this.holes2[hole] = scorehole;
        break;
      case 2:
        this.holes3[hole] = scorehole;
        break;
      case 3:
        this.holes4[hole] = scorehole;
        break;
    }

  }

  updateNet() {
    for (let x = 0; x < this.holes1.length; x++) {
      if (this.holes1[x] !== null) {
        this.changeScore1(x, this.holes1[x].score);
      }
    }
    for (let x = 0; x < this.holes2.length; x++) {
      if (this.holes2[x] !== null) {
        this.changeScore2(x, this.holes2[x].score);
      }
    }
    for (let x = 0; x < this.holes3.length; x++) {
      if (this.holes3[x] !== null) {
        this.changeScore3(x, this.holes3[x].score);
      }
    }
    for (let x = 0; x < this.holes4.length; x++) {
      if (this.holes4[x] !== null) {
        this.changeScore4(x, this.holes4[x].score);
      }
    }

  }


  changeScore1(hole: number, score: number) {
    this.holes1[hole].score = score;
    this.holes1[hole].net = +score + +this.net1[hole];
    this.calculatePoint(hole);
    this.calculateScoreType(hole, this.holes1);
    this.updateTotals();
    this.saveHole(hole, 0);
    this.saveScore(false);

  }

  changeScore2(hole: number, score: number) {
    this.holes2[hole].score = score;
    this.holes2[hole].net = +score + +this.net2[hole];
    this.calculatePoint(hole);
    this.calculateScoreType(hole, this.holes2);
    this.updateTotals();
    this.saveHole(hole, 1);
    this.saveScore(false);
  }

  changeScore3(hole: number, score: number) {
    this.holes3[hole].score = score;
    this.holes3[hole].net = +score + +this.net3[hole];
    this.calculatePoint(hole);
    this.calculateScoreType(hole, this.holes3);
    this.updateTotals();
    this.saveHole(hole, 2);
    this.saveScore(false);

  }

  changeScore4(hole: number, score: number) {
    this.holes4[hole].score = score;
    this.holes4[hole].net = +score + +this.net4[hole];
    this.calculatePoint(hole);
    this.calculateScoreType(hole, this.holes4);
    this.updateTotals();
    this.saveHole(hole, 3);
    this.saveScore(false);
  }

  updateTotals(): void {

    this.canSave2 = true;
    this.total1 = this.updateScoreTotal(this.holes1, 1);
    this.total2 = this.updateScoreTotal(this.holes2, 2);
    this.total3 = this.updateScoreTotal(this.holes3, 3);
    this.total4 = this.updateScoreTotal(this.holes4, 4);
    this.canSave1 = this.canSave2;
    this.netTotal1 = this.updateNetTotal(this.holes1);
    this.netTotal2 = this.updateNetTotal(this.holes2);
    this.netTotal3 = this.updateNetTotal(this.holes3);
    this.netTotal4 = this.updateNetTotal(this.holes4);

    if ((this.total1 > 0) && (this.total2 > 0)) {

      if (+this.netTotal1 > +this.netTotal2) {
        this.points1[9] = 0;
        this.points2[9] = 1;
      } else if (+this.netTotal1 < +this.netTotal2) {
        this.points1[9] = 1;
        this.points2[9] = 0;
      } else {
        this.points1[9] = .5;
        this.points2[9] = .5;
      }

      this.pointTotal1 = this.updateTotal(this.points1);
      this.pointTotal2 = this.updateTotal(this.points2);

      this.cssTotal1 = this.getCSS('1', 9);
      this.cssTotal2 = this.getCSS('2', 9);
    }
    //  SECOND MATCH
    if ((this.total3 > 0) && (this.total4 > 0)) {

      if (+this.netTotal3 > +this.netTotal4) {
        this.points3[9] = 0;
        this.points4[9] = 1;
      } else if (+this.netTotal3 < +this.netTotal4) {
        this.points3[9] = 1;
        this.points4[9] = 0;
      } else {
        this.points3[9] = .5;
        this.points4[9] = .5;
      }

      this.pointTotal3 = this.updateTotal(this.points3);
      this.pointTotal4 = this.updateTotal(this.points4);

      this.cssTotal3 = this.getCSS('3', 9);
      this.cssTotal4 = this.getCSS('4', 9);
    }

  }

  updateScoreTotal(array: IScorehole[], golfer: number): number {
    const arrayLength = array.length;
    let total = 0;
    for (let i = 0; i < arrayLength; i++) {
      if (array[i].score !== null) {
        total += +array[i].score;
      } else {
        if (golfer === 1) {
          if (!this.isBye1 && !this.isAbsent1) {
            this.canSave2 = false;
          }
        }
        if (golfer === 2) {
          if (!this.isBye2 && !this.isAbsent2) {
            this.canSave2 = false;
          }
        }
        if (golfer === 3) {
          if (!this.isBye3 && !this.isAbsent3) {
            this.canSave2 = false;
          }
        }
        if (golfer === 4) {
          if (!this.isBye2 && !this.isAbsent2) {
            this.canSave2 = false;
          }
        }
      }
    }
    return total;
  }
  updateNetTotal(array: IScorehole[]): number {
    const arrayLength = array.length;
    let total = 0;
    for (let i = 0; i < arrayLength; i++) {
      if (array[i].score !== null) {
        total += +array[i].net;
      }
    }
    return total;
  }

  updateTotal(array: number[]): number {
    const arrayLength = array.length;
    let total = 0;
    for (let i = 0; i < arrayLength; i++) {
      total += +array[i];
    }
    return total;
  }




  calculateScoreType(hole: number, holes: IScorehole[]): void {

    let score = holes[hole].score;
    let index = 0;
    if (this.useBack) {
      index = 9;
    }
    const par = this.coursedata.courseholes[index + hole].par;
    score += 4; // adjust by 4 beacuse par is score type 4
    let scoreType = score - par;
    if (scoreType > 7) { // 7 is other can't do higher than other
      scoreType = 7;
    }
   holes[hole].type = scoreType;

  }


  save(): void {
    this.saveScore(true);
  }

  saveScore(calcHC: boolean): void {
    // Copy the form values over the golfer object values
    this.scoredata[0].score.score = this.total1;
    this.scoredata[1].score.score = this.total2;
    this.scoredata[0].score.net = this.netTotal1;
    this.scoredata[1].score.net = this.netTotal2;
    this.scoredata[0].score.idschedule.points1 = this.pointTotal1;
    this.scoredata[0].score.idschedule.points2 = this.pointTotal2;
    this.scoredata[1].score.idschedule.points1 = this.pointTotal1;
    this.scoredata[1].score.idschedule.points2 = this.pointTotal2;
    if (this.scoreForm.get('useforhc1').value) {
      this.scoredata[0].score.useforhandicap = 1;
    } else {
      this.scoredata[0].score.useforhandicap = 0;
    }
    if (this.scoreForm.get('useforhc2').value) {
      this.scoredata[1].score.useforhandicap = 1;
    } else {
      this.scoredata[1].score.useforhandicap = 0;
    }

    this.scoredata[2].score.score = this.total3;
    this.scoredata[3].score.score = this.total4;
    this.scoredata[2].score.net = this.netTotal3;
    this.scoredata[3].score.net = this.netTotal4;
    this.scoredata[2].score.idschedule.points1 = this.pointTotal3;
    this.scoredata[2].score.idschedule.points2 = this.pointTotal4;
    this.scoredata[3].score.idschedule.points1 = this.pointTotal3;
    this.scoredata[3].score.idschedule.points2 = this.pointTotal4;
    if (this.scoreForm.get('useforhc3').value) {
      this.scoredata[2].score.useforhandicap = 1;
    } else {
      this.scoredata[2].score.useforhandicap = 0;
    }
    if (this.scoreForm.get('useforhc4').value) {
      this.scoredata[3].score.useforhandicap = 1;
    } else {
      this.scoredata[3].score.useforhandicap = 0;
    }

    const scores: IScore[] = [this.scoredata[0].score, this.scoredata[1].score, this.scoredata[2].score, this.scoredata[3].score];

    this.scoreService.saveScores(scores, calcHC)
      .subscribe(
        () => this.onSaveScoresComplete(calcHC),
        (error: any) => this.errorMessage = <any>error
      );


  }

  onSaveScoresComplete(calcHC: boolean): void {
    if (calcHC) {
      this.router.navigate(['/authenticated/scores']);
    }
    //    console.log('Scores have been saved.');
  }
  getNameCss(golfer: number, hole: number) {

    let cssClasses;
    if (golfer === 1) {
      if (+this.hcHole1[hole] === 1) {
        cssClasses = {
          'hcName': true
        };
      } else {
        cssClasses = {
          'name': true
        };
      }
    }
    if (golfer === 2) {

      if (+this.hcHole2[hole] === 1) {
        cssClasses = {
          'hcName': true
        };
      } else {
        cssClasses = {
          'name': true
        };
      }
    }
    if (golfer === 3) {
      if (+this.hcHole3[hole] === 1) {
        cssClasses = {
          'hcName': true
        };
      } else {
        cssClasses = {
          'name': true
        };
      }
    }
    if (golfer === 4) {

      if (+this.hcHole4[hole] === 1) {
        cssClasses = {
          'hcName': true
        };
      } else {
        cssClasses = {
          'name': true
        };
      }
    }
    return cssClasses;
  }

  getCSS(golfer: String, hole: number) {
    let cssClasses;

    if (golfer === '1') {
      if (+this.points1[hole] === 1) {
        cssClasses = {
          'centerUpper': true
        };
      } else if (+this.points1[hole] === 0.5) {
        cssClasses = {
          'centerUpperHalf': true
        };
      } else {
        cssClasses = {
          'centerNone': true
        };
      }
    }
    if (golfer === '2') {

      if (+this.points2[hole] === 1) {
        cssClasses = {
          'centerLower': true
        };
      } else if (+this.points2[hole] === 0.5) {
        cssClasses = {
          'centerLowerHalf': true
        };
      } else {
        cssClasses = {
          'centerNone': true
        };
      }
    }
    if (golfer === '3') {
      if (+this.points3[hole] === 1) {
        cssClasses = {
          'centerUpper': true
        };
      } else if (+this.points3[hole] === 0.5) {
        cssClasses = {
          'centerUpperHalf': true
        };
      } else {
        cssClasses = {
          'centerNone': true
        };
      }
    }
    if (golfer === '4') {

      if (+this.points4[hole] === 1) {
        cssClasses = {
          'centerLower': true
        };
      } else if (+this.points4[hole] === 0.5) {
        cssClasses = {
          'centerLowerHalf': true
        };
      } else {
        cssClasses = {
          'centerNone': true
        };
      }
    }
    return cssClasses;
  }

}
