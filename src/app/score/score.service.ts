import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { IScoredata } from './scoredata';
import { IScorehole } from './scorehole';
import { IScore } from './score';
import { IGolferScoreData } from './golfer-score-data';
import { IWeeklyScores } from './weekly-scores';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ScoreService {

  private scoreUrl = environment.url + 'score/';
  private pointsUrl = environment.url + 'points/';
  private scoreholeUrl = environment.url + 'scorehole/';
  constructor(private http: Http,
              private httpClient: HttpClient) { }

  getMatchScores(id: number): Observable<IScoredata[]> {

    return this.http.get(this.scoreUrl + 'schedule/' + id)
      .map((response: Response) => response.json())
    //  .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getFoursomeScores(id: number): Observable<IScoredata[]> {
    return this.http.get(this.scoreUrl + 'foursome/' + id)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }
  
  getMatchScore(id: number, golfer: number): Observable<IScoredata> {
    return this.http.get(this.scoreUrl + 'golferWeek/' + id + '/' + golfer)
      .map((response: Response) => response.json())
    //  .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getGolfersScores(year: number): Observable<IGolferScoreData[]> {

    return this.http.get(this.scoreUrl + 'golfersAll/' + year)
      .map((response: Response) => response.json())
    //  .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getLeagueAvg(year: number): Observable<number> {

    return this.http.get(this.scoreUrl + 'avgAll/' + year)
      .map((response: Response) => response.json())
      .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getGolferAvg(year: number, golfer: number): Observable<number> {

    return this.http.get(this.scoreUrl + 'avg/' + golfer + '/' + year)
      .map((response: Response) => response.json())
      .do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }


  hasGolfWeekStarted(year: number, week: number): Observable<Boolean> {

    return this.httpClient.get(this.scoreUrl + 'hasGolfStarted/' + week + '/' + year)
      .catch(this.handleError);
  }

  getWeeklyScores(year: number, week: number): Observable<IWeeklyScores[]> {

    return this.http.get(this.scoreUrl + 'week/' + week + '/' + year)
      .map((response: Response) => response.json())
      //.do(data => console.log('getMatchScore: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveScorehole(scorehole: IScorehole): Observable<IScorehole> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    if (scorehole.idscorehole === 0) {
      scorehole.idscorehole = undefined;
    }
    const url = this.scoreholeUrl + 'save';
    return this.http.post(url, scorehole, options)
      .map((response: Response) => response.json())
      //.do(data => console.log('createScorehole: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  saveScores(scores: IScore[], calcHC: boolean): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    let param = '?hc=false';

    if (calcHC) {
      param = '?hc=true';
    }

    const url = this.scoreUrl + 'saveScores' + param;
    return this.http.post(url, scores, options)
      //.map((response: Response) => response.json())
      //.do(data => console.log('createScorehole: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  calcPoints(week: number, year: number): void {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.pointsUrl + 'calcPoints/' + week + '/' + year;
    this.http.get(url).subscribe({ error: e => console.error(e) });
  }
  
  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  // initializeScedule(): ISchedule {
  //   // Return an initialized object
  //   return {
  //     idschedule: 0,
  //     year: null,
  //     idweek: null,
  //     points1: null,
  //     points2: null,
  //     active: null,
  //     idcourse: null,
  //     idgolfer1: null,
  //     idgolfer2: null
  //   };
  // }

}
