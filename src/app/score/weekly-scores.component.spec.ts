import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyScoresComponent } from './weekly-scores.component';

describe('WeeklyScoresComponent', () => {
  let component: WeeklyScoresComponent;
  let fixture: ComponentFixture<WeeklyScoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyScoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyScoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
