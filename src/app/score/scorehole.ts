import { IScore } from './score';

export interface IScorehole {
    idscorehole: number;
    idscore: IScore;
    hole: number;
    score: number;
    net: number;
    adjustedscore: number;
    type: number;
}
