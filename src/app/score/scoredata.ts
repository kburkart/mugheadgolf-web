import { IScore } from './score';
import { IScorehole } from './scorehole';

export interface IScoredata {
    score: IScore;
    scoreholes: IScorehole[];
}
