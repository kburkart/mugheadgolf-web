import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { SlicePipe } from '@angular/common';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ScoreService } from './score.service';
import { CourseService } from '../course/course.service';
import { GolferService } from '../golfer/golfer.service';

import { IGolfer } from '../golfer/golfer';
import { IScore } from './score';
import { IScoredata } from './scoredata';
import { ICoursedata } from '../course/coursedata';
import { ICourse } from '../course/course';
import { ICourseholes } from '../course/coursehole';
import { IScorehole } from './scorehole';
import { forkJoin } from 'rxjs/observable/forkJoin';


import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { ISchedule } from '../schedule/schedule';
import { ScheduleService } from '../schedule/schedule.service';
import { AnonymousSubscription } from 'rxjs/Subscription';
import {EmptyObservable} from 'rxjs/observable/EmptyObservable';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit, OnDestroy {

  scoreForm: FormGroup;
  errorMessage: string;
  titleBar1: String;
  titleBar2: String;
  name1: string;
  name2: string;

  isAdmin: boolean;
  isBye1: boolean;
  isBye2: boolean;

  isAbsent1: boolean;
  isAbsent2: boolean;

  hc1: number;
  hc2: number;
  public golfers: IGolfer[];
  public schedule: ISchedule;
  private sub: Subscription;


  private coursedata: ICoursedata;
  private scoredata: IScoredata[];
  public cssTotal1: string;
  public cssTotal2: string;


  private holes1: IScorehole[];
  private holes2: IScorehole[];

  private cssHoles1: string[];
  private cssHoles2: string[];

  private hcHole1: number[];
  private hcHole2: number[];

  private net1: number[];
  private net2: number[];

  private points1: number[];
  private points2: number[];

  public total1: number;
  public total2: number;

  private netTotal1: number;
  private netTotal2: number;

  private xtraStroke1: number;
  private xtraStroke2: number;

  private strokes1: number;
  private strokes2: number;

  public pointTotal1: number;
  public pointTotal2: number;

  private id1: number[];
  private id2: number[];
  public firstname1: string;
  public firstname2: string;
  private start: number;
  private useBack: boolean;
  public totalYards: number;
  public totalPar: number;
  public inOut: string;
  private timerSubscription: AnonymousSubscription;
  private autoRefresh: boolean;
  private canSave1: boolean;
  private canSave2: boolean;


  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private scoreService: ScoreService,
    private golferService: GolferService,
    private courseService: CourseService,
    private scheduleService: ScheduleService) { }




  ngOnInit(): void {

    this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
    this.isAdmin = (localStorage.getItem('isAdmin') === 'true');
    this.titleBar1 = '';
    this.titleBar2 = '';
    this.holes1 = [null, null, null, null, null, null, null, null, null];
    this.holes2 = [null, null, null, null, null, null, null, null, null];

    this.useBack = false;
    this.total1 = 0;
    this.total2 = 0;
    this.hc1 = 0;
    this.hc2 = 0;
    this.pointTotal1 = 0;
    this.pointTotal2 = 0;
    this.cssHoles1 = ['centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone'];
    this.cssHoles2 = ['centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone',
      'centerNone', 'centerNone', 'centerNone'];
    this.id1 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.id2 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.hcHole1 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.hcHole2 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.points1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.points2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.net1 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.net2 = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.start = 0;
    this.strokes1 = 0;
    this.strokes2 = 0;
    this.canSave1 = false;
    this.canSave2 = false;

    this.scoreForm = this.fb.group({
      useforhc1: '',
      useforhc2: '',
      backnine: '',
      matchholes: this.fb.array([]),
    });

    for (let i = 0; i < 9; i++) {
      this.addHole(i);
    }
    this.sub = this.route.params.subscribe(
      params => {
        const idschedule = +params['idschedule'];
        this.getScheduleInfo(idschedule);
      });

  }

  private subscribeToData(): void {
    this.timerSubscription = Observable.timer(10000).first().subscribe(() => this.getMatchScores());
  }

  unsubscribe(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  getScheduleInfo(id: number) {
    this.scheduleService.getScheduleById(id)
      .subscribe(schedule => this.getData(schedule),
        (error: any) => this.errorMessage = <any>error
      );
  }


  getData(schedule: ISchedule) {
    this.schedule = schedule;
    const requests: any[] = [];
    requests[0] = this.scoreService.getMatchScore(schedule.idschedule, schedule.idgolfer1.idgolfer);
    requests[1] = this.scoreService.getMatchScore(schedule.idschedule, schedule.idgolfer2.idgolfer);
    requests[2] = this.courseService.getCourseData(schedule.idcourse.idcourse);
    forkJoin(requests).subscribe(
      results => this.processResults(results),
      (error: any) => this.errorMessage = <any>error
    );
  }

  canSave() {
    return this.canSave1;
  }

  getMatchScores() {

    const time = (new Date()).toLocaleTimeString();

    if (this.autoRefresh) {
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
//      console.log('updating scores ' + time);
    const requests: any[] = [];
    requests[0] = this.scoreService.getMatchScore(this.schedule.idschedule, this.schedule.idgolfer1.idgolfer);
    requests[1] = this.scoreService.getMatchScore(this.schedule.idschedule, this.schedule.idgolfer2.idgolfer);
    forkJoin(requests).subscribe(
      results => this.processScores(results),
      (error: any) => this.errorMessage = <any>error
    );
    } else {
//      console.log('not updating scores ' + time);
      this.autoRefresh = (localStorage.getItem('autoRefresh') === 'true');
      this.subscribeToData();
    }
  }


  processScores(results: any[]) {
    const scoreData = [results[0], results[1]];
    this.setScoreData(scoreData);
    this.populateHoleScore();
    this.subscribeToData();
  }

  processResults(results: any[]) {

    const scoreData = [results[0], results[1]];
    this.setScoreData(scoreData);
    this.setCourseData(results[2]);
    this.subscribeToData();
    let par = 0;
    let yards = 0;
    let start = 0;
    let end = 9;
    const holes = this.coursedata.courseholes;
    if (this.useBack) {
      start = 9;
      end = 18;
    }
    for (let x = start; x < end; x++) {
      par += +(holes[x].par);
      yards += +(holes[x].length);
    }
    this.totalYards = yards;
    this.totalPar = par;

  }

  setScoreData(scoredata: IScoredata[]): void {

    this.scoredata = scoredata;
    this.schedule = scoredata[0].score.idschedule;
    if (this.schedule.backnine === 1) {
      this.start = 9;
      this.useBack = true;
      this.scoreForm.get('backnine').patchValue(true);
      this.inOut = 'In';

    } else {
      this.start = 0;
      this.useBack = false;
      this.scoreForm.get('backnine').patchValue(false);
      this.inOut = 'Out';

    }

    if (scoredata[0].score.useforhandicap === 1) {
      this.scoreForm.get('useforhc1').patchValue(true);
    } else {
      this.scoreForm.get('useforhc1').patchValue(false);
    }
    if (scoredata[1].score.useforhandicap === 1) {
      this.scoreForm.get('useforhc2').patchValue(true);
    } else {
      this.scoreForm.get('useforhc2').patchValue(false);
    }
    this.scoreForm.get('backnine').valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.backNineIndicador(value));

    this.firstname1 = scoredata[0].score.idgolfer.firstname;
    this.firstname2 = scoredata[1].score.idgolfer.firstname;
    this.hc1 = Math.round(this.schedule.handicap1);
    this.hc2 = Math.round(this.schedule.handicap2);
      this.isAbsent1 = this.schedule.absent1;
      this.isAbsent2 = this.schedule.absent2;
    if (scoredata[0].score.idgolfer.idgolfer === 0) {
      this.isBye1 = true;
    } else {
      this.isBye1 = false;
    }
    if (scoredata[1].score.idgolfer.idgolfer === 0) {
      this.isBye2 = true;
    } else {
      this.isBye2 = false;
    }

    this.name1 = this.schedule.idgolfer1.firstname + ' ' + this.schedule.idgolfer1.lastname;
    this.name2 = this.schedule.idgolfer2.firstname + ' ' + this.schedule.idgolfer2.lastname;


    if (!this.isBye1 && !this.isBye2) {
      this.strokes1 = +this.hc1 - +this.hc2;
      if (+this.strokes1 < 0) {
        this.strokes1 = 0;
      }
      this.strokes2 = +this.hc2 - +this.hc1;
      if (+this.strokes2 < 0) {
        this.strokes2 = 0;
      }
    } else {
      this.strokes1 = 0;
      this.strokes2 = 0;
    }

    this.xtraStroke1 = Math.floor(this.strokes1 / 9);
    this.xtraStroke2 = Math.floor(this.strokes2 / 9);

  }

  setCourseData(coursedata: ICoursedata): void {
    this.coursedata = coursedata;
    this.calculateHCHoles();
    this.calculateNetHoles();
    this.populateHoleScore();
    this.setupValueChangeSubs();

  }

  calculateHCHoles(): void {

    const r1 = +this.strokes1 % 9;
    const r2 = +this.strokes2 % 9;


    let index = 0;
    if (this.useBack) {
      index = 9;
    }
    for (let i = 0; i < 9; i++) {
      const hc = this.coursedata.courseholes[i + index].handicap;

      if (+r1 >= +hc) {
        this.hcHole1[i] = 1;
      } else {
        this.hcHole1[i] = 0;
      }
      if (+r2 >= +hc) {
        this.hcHole2[i] = 1;
      } else {
        this.hcHole2[i] = 0;
      }
    }
  }

  calculatePoint(hole: number) {
    let score1 = this.holes1[hole].score;
    let score2 = this.holes2[hole].score;

    if ((+score1 !== 0) && (+score2 !== 0)) {

      score1 -= (+this.xtraStroke1 + this.hcHole1[hole]);
      score2 -= (+this.xtraStroke2 + this.hcHole2[hole]);

      if (+score1 === +score2) {
        this.points1[hole] = .5;
        this.points2[hole] = .5;
      } else if (+score1 > +score2) {
        this.points1[hole] = 0;
        this.points2[hole] = 1;
      } else {
        this.points1[hole] = 1;
        this.points2[hole] = 0;
      }
    }
    this.cssHoles1[hole] = this.getCSS('1', hole);
    this.cssHoles2[hole] = this.getCSS('2', hole);

  }
  calculateNetHoles(): void {

    const xtraStroke1 = Math.floor(+this.hc1 / 9);
    const xtraStroke2 = Math.floor(+this.hc2 / 9);

    this.net1.forEach((value, key) => {
      this.net1[key] = -xtraStroke1;
    });
    this.net2.forEach((value, key) => {
      this.net2[key] = -xtraStroke2;
    });

    const R1 = Math.floor(+this.hc1 % 9);
    const R2 = Math.floor(+this.hc2 % 9);

    let index = 0;
    if (this.useBack) {
      index = 9;
    }
    for (let i = 0; i < 9; i++) {
      const hc = this.coursedata.courseholes[i + index].handicap;

      if (+R1 >= +hc) {
        this.net1[i] -= 1;
      }
      if (+R2 >= +hc) {
        this.net2[i] -= 1;
      }

    }

  }

  addHole(i): void {

    const control = <FormArray>this.scoreForm.get('matchholes');
    control.push(this.createHole(i));

  }

  populateHoleScore() {


    let numDBScore1: number;
    let numDBScore2: number;


    if (this.scoredata[0].hasOwnProperty('scoreholes')) {
      numDBScore1 = this.scoredata[0].scoreholes.length;
    } else {
      numDBScore1 = 0;
    }

    if (this.scoredata[1].hasOwnProperty('scoreholes')) {
      numDBScore2 = this.scoredata[1].scoreholes.length;
    } else {
      numDBScore2 = 0;
    }


    for (let x = 0; x < numDBScore1; x++) {
      this.holes1[x] = this.scoredata[0].scoreholes[x];
      const array = this.scoreForm.get('matchholes') as FormArray;
      const score = array.at(x).get('score1');
      score.patchValue(this.holes1[x].score);
    }
    for (let x = 0; x < numDBScore2; x++) {
      this.holes2[x] = this.scoredata[1].scoreholes[x];
      const array = this.scoreForm.get('matchholes') as FormArray;
      const score = array.at(x).get('score2');
      score.patchValue(this.holes2[x].score);
    }

    for (let x = +numDBScore1; x < 9; x++) {
      this.holes1[x] = this.initHole(x, 0);
    }
    for (let x = +numDBScore2; x < 9; x++) {
      this.holes2[x] = this.initHole(x, 1);
    }

    for (let x = 0; x < 9; x++) {
      this.calculatePoint(x);
    }

    this.updateTotals();

  }
  setupValueChangeSubs() {
    for (let x = 0; x < 9; x++) {
      this.calculatePoint(x);
      const array = this.scoreForm.get('matchholes') as FormArray;
      let score = array.at(x).get('score1');
      score.valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.changeScore1(x, value));
      score = array.at(x).get('score2');
      score.valueChanges.auditTime(2000).distinctUntilChanged().subscribe(value => this.changeScore2(x, value));

    }

  }


  createHole(i): FormGroup {
    let newGroup: FormGroup;
    newGroup = this.fb.group({
      score1: ['', this.myRequired(this.isBye1)],
      score2: ['', this.myRequired(this.isBye2)]
    });

    return newGroup;

  }

  initHole(hole: number, golfer: number): IScorehole {
    return {
      idscorehole: 0,
      idscore: this.scoredata[golfer].score,
      hole: +hole + 1,
      score: null,
      net: null,
      adjustedscore: null,
      type: null
    };
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.unsubscribe();

  }

  myRequired(isBye: boolean) {
    return function (input: FormControl) {
      if ((input.value && !isNaN(input.value)) || isBye) {
        return null;
      } else {
        return { 'required': true };
      }
    };
  }

  getControlName(c: AbstractControl): string | null {
    const formGroup = c.parent.controls;
    return Object.keys(formGroup).find(name => c === formGroup[name]) || null;
  }

  backNineIndicador(isChecked): void {

    if (isChecked) {
      this.useBack = true;
      this.start = 9;
      this.schedule.backnine = 1;

    } else {
      this.useBack = false;
      this.start = 0;
      this.schedule.backnine = 0;

    }

    this.scheduleService.saveBacknine(this.schedule)
      .subscribe(
        () => this.onSaveBacknineComplete(),
        (error: any) => this.errorMessage = <any>error
      );

  }

  onSaveBacknineComplete(): void {
    //    console.log('Backnine saved, recalculating points');
    this.calculateHCHoles();
    this.calculateNetHoles();
    this.updateNet();
    for (let x = 0; x < 9; x++) {
      this.calculatePoint(x);
    }
    this.updateTotals();
  }

  saveHole(hole: number, golfer: number): void {


    // Copy the form values over the golfer object values
    let scorehole: IScorehole;

    if (golfer === 0) {
      scorehole = this.holes1[hole];
    } else {
      scorehole = this.holes2[hole];
    }

    // console.log('saving hole ' + JSON.stringify(scorehole));
    this.scoreService.saveScorehole(scorehole)
      .subscribe(
        (newScorehole) => this.onSaveHoleComplete(newScorehole, hole, golfer),
        (error: any) => this.errorMessage = <any>error
      );
  }

  onSaveHoleComplete(scorehole: IScorehole, hole: number, golfer: number): void {
    if (golfer === 0) {
      this.holes1[hole] = scorehole;
    } else {
      this.holes2[hole] = scorehole;
    }
  }

  updateNet() {
    for (let x = 0; x < this.holes1.length; x++) {
      if (this.holes1[x] !== null) {
        this.changeScore1(x, this.holes1[x].score);
      }
    }
    for (let x = 0; x < this.holes2.length; x++) {
      if (this.holes2[x] !== null) {
        this.changeScore2(x, this.holes2[x].score);
      }
    }
  }


  changeScore1(hole: number, score: number) {
    this.holes1[hole].score = score;
    this.holes1[hole].net = +score + +this.net1[hole];
    this.calculatePoint(hole);
    this.calculateScoreType(hole, this.holes1);
    this.updateTotals();
    this.saveHole(hole, 0);
    this.saveScore(false);

  }

  changeScore2(hole: number, score: number) {
    this.holes2[hole].score = score;
    this.holes2[hole].net = +score + +this.net2[hole];
    this.calculatePoint(hole);
    this.calculateScoreType(hole, this.holes2);
    this.updateTotals();
    this.saveHole(hole, 1);
    this.saveScore(false);
  }

  updateTotals(): void {

    this.canSave2 = true;
    this.total1 = this.updateScoreTotal(this.holes1, 1);
    this.total2 = this.updateScoreTotal(this.holes2, 2);
    this.canSave1 = this.canSave2;
    this.netTotal1 = this.updateNetTotal(this.holes1);
    this.netTotal2 = this.updateNetTotal(this.holes2);

    if ((this.total1 > 0) && (this.total2 > 0)) {

      if (+this.netTotal1 > +this.netTotal2) {
        this.points1[9] = 0;
        this.points2[9] = 1;
      } else if (+this.netTotal1 < +this.netTotal2) {
        this.points1[9] = 1;
        this.points2[9] = 0;
      } else {
        this.points1[9] = .5;
        this.points2[9] = .5;
      }

      this.pointTotal1 = this.updateTotal(this.points1);
      this.pointTotal2 = this.updateTotal(this.points2);

      this.cssTotal1 = this.getCSS('1', 9);
      this.cssTotal2 = this.getCSS('2', 9);
    }
  }

  updateScoreTotal(array: IScorehole[], golfer: number): number {
    const arrayLength = array.length;
    let total = 0;
    for (let i = 0; i < arrayLength; i++) {
      if (array[i].score !== null) {
        total += +array[i].score;
      } else {
        if (golfer === 1) {
          if (!this.isBye1 && !this.isAbsent1) {
            this.canSave2 = false;
          }
        }
        if (golfer === 2) {
          if (!this.isBye2 && !this.isAbsent2) {
            this.canSave2 = false;
          }
        }
      }
    }
    return total;
  }
  updateNetTotal(array: IScorehole[]): number {
    const arrayLength = array.length;
    let total = 0;
    for (let i = 0; i < arrayLength; i++) {
      if (array[i].score !== null) {
        total += +array[i].net;
      }
    }
    return total;
  }

  updateTotal(array: number[]): number {
    const arrayLength = array.length;
    let total = 0;
    for (let i = 0; i < arrayLength; i++) {
      total += +array[i];
    }
    return total;
  }




  calculateScoreType(hole: number, holes: IScorehole[]): void {

    let score = holes[hole].score;
    let index = 0;
    if (this.useBack) {
      index = 9;
    }
    const par = this.coursedata.courseholes[index + hole].par;
    score += 4; // adjust by 4 beacuse par is score type 4
    let scoreType = score - par;
    if (scoreType > 7) { // 7 is other can't do higher than other
      scoreType = 7;
    }
   holes[hole].type = scoreType;

  }


  save(): void {
    this.saveScore(true);
  }

  saveScore(calcHC: boolean): void {
    // Copy the form values over the golfer object values
    this.scoredata[0].score.score = this.total1;
    this.scoredata[1].score.score = this.total2;
    this.scoredata[0].score.net = this.netTotal1;
    this.scoredata[1].score.net = this.netTotal2;
    this.scoredata[0].score.idschedule.points1 = this.pointTotal1;
    this.scoredata[0].score.idschedule.points2 = this.pointTotal2;
    this.scoredata[1].score.idschedule.points1 = this.pointTotal1;
    this.scoredata[1].score.idschedule.points2 = this.pointTotal2;
    if (this.scoreForm.get('useforhc1').value) {
      this.scoredata[0].score.useforhandicap = 1;
    } else {
      this.scoredata[0].score.useforhandicap = 0;
    }
    if (this.scoreForm.get('useforhc2').value) {
      this.scoredata[1].score.useforhandicap = 1;
    } else {
      this.scoredata[1].score.useforhandicap = 0;
    }

    const scores: IScore[] = [this.scoredata[0].score, this.scoredata[1].score];

    this.scoreService.saveScores(scores, calcHC)
      .subscribe(
        () => this.onSaveScoresComplete(calcHC),
        (error: any) => this.errorMessage = <any>error
      );


  }

  onSaveScoresComplete(calcHC: boolean): void {
    if (calcHC) {
      this.router.navigate(['/authenticated/scores']);
    }
    //    console.log('Scores have been saved.');
  }
  getNameCss(golfer: number, hole: number) {

    let cssClasses;
    if (golfer === 1) {
      if (+this.hcHole1[hole] === 1) {
        cssClasses = {
          'hcName': true
        };
      } else {
        cssClasses = {
          'name': true
        };
      }
    }
    if (golfer === 2) {

      if (+this.hcHole2[hole] === 1) {
        cssClasses = {
          'hcName': true
        };
      } else {
        cssClasses = {
          'name': true
        };
      }
    }
    return cssClasses;
  }

  getCSS(golfer: String, hole: number) {
    let cssClasses;

    if (golfer === '1') {
      if (+this.points1[hole] === 1) {
        cssClasses = {
          'centerUpper': true
        };
      } else if (+this.points1[hole] === 0.5) {
        cssClasses = {
          'centerUpperHalf': true
        };
      } else {
        cssClasses = {
          'centerNone': true
        };
      }
    }
    if (golfer === '2') {

      if (+this.points2[hole] === 1) {
        cssClasses = {
          'centerLower': true
        };
      } else if (+this.points2[hole] === 0.5) {
        cssClasses = {
          'centerLowerHalf': true
        };
      } else {
        cssClasses = {
          'centerNone': true
        };
      }
    }
    return cssClasses;
  }

}
