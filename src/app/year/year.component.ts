import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,  Validators, } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { UserApi } from '../fw/users/user-api';



@Component({
  selector: 'app-year',
  templateUrl: './year.component.html',
  styleUrls: ['./year.component.css']
})
export class YearComponent implements OnInit {

  pageTitle = 'Select Year To Display';
  yearSelectForm: FormGroup;
  errorMessage: string;
  selectedYear: number;
  public years: number[] = [2018];

  constructor(private userApi: UserApi,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
) { }

  ngOnInit(): void {
   const d = new Date();
   this.selectedYear = +localStorage.getItem('year');
   const currentYear = d.getFullYear();

   const totalYears = currentYear - 2018;
   for (let i = 1; i <= totalYears; i++) {
    this.years[i] = 2018 + i;
  }

  this.yearSelectForm = this.fb.group({
      selectYear: ['', Validators.required],
    });

  }

  switchYear() {
    this.selectedYear = this.yearSelectForm.controls['selectYear'].value;
    console.log('Selected Year is ' + this.selectedYear);
    localStorage.setItem('year', this.selectedYear + '');
    this.userApi.setupLocalStorage();
//    this.router.navigate(['/authenticated/dashboard']);

  }

}
