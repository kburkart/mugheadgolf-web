import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { TickerApi } from '../fw/ticker/ticker-api';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

//import { Observable } from 'rxjs/Observable';
import {Observable} from 'rxjs/Rx'
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { isNull } from 'util';


@Injectable()
export class TickerService implements TickerApi {

  public isAuthenticated: boolean;
  private tickerUrl = environment.url + 'ticker/messages';
  message: String;

  constructor(private http: Http) {
    // console.log('ticker service constructor');

   }

   getMessages(): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

  return this.http.get(this.tickerUrl, options);

  }

 

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

}
