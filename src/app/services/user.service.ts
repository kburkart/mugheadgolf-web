import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { UserApi } from '../fw/users/user-api';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

// import { Observable } from 'rxjs/Observable';
// tslint:disable-next-line:import-blacklist
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { IGolfer } from '../golfer/golfer';
import { isNull } from 'util';
import { IGolferMoney } from '../money/golfer-money';
import { IDivision } from '../divisions/division';
import { IPointTotal } from '../standings/pointTotal';
import { ScoreService } from '../score/score.service';
import { PointsService } from '../standings/points.service';
import { MoneyService } from '../money/money.service';
import { DivisionsService } from '../divisions/divisions.service';
import { ScheduleService } from '../schedule/schedule.service';
import { DivisionGolferService } from '../divisions/division-golfer.service';
import { IDivisionGolfer } from '../divisions/division-golfer';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { ISchedule } from '../schedule/schedule';


@Injectable()
export class UserService implements UserApi {

  public isAuthenticated: boolean;
  private forgotUrl = environment.url + 'golfer/forgot/';
  private loginUrl = environment.url + 'golfer/login';
  private autorefreshUrl = environment.url + 'golfer/autoRefresh/';
  public firstname: string;
  public lastname: string;
  public admin: boolean;
  public phone: string;
  division: IDivision;
  divisionId: number;
  errorMessage: string;
  totalPoints: IPointTotal;
  currentWeek: number;
  currentHandicap: number;
  idgolfer: number;

  constructor(private http: Http,
    private scoreService: ScoreService,
    private pointsService: PointsService,
    private moneyService: MoneyService,
    private divisionGolferService: DivisionGolferService,
    private divisionService: DivisionsService,
    private scheduleService: ScheduleService,
    private router: Router) {
   }

  isAuthorized(): boolean {
    if (localStorage.getItem('isAuthenticated') !== 'true') {
      this.isAuthenticated = false;
      return false;
    } else {
      this.firstname = localStorage.getItem('firstname');
      this.lastname = localStorage.getItem('lastname');
      this.isAuthenticated = true;
      return true;
    }

  }
  toggleAutoRefresh(): Observable<any> {
    const url = this.autorefreshUrl + localStorage.getItem('idgolfer');
    return this.http.get(url).catch(this.handleError);
  }

  signIn(username: string, password: string): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    let golfer: IGolfer;
    golfer = this.initializeGolfer();
    golfer.username = username;
    golfer.password = password;

  //  console.log ('UserService.signIn: ' + username + ' ' + password);
    return this.http.post(this.loginUrl, golfer, options)
    .map(this.extractData)
    .do(data => { console.log('sign in response: ' + JSON.stringify(data));
                  this.firstname = data.firstname;
                  this.lastname = data.lastname;
                  this.isAuthenticated = true;
                  this.admin = data.admin; })
    .catch(this.handleError);
  }

  forgot(phone: string): Observable<any> {
    // console.log('UserService.forgot: ' + phone);
    return this.http.get(this.forgotUrl + phone)
    .catch(this.handleError);
  }


  signOut(): Observable<any> {
      localStorage.clear();
      this.isAuthenticated = false;
      this.firstname = '';
      this.lastname = '';
      this.router.navigate(['/signin']);
      return Observable.of({});
  }

  private extractData(response: Response) {
    if (response.text() === '') {
      this.isAuthenticated = false;
      localStorage.clear();
      throw new Error('Invalid username and/or password');
    } else {
    let golfer: IGolfer;
    golfer = response.json();
      if ( golfer ) {
        localStorage.setItem('firstname', golfer.firstname.toUpperCase());
        localStorage.setItem('lastname', golfer.lastname.toUpperCase());
        localStorage.setItem('isAuthenticated', 'true');
        localStorage.setItem('idgolfer', golfer.idgolfer.toString());
        localStorage.setItem('isAdmin', golfer.admin.toString());
        localStorage.setItem('autoRefresh', golfer.autorefresh ? 'true' : 'false');
        localStorage.setItem('year', (new Date()).getFullYear() + '');
        return golfer;
      } else {
        this.isAuthenticated = false;
        localStorage.clear();
        throw new Error('Invalid username and/or password');
      }
    }
  }

  getInformation() {

    const requests: any[] = [];
    const year  = +localStorage.getItem('year');
    // console.log('Current year is ' + year);
    requests[0] = this.moneyService.getGolferMoney(year, this.idgolfer);
    requests[1] = this.pointsService.getGolferTotalPoints(this.idgolfer);
    requests[2] = this.scoreService.getGolferAvg(year, this.idgolfer);
    requests[3] = this.scoreService.getLeagueAvg(year);
    requests[4] = this.scheduleService.getGolferSchedule(year, this.idgolfer);
    if (this.divisionId !== 0) {
      requests[5] = this.divisionService.getDivision(this.divisionId);
      requests[6] = this.pointsService.getDivisionLeaderPoints(this.divisionId);
    }
      forkJoin(requests).subscribe(
        results => this.processResults(results),
        (error: any) => {this.errorMessage = <any>error;
        console.log(this.errorMessage);
      }
      );
  }

  getCurrentWeek() {
    const year  = +localStorage.getItem('year');
    this.scheduleService.getCurrentWeek(year).subscribe((currentWeek: number) => {
      localStorage.setItem('currentWeek', currentWeek.toString());
      this.currentWeek = currentWeek;
      // console.log('current week is ' + this.currentWeek);
      this.getInformation();
    });
  }
  setOpponentandResults(matches: ISchedule[]): void {
    const currentMatch = matches[this.currentWeek - 1];
    if (currentMatch) {
    localStorage.setItem('currentMatch', currentMatch.idschedule.toString());
    if (currentMatch.idgolfer1.idgolfer === this.idgolfer) {
      localStorage.setItem('currentOpponent', (currentMatch.idgolfer2.firstname + ' ' + currentMatch.idgolfer2.lastname));
      localStorage.setItem('opponentHandicap', currentMatch.idgolfer2.currenthandicap.toString());
      localStorage.setItem('currentHandicap', currentMatch.idgolfer1.currenthandicap.toString());
      localStorage.setItem('opponentAbsent', currentMatch.absent2 ? 'true' : 'false');
    } else if (currentMatch.idgolfer2.idgolfer === this.idgolfer) {
      localStorage.setItem('currentOpponent', (currentMatch.idgolfer1.firstname + ' ' + currentMatch.idgolfer1.lastname));
      localStorage.setItem('opponentHandicap', currentMatch.idgolfer1.currenthandicap.toString());
      localStorage.setItem('currentHandicap', currentMatch.idgolfer2.currenthandicap.toString());
      localStorage.setItem('opponentAbsent', currentMatch.absent1 ? 'true' : 'false');
    }
  }

    if (this.currentWeek > 1) {
      const lastMatch = matches[this.currentWeek - 2];
      if (lastMatch) {
      localStorage.setItem('lastMatch', lastMatch.idschedule.toString());
      if (lastMatch.idgolfer1.idgolfer === this.idgolfer) {
        localStorage.setItem('lastOpponent', (lastMatch.idgolfer2.firstname + ' ' + lastMatch.idgolfer2.lastname));
        localStorage.setItem('lastOpponentPoints', lastMatch.points2.toString());
        localStorage.setItem('lastPoints', lastMatch.points1.toString());
      } else if (lastMatch.idgolfer2.idgolfer === this.idgolfer) {
        localStorage.setItem('lastOpponent', (lastMatch.idgolfer1.firstname + ' ' + lastMatch.idgolfer1.lastname));
        localStorage.setItem('lastOpponentPoints', lastMatch.points1.toString());
        localStorage.setItem('lastPoints', lastMatch.points2.toString());
      }
    }

    }


  }


  processResults(results: any[]) {

    this.setGolferMoney(results[0]);
    this.setGolferPointTotal(results[1]);
    this.setGolferAvg(results[2]);
    this.setLeagueAvg(results[3]);
    this.setOpponentandResults(results[4]);
    if (this.divisionId !== 0) {
      this.setDivisionInfo(results[5]);
      this.setDivisionLeaderPoints(results[6]);
    }
    this.router.navigate(['/authenticated']);
  }

  getGolferDivision() {
    this.divisionGolferService.getDivisionGolfersByGolfer(this.idgolfer).subscribe((data: IDivisionGolfer) => {
      if (data != null) {
        localStorage.setItem('divisionId', data.iddivision.toString());
        this.divisionId = data.iddivision;
        this.getCurrentWeek();
      } else {
        console.log('Null data returned');
        this.divisionId = 0;
        this.getCurrentWeek();
      }

    });
  }

  setupLocalStorage() {
    this.idgolfer = +localStorage.getItem('idgolfer');
    this.getGolferDivision();
  }

  setGolferMoney(data: IGolferMoney) {
    localStorage.setItem('winnings', data.total.toString());
  }
  setDivisionInfo(info: IDivision) {
    localStorage.setItem('divisionName', info.name);
  }

  setDivisionLeaderPoints(points: IPointTotal) {
    localStorage.setItem('divisionLeader', points.name);
    localStorage.setItem('divisionLeaderPoints', points.totalpoints.toString());

  }
  setGolferPointTotal(points: number) {
    localStorage.setItem('totalpoints', points.toString());
  }

  setGolferAvg(data: number) {
      localStorage.setItem('golferAvg', data.toString());
  }

  setLeagueAvg(data: number) {
      localStorage.setItem('leagueAvg', data.toString());
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.log('UserService.failure: ');

    return Observable.throw('Invalid username and/or password');
  }

  initializeGolfer(): IGolfer {
    // Return an initialized object
    return {
      idgolfer: 0,
      firstname: null,
      lastname: null,
      username: null,
      password: null,
      address: null,
      city: null,
      state: null,
      zip: null,
      email: null,
      spouse: null,
      active: null,
      phone: null,
      currenthandicap: null,
      admin: null,
      autorefresh: true
    };
  }
}
