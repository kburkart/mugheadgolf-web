import { AbstractControl, ValidatorFn } from '@angular/forms';

export class ScoreValidator {

    static required(isBye: boolean): ValidatorFn {
        return (c: AbstractControl): { [key: string]: boolean } | null => {
            if (isBye || (c.value && !isNaN(c.value))) {
                return null;
            } else {
                return { 'required': true };
            }
        };
    }
}
