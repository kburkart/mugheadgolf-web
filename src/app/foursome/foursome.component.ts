import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { FoursomeService } from './foursome.service';

import { IGolfer } from '../golfer/golfer';
import { ISchedule } from '../schedule/schedule';
import { NumberValidators } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';
import { IFoursome } from './foursome';
import { Console } from '@angular/core/src/console';
import { ScreenService } from '../fw/services/screen.service';
import { IWeeklyFoursome } from './weekly-foursomes';


@Component({
  selector: 'app-foursome',
  templateUrl: './foursome.component.html',
  styleUrls: ['./foursome.component.css']
})
export class FoursomeComponent implements OnInit {
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  errorMessage: string;
  foursomeForm: FormGroup;
  schedules: ISchedule[];
  emailMessage: String;
  currentWeek: number;
  year: number;
  isAdmin: boolean;
  user: number;
  weeklyFoursomes: IWeeklyFoursome[];
  dragGolfer: number;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private foursomeService: FoursomeService) { }

  ngOnInit() {
    this.isAdmin = (localStorage.getItem('isAdmin') === 'true');
    this.user = +localStorage.getItem('idgolfer');
    this.currentWeek = +localStorage.getItem('currentWeek');
    this.year  = +localStorage.getItem('year');
    this.getFoursomes();
    this.foursomeForm = this.fb.group({
    });

  }

  ngAfterViewInit(): void {
  }

  gotoScoreCard(idfoursome: number) {
    console.log('Going to scorecard for ' + idfoursome);
    this.router.navigate(['/authenticated/foursomeScore', idfoursome]);
  }

  getFoursomes(): void {
    this.foursomeService.getFoursomes(this.year)
      .subscribe(weeklyFoursomes => {
        this.weeklyFoursomes = weeklyFoursomes;
        console.log('getForesomes: ' + this.weeklyFoursomes[1].foursomes2[5].idschedule1.absent1)
        },
        (error: any) => this.errorMessage = <any>error
      );
  }

  isGolferAdminOrUser(golfer: IGolfer): boolean {
    if (this.isAdmin) {
      return true;
    } else {
      return false;
    }
  }


  swapTeeTimes($event: any, schedule: ISchedule, week: number) {
    const schedule1 = $event.dragData;
    const answer = confirm('Are you sure you want to swap ' + schedule1.idgolfer1.lastname + ' vs ' +
    schedule1.idgolfer2.lastname + ' with ' + schedule.idgolfer1.lastname + ' vs ' + schedule.idgolfer2.lastname + '?');

    if ( answer ) {
      const year  = +localStorage.getItem('year');
      this.foursomeService.swapCarts(year, week, schedule.idschedule, schedule1.idschedule)
        .subscribe(weeklyFoursomes => this.weeklyFoursomes = weeklyFoursomes,
          (error: any) => this.errorMessage = <any>error
        );
    }
  }





}
