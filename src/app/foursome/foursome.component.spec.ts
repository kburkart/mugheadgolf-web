import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoursomeComponent } from './foursome.component';

describe('FoursomeComponent', () => {
  let component: FoursomeComponent;
  let fixture: ComponentFixture<FoursomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoursomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoursomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
