import { TestBed, inject } from '@angular/core/testing';

import { FoursomeService } from './foursome.service';

describe('FoursomeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FoursomeService]
    });
  });

  it('should be created', inject([FoursomeService], (service: FoursomeService) => {
    expect(service).toBeTruthy();
  }));
});
