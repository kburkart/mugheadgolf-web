import { IFoursome } from './foursome';

export interface IWeeklyFoursome {
    week: number;
    course1: String
    course2: String
    foursomes1: IFoursome[];
    foursomes2: IFoursome[];
}
