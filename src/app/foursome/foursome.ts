import { Time } from "@angular/common";
import { ISchedule } from "../schedule/schedule"

export interface IFoursome {
    idfoursome: number;
    year: number;
    idweek: number;
    teetime: string;
    idschedule1: ISchedule;
    idschedule2: ISchedule;
}
