import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { environment } from '../../environments/environment';

import { IWeeklyFoursome } from './weekly-foursomes'
import { IFoursome } from './foursome'

@Injectable()
export class FoursomeService {

  private scheduleUrl = environment.url + 'schedule/';
  private foursomeUrl = environment.url + 'foursome/';
  constructor(private http: Http) { }

  getFoursomes(year: number): Observable<IWeeklyFoursome[]> {
    return this.http.get(this.foursomeUrl + 'year/' + year)
      .map((response: Response) => this.mapWeeklyFoursomes(response.json()))
     //  .do(data => console.log('getForesomes: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getFoursomeById(id: number): Observable<IFoursome> {
    return this.http.get(this.foursomeUrl + id)
      .map((response: Response) => this.toFoursome(response.json()))
     //  .do(data => console.log('getForesomes: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  createFoursomes(week: number, year: number): Observable<any[]> {
    return this.http.get(this.foursomeUrl + 'generate/week/' + week+ '/year/' + year)
     .map((response: Response) => response.json())
     .do(data => console.log('getScedules: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  toWeeklyFoursome(r: any): IWeeklyFoursome {

    const result = <IWeeklyFoursome> ({
      week: r.week,
      course1: r.course1,
      course2: r.course2,
      foursomes1: this.mapFoursome(r.foursomes1),
      foursomes2: this.mapFoursome(r.foursomes2),
    });
//    console.log('Parsed item:', result);
    return result;
  }

  saveBacknine(foursome: IFoursome): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    const url = this.foursomeUrl + 'saveBacknine';
    return this.http.post(url, foursome, options)
    //  .map((response: Response) => response.json())
      // .do(data => console.log('createScorehole: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }


  getCurrentWeek(year: number): Observable<number> {
    return this.http.get(this.scheduleUrl + 'currentWeek/' + year)
      .map((response: Response) => response.json())
  //    .do(data => console.log('currentweek: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  mapWeeklyFoursomes(data: Array<any>): IWeeklyFoursome[] {
    return data.map(item => this.toWeeklyFoursome(item));
  }


  mapFoursome(data: Array<any>): IFoursome[] {
    // console.log('getScedules: ' + JSON.stringify(data));
     return data.map(item => this.toFoursome(item));
   }

  swapCarts(week: number, year: number, schedule1: number, schedule2: number): Observable<IWeeklyFoursome[]> {
    return this.http.get(this.foursomeUrl + 'swap/' + year + '/' + week + '/' + schedule1 + '/' + schedule2)
    .map((response: Response) => this.mapWeeklyFoursomes(response.json()))
   // .do(data => console.log('getScedules: ' + JSON.stringify(data)))
    .catch(this.handleError);
  }

  toFoursome(r: any): IFoursome {

    const result = <IFoursome> ({
      idfoursome: r.idfoursome,
      idschedule1: r.idschedule1,
      idschedule2: r.idschedule2,
      year: r.year,
      idweek: r.idweek,
      teetime: this.toDate(r.teetime)
    });
 //   console.log('Parsed item:', result);
    return result;
  }

  initializeFoursome(): IFoursome {
    // Return an initialized object
    return {
      idfoursome: 0,
      idschedule1: null,
      idschedule2: null,
      year: null,
      idweek: null,
      teetime: null

    };
  }

  private toDate(datetoconvert: string): string{
    let timetokens = datetoconvert.split(':')
    let time = (parseInt(timetokens[0], 10) + ':' + timetokens[1])
    return time
  }

  private handleError(error: Response): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
