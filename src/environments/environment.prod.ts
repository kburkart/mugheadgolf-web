export const environment = {
  production: true,
  url: 'https://api.mugheadgolf.com/',
  env: 'prod'
};
